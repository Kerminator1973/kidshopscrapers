#!/usr/bin/python
# -*- coding: utf-8

import logging


# Класс является контейнером товаров и реализует стратегию их группировки
class GroupingByComposedArticle:

    def __init__(self):
        self._products = {}

    def append(self, article, product, ignore_duplicate=False):

        # Если композитный индекс товара уже есть в контейнере, то выполняем
        # операцию объединение ранее добавленного товара с новым. Если
        # индекс ещё не определён, то включаем товар по его артикулом
        if self._products.get(article) is None:
            self._products[article] = product
        else:
            self._products[article].merge_from(product, ignore_duplicate)

    def get_products(self):
        return self._products


# Класс является чуть более сложной стратегией, учитывающей возможность
# группировки товаров ещё и по альтернативным индексам
class GroupingByAlternativeIndexes:
    def __init__(self):
        self._products = {}
        self._alt_indexes = {}

    def append(self, article, product, ignore_duplicate=False):
        """
        :param str article: композитный артикул товара (тип товара+бренд+модель) 
        :param product: объект, хранящий информацию о товаре
        :param bool ignore_duplicate: проверять дублирование товара (Issue #137)
        """

        alternative_index = product.get_alternative_index()
        if alternative_index is not None:

            if self._alt_indexes.get(alternative_index) is None:

                # Если в списке альтернативных индексов указанного
                # значения ещё нет, то добавляем его с ссылкой на
                # традиционный композитный индекс
                self._alt_indexes[alternative_index] = article

            else:
                # Если товар с указанным альтернативным индексом уже
                # был добавлен ранее, то текущий товар включаем как товарное
                # предложение ранее встречавшегося товара
                traditional_index = self._alt_indexes[alternative_index]
                if self._products.get(traditional_index) is not None:

                    self._products[traditional_index].merge_from(product, ignore_duplicate)
                    return

        # Если композитный индекс товара уже есть в контейнере, то выполняем
        # операцию объединения ранее добавленного товара с новым. Если
        # индекс ещё не определён, то включаем товар по его артикулом
        if self._products.get(article) is None:
            self._products[article] = product
        else:
            self._products[article].merge_from(product, ignore_duplicate)

    def get_products(self):
        return self._products
