#!/usr/bin/python
# -*- coding: utf-8

import re
import json
import html
import copy
import time
from time import localtime, strftime
import logging
import requests
from bs4 import BeautifulSoup
from ProductDescription import ProductContainer         # Класс, используемый для объединения информации о товаре
import ScrapingHelper                                   # Набор вспомогательных функций
import FilterCollection                                 # Класс-агрегатор атрибутов отбора данных
from multiprocessing.dummy import Pool as ThreadPool    # API поддержки много-поточности в Python-приложении


# Класс-контейнер используется при обмене данными при выполнении
# много-поточной загрузки html-страниц
class ProductData:
    def __init__(self, _product_id, _product_url, _product_page, _json_code=None):
        self.product_id = _product_id
        self.product_url = _product_url
        self.product_page = _product_page
        self.json_code = _json_code


# Функция асинхронной загузки html-страниц. Может быть использована для
# работы в Threading Pool
def async_download_html_page_ds(product):

    url = DochkisinochkiCatalogScraper.get_shop_site() + product[1]

    try:

        session = requests.Session()
        response = session.get(url)
        if response.status_code != requests.codes.ok:
            logging.error('Статус-код отличается от [200]: {}'.format(response.status_code))
            return ProductData(product[0], product[1], None)

        html_page = ProductData(product[0], product[1], response.text)
        return html_page

    except requests.exceptions.RequestException as e:
        logging.error('Сбой в сетевой подсистеме:', e)

    # Возвращаем пустую страницу
    return ProductData(product[0], product[1], None)


class DochkisinochkiCatalogScraper:

    DEFAULT = "DEFAULT"

    def __init__(self, database_helper, options):

        # Сохраняем ссылку на класс работы с базой данных. Запоминаем
        # настройки, полученные от пользователя класса
        self.db = database_helper
        self.so = options

        # Указываем название магазина, с которым должны быть связаны все операции
        # в базе данных, связанные с категориями магазина "Дочки-Сыночки"
        database_helper.activate_shop('DochkiSinochki')

        # Создаём объёкт-сессию, через который будет осуществляться загрузка
        # страниц с сайта магазина "Дочки-сыночки". Использование библиотеки Requests
        # позволяет активировать режим "Keep-ALive", который ускоряет получение
        # информации с сайта магазина
        self.session = requests.Session()

        # Считываем из базы данных в память таблицу брендов и моделей, которая
        # может быть использована при разборе строки с описанием моделей, либо
        # при экспорте в JSON. См. #43
        self.trademarks = self.db.get_brands_and_trademarks()

        # Определяем свойство здесь, чтобы успокоить среду разработки
        self.extract_attr_count = 0

        # Общее количество товаров, которые были извлечены с витрины магазина
        self.showcase_products_count = 0

        # Issue #38. Проверка дублирования товаров при извлечении данных
        # с витрины магазина
        self.duplication_check_set = set()

    @staticmethod
    def get_shop_name():
        return 'Дочки-Сыночки'

    @staticmethod
    def get_shop_site():
        return 'http://www.dochkisinochki.ru'

    # Public-функция осуществляет загрузку каталога товарных групп магазина "Дочки-Сыночки"
    def scrape_catalog(self, filter_name):

        # При каждом запуске скрипта импортируем категории товаров. Это не занимает
        # много времени, но зато список категорий всегда находится в актуальном
        # состоянии. Так же импортируем список брендов - это поможет в дальнейшей
        # обработке товарных позиций
        start_time = time.time()
        self._scrape_categories()
        print("Извлечение категорий товаров: {} мс".format(
            int(round((time.time() - start_time) * 1000))))

        # Осуществляем попытку получения данных по всем накопленным в базе
        # данных категориям.
        # При получении фактического списка подкатегорий для извлечения
        # данных следует исключать из обработки дочерние подкатегории,
        # т.к. в родительских должны быть определены все те же самые
        # товары
        self.db.process_categories(
            lambda rowid, url: self._scrape_the_showcase(rowid, url),
            self.so.scrape_categories_to_process,
            additional_condition=self._get_condition_by_name(filter_name),
            ignore_children=self.so.ignore_children, ignore_parents=self.so.ignore_parents)

        # Если установлен параметр извлечения дополнительной информации по ранее
        # извлечённым данным, осуществляем обработку страниц отдельных товаров.
        # Мы там ищем: постоянные ссылки на фотографии товаров, расширенный
        # список свойств для последующей классификации
        if self.so.scrape_details_by_count > 0:

            print("Формируются URL-адреса для обработки: {}".format(strftime("%H:%M:%S", localtime())))

            # Используем лямбда-функцию, чтобы собрать список product_id и URL
            # для их последующей обработки
            product_url_list = []
            self.db.process_each_product(
                lambda product_id, product_url: product_url_list.append((product_id, product_url)),
                self.so.scrape_details_by_count,
                additional_condition=self._get_condition_by_name(filter_name), commit_periodically=False)

            part_count = 100
            begin_index = 0
            while True:

                print("Загрузка html-страниц с {} по {}. Время: {}".format(begin_index + 1, begin_index + part_count,
                                                                           strftime("%H:%M:%S", localtime())))

                partial_urls_list = product_url_list[begin_index:begin_index + part_count]

                # Формируем pool рабочий потоков, задачей которых будет являться
                # загрузка html-страниц с данными товара
                pool = ThreadPool(4)

                # Результаты работы будут накапливаться в отдельном списке
                results = pool.map(async_download_html_page_ds, partial_urls_list)

                # Устанавливаем флаг завершения работы и ждём, когда
                # все рабочие потоки завершат свою работу
                pool.close()
                pool.join()

                print("Обработка: {}".format(strftime("%H:%M:%S", localtime())))

                for product_data in results:
                    if product_data.product_page is not None:
                        self._scrape_details(product_data.product_id, product_data.product_url,
                                             product_data.product_page)
                self.db.connection.commit()

                # Переходим к обработке следующего блока данных
                begin_index += part_count

                # Если все диапазоны URL-адресов обработаны, то завершаем работу
                if begin_index >= len(product_url_list):
                    break

            print("Извлечение деталей товаров завершено: {}".format(strftime("%H:%M:%S", localtime())))

    def _scrape_details_adapter(self, product):

        url = self.get_shop_site() + product[1]
        html_document = self.session.get(url).text
        return ProductData(product[0], product[1], html_document)

    # Функция извлекает дополнительные атрибуты из названия товара, ранее сохранённого
    # в базе данных
    def extract_attributes(self, filter_name):

        # Получаем условия отбора данных для дальнейшей обработки
        condition = self._get_condition_by_name(filter_name)
        if condition is None:
            logging.error('Ошибка: не удалось получить условия отбора данных')
            return

        if "LEGO" == filter_name:

            # В магазине "Дочки-Сыночки" перед названием товара может идти его подкатегория.
            # Например, для LEGO-наборов такой подкатегорией является название серии наборов
            # (City, Super Heroes, StarWars, и т.д.)
            # Для того, чтобы выделить название подкатегории из строки с названием товара
            # необходимо получить эталонный набор подкатегорий. Это можно сделать используя
            # фильтры отбора, размещённые в отдельной форме HTML-документа.
            # Для получения списка критериев отбора используется специальная функция -
            # _extract_filter_names()
            all_filters = self._extract_filter_names(self.db.get_category_url(condition))
            if all_filters is None or all_filters.get('all_Filter') is None:
                logging.error('Ошибка: не удалось выделить фильтры отбора данных')
                return

            series_short_list = []
            for title in all_filters['all_Filter']:

                prefix = "Lego"
                if 0 == title.find(prefix):
                    # Добавляем в список названий наборов сначала название,
                    # включающее слово "Lego" (Lego Princess), а затем
                    # без слова "Lego" (City). Это позволит надёжно
                    # исключать название серии из названия набора
                    series_short_list.append(title)
                    title = title[len(prefix):].strip()
                    series_short_list.append(title)

            # Осуществляем обработку ранее извлечённых данных о товарах магазина. В общем случае,
            # обработка предполагает извлечение дополнительных атрибутов из названия товара
            self.db.for_each_product(
                condition,
                lambda cat_id, cat_name, prod_id, prod_name:
                self._extract_lego_attrs(series_short_list, prod_id, prod_name))

        else:

            # Извлекаем полный список брендов, представленных в магазине
            # "Дочки-сыночки". Этот список находится по URL:
            # http://www.dochkisinochki.ru/brands/
            # Причина, по которой нельзя использовать список брендов
            # из фильтров отбора (all_filters['code_BRANDS']) состоит
            # в том, что для разных подкатегорий товаров используются
            # разные подмножества брендов и в большом количестве
            # случаев, бренд может не быть найден
            brands = self._extract_all_brands()

            # Issue #74. Костылим ошибки "Дочек-сыночков"
            brands.append('АЛЕКС ЮНИС ')
            brands.append('АЛЕКС ЮНИС,')

            self.extract_attr_count = 1

            # Загружаем дополнительный справочник названий моделей
            trademarks = self.db.get_brands_and_trademarks()

            # Issue #119. Определяем список вариаций применяемых к конкретной
            # категории товаров.
            filters = FilterCollection.Filter(self.get_shop_name(), filter_name)
            valuable_props = filters.get_variations_priority()
            if valuable_props is None:
                valuable_props = self.db.get_unified_variations()

            self.db.for_each_product(
                condition,
                lambda cat_id, cat_name, prod_id, prod_name:
                self._extract_common_attrs(cat_name, brands, prod_id, prod_name, trademarks, valuable_props))
        pass

    # Private-функция формирует фильтр, использующийся для извлечения данных
    # по названию соответствующего правила. Например, правило может называться
    # "LEGO" и эта функция сформирует SQL-условие, которое позволит отобрать
    # только LEGO-категории
    def _get_condition_by_name(self, filter_name):

        if "ALL" == filter_name:
            return ""  # Если фильтр не указан, то отбирать будем всё

        # В магазине "Дочки-сыночки" все LEGO-наборы находятся в одной категории
        elif "LEGO" == filter_name:
            return "Categories.group_name = 'Конструкторы Lego'"

        else:

            filters = FilterCollection.Filter(self.get_shop_name(), filter_name)
            return filters.get_conditions(self.db)

    # Private-функция осуществляет загрузку главного меню
    def _scrape_categories(self):
        try:

            html_document = self.session.get(self.get_shop_site()).text
            soup = BeautifulSoup(html_document, "html.parser")

            # Сначала выделяем товарные категории верхнего уровня:
            # <a class="shop-main-menu_item" id="mm1912" href="/icatalog/categories/kormlenie-pitanie/">
            # <div class="shop-main-menu_item-icon"
            #   style="background-image:
            #   url(/bitrix/components/custom/main_menu/images/icon_kormlenie-pitanie.png?v=1);">
            # </div> Питание и кормление </a>
            categories_dict = {}

            # В начале 2016 года, магазин "Дочки-Сыночки" расширил продуктовую линейку
            # товарами, которые не имеют прямого отношения к малышам и из мамам - появились
            # такие категории, как "Товары для Животных", "Бытовая техника", и т.д.
            # Отсекаем такие дополнительные категории. В версии от 29.03.2017, чужими
            # считались категории, которые находятся вне следующего тэга:
            #   <ul data-block="merchant-menu" data-merchant-code="dochkisinochki" >

            just_baby_categories = soup.find('ul', attrs={'data-block': 'merchant-menu',
                                                          'data-merchant-code': 'dochkisinochki'})
            if just_baby_categories is None:
                logging.error("Не найден блок основного меню магазина 'Дочки-сыночки'")
                return

            main_categories = just_baby_categories.findAll('a', attrs={'class': 'shop-main-menu_item'})
            for category in main_categories:

                if not category.has_attr('id'):
                    continue

                if not category.has_attr('href'):
                    continue

                category_name = category.getText().strip()

                # Полностью игнорируем главную категорию "Распродажа"
                if "/icatalog/categories/rasprodazha/" == category['href']:
                    logging.info("Игнорируем категорию 'Распродажа'")
                    continue

                # Сохраняем информацию о категориях верхнего уровня в базе данных,
                # а также сохраняем информацию об идентификаторах эти категорий для
                # создания связи между дочерними и родительскими элементами
                row_id = self.db.insert_category(category_name, category['href'], "0")
                categories_dict[category['id']] = row_id
                pass

            # <div class="shop-main-menu_submenu" id="sm_mm1912">
            # <div class="shop-main-menu_submenu-item">
            #       <a href="/icatalog/categories/pitanie/"
            #           class="shop-main-menu_submenu-item--title"> Детское питание </a>
            # ...
            #       <a href="/icatalog/categories/molochnye_smesi/"
            #           class="shop-main-menu_submenu-item--link">Молочные смеси</a>
            # </div>
            #   <div class="shop-main-menu_submenu-item">
            #       <a href=...
            #   </div>
            # </div>
            main_categories = soup.findAll('div', attrs={'class': 'shop-main-menu_submenu'})
            for category in main_categories:

                # По идентификатору элемента (id) можно найти идентификатор
                # записи в базе данных (row_id) меню верхнего уровня
                if not category.has_attr('id'):
                    continue

                parts_of_id = category['id'].split('_')
                if len(parts_of_id) < 2:
                    continue

                if parts_of_id[1] not in categories_dict:
                    continue

                main_menu_parent_id = categories_dict[parts_of_id[1]]

                # Выделяем все подкатегории в полном списке категорий
                sub_categories = category.findAll('div', attrs={'class': 'shop-main-menu_submenu-item'})
                for sub_category in sub_categories:

                    # В каждой подкатегории есть заголовок - выделяем его и сохраняем в базе данных
                    sub_category_title = sub_category.find('a',
                                                           attrs={'class': 'shop-main-menu_submenu-item--title'})
                    category_name = sub_category_title.getText().strip()

                    if not sub_category_title.has_attr('href'):
                        continue

                    row_id = self.db.insert_category(category_name, sub_category_title['href'],
                                                     str(main_menu_parent_id))

                    # Все остальные категории будем привязывать к этому заголовку
                    sub_menu_parent = str(main_menu_parent_id) + "." + str(row_id)

                    third_level_categories = sub_category.findAll('a', attrs={
                        'class': 'shop-main-menu_submenu-item--link'})
                    for element in third_level_categories:

                        if not element.has_attr('href'):
                            continue

                        # Issue #91. Все в списке есть закольцованная ссылка, т.е. ссылка
                        # с текстом "Показать все" и с URL родительской подкатегории,
                        # то извлекаем полный список суб-подкатегорий с дополнительной
                        # страницы
                        element_title = element.getText().strip()
                        if element_title.upper() == 'ПОКАЗАТЬ ВСЕ':

                            # Не осуществляем извлечение подкатегорий только в том случае,
                            # если параметр --ignore_subcats явно указан при запуске скриптов
                            if not self.so.ignore_subcats:
                                logging.warning('Извлекаются подкатегории из дополнительной'
                                                ' страницы "{}"'.format(element['href']))
                                self._import_subcategories(element['href'], sub_menu_parent)
                        else:

                            # Добавляем обычную суб-подкатегорию
                            self.db.insert_category(element_title, element['href'], sub_menu_parent)
                pass

        except requests.exceptions.RequestException as e:
            logging.error('Сетевая подсистема. Катастрофический сбой в сетевой подсистеме: {}'.format(e))
            self.db.connection.commit()
            exit()

    # Private-функция извлекает дополнительные названия подкатегорий
    def _import_subcategories(self, category_url, material_path):

        try:
            url = self.get_shop_site() + category_url
            html_document = self.session.get(url).text
            soup = BeautifulSoup(html_document, 'html.parser')

            category_tree = soup.find('li', attrs={'class': 'tree_cat_show'})
            if category_tree is None:
                logging.error("Отсутствует секция 'tree_cat_show' в 'Дочки-сыночки'")
                return

            plain_list = category_tree.find('ul', attrs={'class': 'filSpis'})
            if plain_list is None:

                # На странице может существовать дерево с более сложной структурой.
                # В это случае, необходимо сначала извлечь из списка родительский
                # элемент, сформировать его Materialized Path, а затем обработать
                # все дочерние элементы
                for parent_element in category_tree.findAll('a', attrs={'class': 'filterTree_сat'}):

                    category_name = parent_element.getText().strip()
                    current_category_path = self.db.get_materialized_path_by_name(category_name)
                    if current_category_path is None:
                        logging.error('Не удалось найти Materialized Path для "{}"'.format(current_category_path))
                        continue

                    for subcat in category_tree.findAll('a', attrs={'class': 'filterTree_subCat'}):

                        if not subcat.has_attr('href'):
                            logging.error('Отсутствует ссылка на суб-подкатегорию "{}"'.format(
                                subcat.getText().strip()))
                            continue

                        self.db.insert_category(subcat.getText().strip(), subcat['href'], current_category_path)

            else:

                # Отрабатываем простой случай, в котором на странице содержится лишь
                # список суб-подкатегорий - мы извлекаем их и добавляем в качестве
                # дочерних, как указанной ранее подкатегории
                subcategories = plain_list.findAll('li', attrs={'class': 'filter_element'})
                for subcategory in subcategories:

                    category_ref = subcategory.find('a', attrs={'class': 'href_no_script'})
                    if category_ref is None:
                        logging.error("Ошибка поиска элемента в доп.файле (критерий - href_no_script)")
                        continue

                    if not category_ref.has_attr('href'):
                        logging.error("Ошибка поиска элемента в доп.файле (критерий - href)")
                        continue

                    self.db.insert_category(category_ref.getText().strip(), category_ref['href'], material_path)

        except requests.exceptions.RequestException as e:
            logging.error('Сетевая подсистема. Катастрофический сбой в сетевой подсистеме: {}'.format(e))
            self.db.connection.commit()
            exit()
        pass

    # Private-функция выполняющая задачу получения данных по одной
    # товарной категории. Способ отображения информации о товарах
    # разумно назвать "витриной" (a showcase)
    def _scrape_the_showcase(self, category_id, category_url):

        # Для того, чтобы избежать рекурсии, заставляем метод разбора
        # отдельной страницы возвращать ссылку на следующую страницу
        current_page = category_url

        while True:

            logging.info('Извлекаем данные по ссылке: ' + current_page)

            url = ScrapingHelper.concat_path(self.get_shop_site(), current_page)
            html_page = self.session.post(url, data={'page_size': 51})

            self.db.begin_products_insertion()
            current_page = self._scrape_the_page(category_id, html_page.text)
            self.db.end_products_insertion()

            if len(current_page) == 0:
                break

    # Private-функция разбора отдельной страницы с товарами
    def _scrape_the_page(self, category_id, html_page):

        # ОПТИМИЗАЦИЯ ПО СКОРОСТИ:
        # Выделяем блок текста, который начинается и заканчивается
        # определёнными div(ами). Это нужно для того, чтобы уменьшить
        # объём обрабатываемых данных.
        # С целью сохранения совместимости, используюся два варианта
        # сужения области обработки документов, для старого и нового
        # вариантов дизайна страниц
        begin = html_page.find('<div class="esk esk_n')
        if -1 == begin:
            begin = html_page.find('<div class="itemsWr eskiz">')
        end = html_page.find('<p class="advantage">', begin)

        # Удаляем из документа все тэги <br> и </br> - они расставлены
        # в html-документе хаотично и нарушают разбор документа
        sub_html = html_page[begin:end].replace('<br>', '').replace('</br>', '')
        soup = BeautifulSoup(sub_html, 'html.parser')

        # Выделяем товарные позиции
        all_products = soup.findAll('div', attrs={'class': 'esk_n'})
        for product in all_products:

            name_tag = product.find('span', attrs={'class': 'h4_span_plist'})
            if name_tag is None:
                logging.error("Отсутствует информация о названии продукта")
                continue

            name = name_tag.getText().strip()

            href_tag = name_tag.find('a', attrs={'href': True})
            if href_tag is None:
                logging.error("Отсутствует информация о ссылке на страницу атрибутов товара")
                continue

            href_details = href_tag['href']

            # Класс 'yelloyprice' определяет Html-разметку, которая связана с ценой.
            # Вместе с тем, этот класс может применятся несколько раз и комбинироваться
            # с классом 'nact' (not active), который указывает на то, что товар
            # отсутствует в продаже в данный момент.
            # В текущей реализации, если товар отсутствует на продаже у него
            # устанавливается нулевая цена. Можно было бы дублировать эту
            # информацию явным образом, например, добавляя свойство 'availability',
            # или 'nact', но это не создаёт каких-либо явных преимуществ для обработки
            price_tag = product.find('div', attrs={'class': 'yelloyprice'})
            if price_tag is None:
                logging.error("Отсутствует информация о цене товара")
                continue

            price = ScrapingHelper.parse_amount(price_tag.getText())

            # Ищем базовую цену товара, т.е. цену без применения скидки.
            price_tag_old = product.find('div', attrs={'class': 'old-price'})
            if price_tag_old is not None:

                old_price = ScrapingHelper.parse_amount(price_tag_old.getText())

            # ISSUE #51: Следует игнорировать обобщённые товарные позиции, у которых нет
            # уникального номера товара в URL, например:
            #   http://www.dochkisinochki.ru/icatalog/models/fd-design-kolyaska-2-v-1-mamba/
            # В конкретных вариациях, ссылки содержат уникальный номер, например:
            #   http://www.dochkisinochki.ru/icatalog/products/775298/#
            #   http://www.dochkisinochki.ru/icatalog/products/775295/#

            if re.match('.*/products/\d{3,12}/.*', href_details) is None:
                logging.warning('URL описания товара не содержит уникального идентификатора: {}'.format(href_details))
                continue

            # Issue #38. Проверяем, а добавляли ли мы данные о url с детальной
            # информацией о товаре в базу данных, или ещё не добавляли
            if href_details in self.duplication_check_set:

                # Логи временно отключены, т.к. объём дублирования, по факту, огромный
                '''logging.error('Товар "{}" с url "{}" повторно найден в категории {}'.format(name, href_details,
                                                                                            category_id))'''
            else:

                self.duplication_check_set.add(href_details)
                self.db.insert_product(category_id, name, price, href_details, price_old=old_price)
                self.showcase_products_count += 1

        # Paginator на сайте "Дочки-Сыночки" выглядит таким образом:
        # <div class="pagination-new">
        # <div class="select" href = "/icatalog/categories/10001/" >1</div>
        # <a href="/icatalog/categories/10001/?PAGEN_1=2" > 2 </a>
        # <a href="/icatalog/categories/10001/?PAGEN_1=3" > 3
        # ...
        # Т.е. нужно найти div-элемент класса "select" внутри
        # <div class="pagination-new"> и получить номер текущей страницы
        # затем, следует поискать ссылку на следующую страницу. Если
        # она отсутствует, то следует прекратить поиск

        paginator = soup.find('div', attrs={'class': 'pagination-new'})
        if paginator is not None:

            selected = paginator.find('div', attrs={'class': 'select'})
            if selected is not None:

                page_number = int(selected.getText().strip())
                if page_number == 0:
                    logging.error("Ошибка определения номера текущей страницы")
                    return

                all_hrefs = paginator.findAll('a')
                for href in all_hrefs:

                    if not href.has_attr('href'):
                        logging.error("Отсутствует атрибут href")
                        return

                    try:
                        link_to_page_number = int(href.getText().strip())
                        if link_to_page_number == page_number + 1:
                            # Найдена ссылка на следующую страницу
                            return href['href']

                    except ValueError:
                        # Ничего не делать! На кнопке может быть размещён
                        # текст, например "prev", или "next"
                        pass

        # Возвращаем пустое значение, т.к. ссылка на следующую страницу
        # не была найдена
        return ""

    # Private-функция, разбирает JSON-структуру и выделяет вариации товара
    def _insert_product_variations(self, category_id, json_list, price, old_price):

        if json_list is None or len(json_list) == 0:
            return

        for product_obj in json_list:

            if product_obj.get('JSON') is None or len(product_obj['JSON']) == 0:
                continue

            try:

                variation = json.loads(product_obj['JSON'])
                if variation is not None:

                    if variation.get('NAME') is None or variation.get('URL') is None:
                        continue

                    name = variation['NAME']
                    url_details = variation['URL']
                    self.db.insert_product(category_id, name, price, url_details, price_old=old_price)
                    self.showcase_products_count += 1

            except TypeError:
                pass

            except ValueError:  # Это базовый класс исключения json.decoder.JSONDecodeError (#69)
                pass

    # Private-функция разбора страницы отдельного товара
    def _scrape_details(self, product_id, product_url, html_document):

        # Удаляем ранее извлечённые свойства товара
        self.db.delete_properties_and_photos(product_id)

        # Сохранять свойства будем через специальный кэширующий механизм
        self.db.begin_props_insertion()

        success = False
        try:

            # Уменьшаем объём данных, обрабатываемых компонентом
            # BeautifulSoup, отрезая заведомо бесполезные части
            # HTML-документа
            soup = self._cook_reduced_soup(html_document)

            # Получаем список всех изображений, связанных с товаром в магазине
            all_large_images = soup.findAll('div', attrs={'class': ['largeImg', 'smImg']})
            for image_block in all_large_images:

                # Среди изображений есть главное (largeImg) и дополнительные (smImg)
                is_main_photo = 0
                if image_block.has_attr('class'):
                    if 'largeImg' in image_block['class']:
                        is_main_photo = 1

                # Оригинал изображения хранится в поле 'data-source', в остальных
                # полях хранятся ссылки на кэшированные изображения разных размеров
                html_tag = image_block.find('a', attrs={'data-source': True})
                if html_tag is None:
                    continue

                file_name = html_tag['data-source']
                if len(file_name) > 0:

                    # Если в имени файла отсутствует префикс http://, то добавляем
                    # необходмые данные для того, чтобы сделать ссылку полной
                    if file_name.find(R'http://') != 0:

                        # Issue #63. Если оригинал фотографии не доступен, то используем
                        # отмасштабированное изображение из кэша
                        logging.warning('Не полное имя ресурса ("Дочки-сыночки"): "{}"'.format(file_name))
                        if html_tag.has_attr('href'):
                            file_name = html_tag['href']
                            logging.warning('Изображение товара заменяется на: "{}"'.format(file_name))

                    # Сохраняем ссылку на оригинальное изображение товара в базе данных
                    success = self.db.add_photo_of_product(product_id, file_name, successful=True,
                                                           is_main=is_main_photo)

            self._scrape_properties(soup, product_id, product_url, self.DEFAULT)

            # Пытаемся извлечь "хлебные крошки" (breadcrumb), которые, теоретически,
            # позволяют улучшить классификацию товара
            breadcrumb = ""

            breadcrumb_block = soup.find('div', attrs={'class': 'breadcrumb_product'})
            if breadcrumb_block is not None:
                for name in breadcrumb_block.findAll('span', attrs={'itemprop': 'title'}):

                    if len(breadcrumb) > 0:
                        breadcrumb += "/"
                    breadcrumb += name.getText().strip().upper()

            if len(breadcrumb) > 0:
                self.db.include_prop(product_id, 'breadcrumb', breadcrumb)

            # Issue #143. Определяем, есть ли товар в наличии, или нет
            instock = 1
            instock_tags = soup.find('div', attrs={'class': 'productStick_info_basket_button'})
            if instock_tags is not None:

                button_tags = instock_tags.find('button', attrs={'data-id': True})
                if button_tags is not None:

                    if "Нет в наличии" == button_tags.getText().strip():
                        instock = 0

            self.db.update_instock_status(product_id, instock)

            # Сохраняем "детальную информацию о товаре" в базе данных в HTML-формате,
            # с тем, чтобы можно было использовать существующую HTML-разметку
            description_block = soup.find('div', attrs={'class': 'description_text'})
            if description_block is not None:

                wrapper = description_block.find('p', attrs={'itemprop': 'description'})
                if wrapper is not None:
                    # Для того, чтобы убрать тэг 'div', в который обёрнут значимый для
                    # покупателя текст, выполняем операцию the list comprehension
                    [x.extract() for x in wrapper.findAll('div', attrs={'itemprop': 'description'})]
                    partial_html = ''.join(str(c) for c in wrapper.contents)
                    self.db.update_product_description(product_id, partial_html)

            # Issue #115. Осуществляем поиск специфического класса "sizes_block"
            # у тэга с id "productStickSizeAnchor" и выделяем значимые вариации товара.
            sizes_variants = soup.find('div', attrs={'class': 'sizes_block', 'id': 'productStickSizeAnchor'})
            if sizes_variants is not None:

                properties = []
                for variant in sizes_variants.findAll('span', attrs={'j-data-hr': True}):

                    property_href = variant['j-data-hr']
                    if property_href is None or len(property_href) == 0:
                        continue

                    pos = property_href.find('#topitem')
                    if pos >= 0:
                        property_href = property_href[:pos]

                    data_sid = variant.find('span', attrs={'data-sid': True})
                    if data_sid is None:
                        continue

                    variation_name = data_sid.getText().strip()

                    product_variation = variation_name
                    product_variation += '{'
                    product_variation += property_href
                    product_variation += '}'

                    # Issue #149. Для каждой вариации товара извлекаем собственный
                    # список свойств. Исключение - главная страница товара
                    if self.so.extract_var_props:
                        if 'currentSize' not in variant['class']:

                            variation_url = self.get_shop_site() + property_href
                            variation_html = self.session.get(variation_url).text
                            variation_soup = self._cook_reduced_soup(variation_html)
                            self._scrape_properties(variation_soup, product_id, property_href, variation_name)

                    properties.append(product_variation)

                if len(properties) > 0:
                    self.db.include_prop(product_id, sizes_variants['id'], ';'.join(properties))

        except requests.exceptions.RequestException as e:
            # Ошибка выполнения запроса может возникнуть при извлечении
            # вариаций товара, см. self.session.get()
            logging.error('Сбой в сетевой подсистеме: {}'.format(e))
            self.db.connection.commit()

        # Если по каким-то причинам выделить ссылку на изображение не удалось, сохраняем
        # информацию об этом в базе данных для последующего разбора. Так же важно исключить
        # эту запись из повторной обработки, т.е. добавив ссылку на Products.rowid
        # из поля product_id таблицы Photos
        if not success:
            self.db.add_photo_of_product(product_id, product_url, successful=False)

        # Осуществляем пакетную запись в базу данных всех свойств сразу
        self.db.end_props_insertion()

    @staticmethod
    def _cook_reduced_soup(html_document):

        begin_pos = html_document.find('<div class="breadcrumb')
        end_pos = html_document.find('<div id="reviews">')

        if begin_pos < 0 or end_pos < 0:

            return BeautifulSoup(html_document, 'html.parser')

        else:

            return BeautifulSoup(html_document[begin_pos:end_pos], 'html.parser')

    # Функция осуществляет выделение отвдельных свойст товара, с учётом
    # его вариации
    def _scrape_properties(self, soup, product_id, product_url, variation):

        # В магазине "Дочки-сыночки" описание свойств товара идёт по следующей схеме:
        #
        # <div class="art">
        # <p class="beforedots">Бренд</p>
        # <p class="dots"></p></div>
        # <p class="manufact"><a href='/brands/merries/'"><span itemprop="brand">Merries</span></a></p>
        # </div>
        properties_block = soup.find('div', attrs={'class': 'artnumber'})
        if properties_block is None:

            # Не удалось найти блок детальной информации о товарах,
            # поскольку в документе отсутствует тэг 'artnumber'.
            # Поскольку "Дочки-сыночки" переходят на новый дизайн, то
            # такое может случится. Пробуем альтернативный подход:
            # <div class="description_short">...
            # <span itemprop="sku">00100220807</span>
            # <span class="brand">Бренд:
            # <a href="/brands/libero/" itemprop="brand">Libero</a></span>
            # В действительности, интерес представляет не только бренд, но
            # и другие поля, например: "Состав"

            properties_block = soup.find('div', attrs={'class': 'description_short'})
            if properties_block is None:

                message = "Внимание: не удалось найти ни блок 'artnumber', ни блок 'description_short'" \
                          " со свойствами товара {}, product_id = {}".format(product_url, product_id)
                logging.warning(message)

            else:

                for line in properties_block.findAll('span', attrs={'class': True}):

                    all_words = line.getText().split(':')
                    if len(all_words) != 2:

                        logging.error('Ошибка выделения свойства товара')
                    else:
                        property_name = all_words[0].strip()
                        property_value = ''.join(str(value) for value in all_words[1])
                        if len(property_name) > 0 and len(property_value) > 0:
                            property_value = ScrapingHelper.clean_property_value(property_value)
                            self.db.include_prop(product_id, property_name, property_value, variation)
        else:

            # todo: Этот код похож на устаревший. Проверить и удалить

            # Выделяем свойства из раздела 'artnumber' - это один из способов
            # представления свойств товара
            for line in properties_block.findAll('div', attrs={'class': 'art'}):

                paragraphs = line.findAll('p')
                if len(paragraphs) != 3:
                    logging.error('Ошибка выделения свойства товара')
                else:
                    property_name = paragraphs[0].getText().strip()
                    property_value = ''.join(str(value) for value in paragraphs[2].contents)
                    if len(property_name) > 0 and len(property_value) > 0:
                        property_value = ScrapingHelper.clean_property_value(property_value)
                        self.db.include_prop(product_id, property_name, property_value, variation)

        # Дополнительно обработываем список "Характеристики". разметка:
        # <div class="tth_text collapse">
        #   <table>
        #     <tr>
        #       <td class="property">Вид</td>
        #       <td class="property_desc">одноразовые</td>
        #     </tr>
        #   </table>
        properties_block = soup.find('div', attrs={'class': 'tth_text'})
        if properties_block is not None:
            for tr_content in properties_block.findAll('tr'):

                td_key = tr_content.find('td', attrs={'class': 'property'})
                td_value = tr_content.find('td', attrs={'class': 'property_desc'})

                if td_key is not None and td_value is not None:

                    property_name = td_key.getText().strip()
                    property_value = ''.join(str(value) for value in td_value.contents)

                    if len(property_name) > 0 and len(property_value) > 0:

                        if len(property_value) > 510:

                            # Слишком длиные значения поля не могут быть сохранены в базу данных -
                            # нужно искать причины, по которым такое случается
                            message = 'Длина свойства превышает 510 байт. "{}"="{}"'.format(
                                property_name, property_value)
                            logging.error(message)

                            message = 'Длина = {}. Url = {}'.format(
                                len(property_value), product_url)
                            logging.error(message)

                        else:

                            property_value = ScrapingHelper.clean_property_value(property_value)
                            self.db.include_prop(product_id, property_name, property_value, variation)
        pass

    # Функция выделяет полный список брендов с отдельной страницы сайта. Полный
    # список нужен для повышения качества обработки названий товаров.
    # Альтернативным решением могло бы быть выделение подкатегорий внутри категории
    # и их последовательная обработка с использованием наборов фильтров актуальных
    # для каждой из подкатегорий
    def _extract_all_brands(self):

        brands = []

        try:

            # Формируем адрес страницы для извлечения брендов
            url = self.get_shop_site() + '/brands/'
            html_document = self.session.get(url).text
            soup = BeautifulSoup(html_document, 'html.parser')

            for element in soup.findAll('div', attrs={'class': 'cataloglist_group_item_link'}):
                link = element.find('a', attrs={'href': True})
                brand_href = link['href']

                # Игнорируем ссылки, в которых нет подстроки "/brands/"
                if re.match('^/brands/.*', brand_href) is not None:

                    name = link.getText().strip().upper()

                    # Странным образом, названия брендов стали заканчиваться на
                    # точку с запятой. Убираем этот символ
                    if len(name) > 0 and name[-1:] == ';':
                        name = name[:-1]

                    # Issue #115. Унифицируем название брендов - вокруг
                    # амперсанда (&) не должно быть пробелов
                    name = ScrapingHelper.normalize_brand(name)

                    # Issue #30. В Python реализован особенный подход, в котором
                    # строки содержат дополнительные, не визуализируемые символы,
                    # например символ backslash перед апострофом. Выкосить такие
                    # символы весьма сложно и, к тому же, сравнивать строки с
                    # подвергшиеся обработке с символами, не подвергшимся обработке
                    # нельзя - "JOHNSON'S" и "\\JOHNSON'S" дадут результат -
                    # ЭТО РАЗНЫЕ СТРОКИ.
                    # Чтобы избежать ошибок сравнения, необходимо применить
                    # метод html.unescape()
                    #
                    # todo: подумать о применении функции repr()
                    if name.find("'") >= 0:
                        name = name.replace("'", "&rsquo;")
                        name = html.unescape(name)

                    # Добавляем в конце названия бренда пробел с тем, чтобы
                    # уменьшить количество ошибок разбора полного
                    # названия продукта, из-за частичного совпадения
                    # названий брендов
                    brands.append(name + ' ')

                    # Для случаев, в которых сразу за брендом идёт запятая,
                    # добавляем ещё и альтернативный вариант.
                    # Причина, по которой мы не работаем с отдельными словами
                    # без символов разделителей состоит в том, что бренды могут
                    # состоять из нескольких слов, например: "Daka Baby",
                    # "Color Puppy", "Ben 10", "Ever Clean"
                    brands.append(name + ',')

            # Сортируем бренды по длине (сначала идут наиболее длинные) с тем,
            # чтобы при сравнении сначала можно было найти более длинные варианты
            # написания брендов. Например, важно найти полный бренд "Paul Hartmann",
            # а не сокращённое название "Hartmann"
            brands.sort(key=lambda s: -len(s))

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме: {}'.format(e))
            self.db.connection.commit()

        return brands

    # В магазине "Дочки-Сыночки" внутри категории могут находится подкатегории.
    # Данная функция позволяет извлекать подобные подкатегории, а так же другие списка
    # атрибутов товаров, по которым возможно выполнять фильтрацию, такие как
    # бренд, цвет, размер, и т.д.
    def _extract_filter_names(self, category_url):

        collection = {}

        try:

            # Формируем адрес страницы для извлечения скрытых категорий
            url = self.get_shop_site() + category_url
            html_document = self.session.get(url).text
            soup = BeautifulSoup(html_document, 'html.parser')

            # Область поиска ограничиваем формой критериев отбора данных
            form_block = soup.find('form', attrs={'id': 'filter_form'})
            if form_block is not None:

                # В блоке фильтрации информации может быть определено несколько групп параметров
                for filter_data in form_block.findAll('div', attrs={'class': 'filter_data'}):

                    criteries_block = filter_data

                    # Если была выделена основная категория, то уменьшаем scope, поскольку
                    # в HTML-разметке в блок 'all_Filter' включены вообще все критерии, а не
                    # только те, которые входят в подкатегории товаров
                    if filter_data.has_attr('id') and filter_data['id'] == 'all_Filter':
                        criteries_block = filter_data.find('ul', attrs={'class': 'filSpis'})

                    # Выделяем названия отдельных критериев фильтрации данных
                    names = []
                    for filter_element in criteries_block.findAll('li', attrs={'class': 'filter_element'}):

                        link = filter_element.find('a', attrs={'class': 'href_no_script'})
                        if link is None:
                            continue

                        if not link.has_attr('href'):
                            continue

                        if link['href'] == category_url:
                            continue

                        # Добавляем очередной реквизит в список
                        names.append(link.getText().strip().upper())

                    # Добавляем список выделенных параметров фильтрации и связываем его
                    # с название группы параметров
                    if len(names) > 0 and filter_data.has_attr('id'):
                        collection[filter_data['id']] = names

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме: {}'.format(e))
            self.db.connection.commit()

        return collection

    # Private-функция извлекает дополнительные атрибуты из названия продукта
    def _extract_lego_attrs(self, series_names, product_id, product_name):

        # Если название товара начинается со слова "Конструктор", то исключаем
        # его из названия
        prefix = "Lego Конструктор"
        if 0 == product_name.find(prefix):
            product_name = product_name[len(prefix):].strip()

        for title in series_names:

            # Название набора в магазине "Дочки-сыночки" идёт в конце списка
            index = product_name.find(title)
            if index > -1:
                product_name = product_name[:index].strip()
                if len(product_name) > 0:
                    # Добавляем сокращённое название товара в базу данных
                    self.db.insert_property(product_id, 'playset_name', product_name)

                # Добавляем название серии
                self.db.insert_property(product_id, 'series', title)
                return

        # Если выделить сокращённое название товара не удалось, то добавляем
        # в базу данных полное название товара
        self.db.insert_property(product_id, 'playset_name', product_name)

    # Private-функция извлекает дополнительные атрибуты из названия продукта
    def _extract_common_attrs(self, category_name, brands, product_id, product_name, trademarks, valuable_props):

        product_name = product_name.replace('  ', ' ')

        # Issue #74. В "Дочках-сыночках", в названии товара часто встречаются
        # символы-паразиты, которые значительно ухудшают качество данных
        product_name = product_name.replace('’', '').replace('`', '')

        # Issue #115. Убираем пробелы вокруг амперсанда (&) с тем, чтобы
        # успешно выделять бренд из названия продукта
        product_name = ScrapingHelper.normalize_brand(product_name)

        # Поскольку выделение товаров может осуществляться достаточно долго,
        # выводим информацию о процессе выделения товаров
        if self.extract_attr_count % 500 == 0:
            print("Выделяются атрибуты товара №{}".format(self.extract_attr_count))
            self.db.connection.commit()

        self.extract_attr_count += 1

        # Поскольку у продукта может быть сразу несколько свойств/атрибутов, то
        # имеет смысл пакетировать операции, т.е. объединять несколько INSERT-ов
        # в один. В MySQL подобная операция может быть выполнена посредством
        # конструкции INSERT INTO ... ON DUPLICATE KEY UPDATE
        self.db.begin_props_insertion()

        # Сохраняем официальную категорию товара
        self.db.include_prop(product_id, 'category', category_name)

        # Начинаем обработку имени продукта:
        # - преобразовываем к верхнему регистру
        # - убираем двойные кавычки
        product_name = product_name.upper().replace('"', '')

        # Issue #98. В базе данных брендов проверяются варианты '[Brand],' и '[Brand][пробел]'.
        # Это деляется для того, чтобы избежать не корректного выделения бренда при частичном
        # совпадении названия. Если модель указана, то за брендом всегда идёт либо пробел,
        # либо запятая, но если модель не указана, то алгоритм будет давать сбой.
        # Чтобы избежать сбоя, добавляем в конец названия продукта пробел
        product_name += ' '

        # Разделяем строку на составляющие: тип продукта, бренд и название. Варианты:
        # - КОЛЯСКА EXPANDER MONDO ECCO 2 В 1
        # - КОНВЕРТ ДЕМИСЕЗОННЫЙ LEADER KIDS КОКОН
        # Иногда на вход могут поступать строки, которые сложнее обрабатывать, например:
        # - Adamex Подсветка светодиодная для коляски 2 шт
        # Adamex - это бренд, "Подсветка светодиодная для коляски" - это тип товара.
        # Похоже на то, что тип товара и название товара совпадают.

        for brand_index, brand in enumerate(brands):

            index = product_name.find(brand)
            if index > -1:

                # Встречаются случаи, в которых "кривой" бренд может развалить разбор
                # строки с описанием товара. Необходимо проверять, что выделен именно
                # бренд, а не часть другого слова, которое похоже на название бренда.
                if index > 0 and product_name[index - 1] != ' ':
                    continue

                # Бренд может состоять из нескольких слов, что мешает применять простой
                # поиск. Поиск осуществляется по всему описанию товара, при этом
                # используются маски "[Бренд] " и "[Бренд],".
                # Удаляем запятую, которая в соответствии с алгоритмом может находится
                # в названии бренда
                brand = brand.replace(',', '')

                # Важный нюанс: в массиве брендов, каждый элемент заканчивается
                # дополнительным пробелом - это сделано для того, чтобы повысить
                # точность поиска брендов, т.к. названия определённых бренда
                # могут совпадать с частью обычных слов, например: "Alex", "Always",
                # "Asi", "Cow", "Big", "Evo", и.т.д.
                # В нашем случае, нас спасает тот факт, что тип товара, чаще всего
                # указан на русском языке, а бренд - на английском
                self.db.include_prop(product_id, 'Бренд', brand.strip())

                # Если есть тип товара, то сохраняем информацию о нём в базу данных
                if index > 0:
                    text_before = product_name[:index].strip().upper()
                    self.db.include_prop(product_id, 'product_type', text_before)

                # Какую-то информацию сохраняем и о названии товара, в комбинации с типом
                # товара, эта информация может оказаться вполне полезной
                text_after = product_name[index + len(brand):].strip().upper()
                if text_after is not None and len(text_after) > 0:

                    # Issue #71, #92. Осуществляем выделение свойств товаров с помощью
                    # унифицированного процесса
                    text_after, results = ScrapingHelper.find_unified_attrs(text_after, self.db.unified_variations)
                    if results is not None and len(results) > 0:

                        model_extension = self.db.insert_prop_variations(product_id, results, valuable_props)
                        if len(model_extension) > 0:
                            if len(text_after) > 0:
                                text_after += ' '
                            text_after += model_extension

                    # Пытаемся выделить модель из строки описания товара, используя
                    # специализированный справочник
                    if trademarks.get(brand) is not None:
                        for model in trademarks[brand]:

                            pos = text_after.find(model)
                            if pos >= 0:
                                if len(text_after) > len(model):
                                    text_after = model
                                break

                    self.db.include_prop(product_id, 'short_name', text_after)

                else:
                    self.db.include_prop(product_id, 'short_name', '')

                # Теперь, чтобы ускорить процесс, перемещаем найденный бренд в начало
                # списка - это должно ускорить дальнейшую обработку данных. Слишком
                # часто это делать не нужно, т.к. тогда затраты на перемещения элемента
                # в начало списка будут очень высокими
                if brand_index > 20:
                    brands.insert(0, brands.pop(brand_index))

                # Осуществляем пакетную запись в базу данных всех свойств сразу.
                # Актуально только для MySQL
                self.db.end_props_insertion()
                return

            pass

        # Если выделить сокращённое название товара не удалось, то добавляем
        # в базу данных полное название товара
        self.db.include_prop(product_id, 'short_name', product_name)

        # Осуществляем пакетную запись в базу данных всех свойств сразу
        self.db.end_props_insertion()

        # todo: Случаи, в которых бренд не был распознан нужно отрабатывать особо.
        # Либо данные мусорные, либо алгоритм не достаточно хорош
        message = "В товаре '{}' не распознан бренд".format(product_name)
        logging.error(message)
        return

    # Public-функция, помещает в коллекцию append_to информацию о товарах
    # отобранных по фильтру filter_name, в иерархическом, структурированном виде
    def export_products(self, filter_name, append_to):

        # Выгружаем всю информацию по продуктам из базы данных по указанному фильтру
        if "LEGO" == filter_name:

            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id, cat_name, prod_id, prod_name: self._export_lego_playset(prod_id, append_to))

        else:

            # Перед началом экспорта данных получаем полный список фотографий
            # и свойств для каждого товара в обрабатываемом магазине
            self.db.preload_data()

            # Issue #97. См. комментарий в scraper-е "Детского мира"
            old_random_product_type = self.so.random_product_type

            filter_obj = FilterCollection.Filter(self.get_shop_name(), filter_name)
            if filter_obj.is_unique_each_product():
                self.so.random_product_type = True

            # Issue #145. Формируем таблицу перекодировки артикулов
            complex_articles_set = self._prepare_articles_dict(filter_name)

            # Осуществляем непосредственное формирование товаров, включая
            # ценовые предложения, с группировкой
            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id, cat_name, prod_id, prod_name:
                    self._export_products(prod_id, append_to, complex_articles_set),
                include_outdated=True, cache_product_fields=True
            )

            self.so.random_product_type = old_random_product_type

            # Удаляем из памяти предварительно загруженные данные
            self.db.clear_preloaded_data()

    # Функция осуществляет подготовку словаря трансляции артикулов
    # для решения проблемы с группировкой товаров в ряде категорий.
    # См. Issue #145
    def _prepare_articles_dict(self, filter_name):
        """
        :param str filter_name: условное имя категории для которой формируются данные
        :return: set
        """

        articles_dict = {}
        self.db.for_each_product(
            self._get_condition_by_name(filter_name),
            lambda cat_id, cat_name, prod_id, prod_name: self._collect_article(prod_id, articles_dict),
            include_outdated=True, cache_product_fields=True
        )

        # См. комментарий к реализации алгоритма в "Кораблике"!
        unique_articles = []
        for key in list(articles_dict.keys()):
            if len(articles_dict[key]) > 1:
                for article in articles_dict[key]:
                    if article not in unique_articles:
                        unique_articles.append(key + '_' + article)

        return set(unique_articles)

    def _collect_article(self, product_id, articles_dict):

        # Извлекаем описание товара из базы данных
        product = ProductContainer(self.db, product_id, self.get_shop_name())

        # Ищем в свойствах товара внутренний артикул
        internal_article = product.get_first_property_from_offer('Артикул')
        if internal_article is None:
            return

        # Исключаем дополнительную обработку свойств товара, т.к.
        # для дальнейшей обработки используются только бренд, тип
        # продукта, название модели и внутренний артикул магазина
        self._update_product(product, modify_everything=False)

        # Подготовливаем параметры, которые будут использованы для формирования артикула
        product_type = product.get_property('product_type')
        brand = product.get_brand()

        # В магазине "Дочки-Сыночки" в строке описания товара, сразу за названием
        # идут атрибуты товара, например, цвет изделия. Для корректного выполнения
        # классификации, имя продукта имеет смысл отделять от атрибутов
        model_name = self._correct_model_name(brand, product.get_property('short_name'))

        article = ScrapingHelper.gen_article(product_type, brand, model_name)

        if articles_dict.get(article) is None:
            articles_dict[article] = []

        else:

            # Добавляем в список только уникальные "внутренние артикулы"
            if internal_article in articles_dict[article]:
                return

        articles_dict[article].append(internal_article)

    # Функция уточняет название товара, основываясь на официальном списке
    # торговых марок (trademarks).
    # Это нужно для того, чтобы выделить из полученного "сырого" названия
    # (например, "ESPRESSO FOR OVER"), реальное название товара ("ESPRESSO").
    def _correct_model_name(self, brand, model_name):

        if brand is not None and model_name is not None:

            if self.trademarks is not None:
                if self.trademarks.get(brand) is not None:

                    # Пробегаемся по всему списку торговых марок (из кэша) и ищем совпадения -
                    # если название товара начинается с торговой марки, то считаем, что
                    # торговая марка и есть название
                    for tm in self.trademarks[brand]:
                        if model_name != tm:
                            pos = model_name.find(tm)
                            if pos == 0:
                                return tm

        return model_name

    # Private-функция включает данные о товаре в контейнер append_to
    def _export_products(self, product_id, append_to, complex_articles_set):

        # Извлекаем описание товара из базы данных
        product = ProductContainer(self.db, product_id, self.get_shop_name())
        self._update_product(product)

        brand = product.get_brand()

        # Issue #93. Если бренд товара начинается с символа не попадающего в указанный,
        # при запуске скрипта, диапазон, то не экспортируем товар
        if self.so.json_limit is not None and len(self.so.json_limit) == 2 and brand is not None and len(brand) > 0:
            if brand[0] < self.so.json_limit[0] or brand[0] > self.so.json_limit[0]:
                return

        product_type = product.get_property('product_type')

        # В магазине "Дочки-Сыночки" в строке описания товара, сразу за названием
        # идут атрибуты товара, например, цвет изделия. Для корректного выполнения
        # классификации, имя продукта имеет смысл отделять от атрибутов
        model_name = self._correct_model_name(brand, product.get_property('short_name'))

        # Формируем имя продукта, как комбинацию типа продукта, бренда
        # и названия модели
        features = []

        if product_type is not None and len(product_type) > 0:
            features.append(product_type.strip())

        if brand is not None and len(brand) > 0:
            features.append(brand.strip())

        if model_name is not None and len(model_name) > 0:
            features.append(model_name.strip())

        product.set_product_name(' '.join(features))

        # Формируем уникальный ключ товара, по которому будет осуществляться
        # упрощённая классификация товарных позиций. Осуществляется попытка
        # унификации типп продукта
        article = ScrapingHelper.gen_article(product_type, brand, model_name)

        # Issue #145. Проверяем, можно ли использовать обобщённый артикул,
        # пригодный к группировке с товарами других магазинов, или нужно
        # применять композитный артикул, для того, чтобы предотвратить
        # ошибочную группировку разных товаров
        product_code = product.get_property('Артикул')
        if product_code is not None and len(product_code) > 0:

            complex_article = article + '_' + product_code
            if complex_article not in complex_articles_set:
                article = complex_article

            # Issue #160. Добавляем список альтернативных индексов товара
            product.set_alternative_index(product_code)

        # Issue #73. Если указан ключ экспорта "не группировать данные",
        # то подмешиваем к артикулу случайное значение
        if self.so.random_product_type:

            # todo: следующий ниже код уже не кажется актуальным

            # Issue #74. В "Дочках-сыночках" достаточно сложно сгруппировать одежду
            # по критериям тип товара/бренд/модель, поскольку модель чаще
            # всего не указывается, а одинаковые товары с одинаковым артикулом, но
            # разными размерами сохраняются как самостоятельные товары. Чтобы
            # обеспечить группировку товаров по размерам, вместо случайной строки
            # подмешивается внутренний артикул товара.
            ds_article = product.get_property('Артикул')
            if ds_article is not None:
                article += '-' + ds_article
            else:
                article += ScrapingHelper.generator_random_string()

        # Issue #115. Если в товара определено свойство "Рост", то:
        # - если не определено свойство "Размер", то переименовываем
        #   "Рост" в "Размер"
        height_prop = product.get_property('Рост')
        if height_prop is not None and len(height_prop) > 0:

            size_prop = product.get_property('Размер')
            if size_prop is None or len(size_prop) == 0:

                product.append_property_in_offer('Размер', height_prop)
                product.delete_property_in_offer('Рост')

        # Issue #115. Дублируем вариации товаров с подменой свойства
        # "productStickSizeAnchor" на "Размер"
        clothes_size = product.get_property('productStickSizeAnchor')
        if clothes_size is not None:

            product.delete_property_in_offer('productStickSizeAnchor')

            for size_value in clothes_size.split(';'):

                # Issue #137. Описание вариации может содержать не только
                # число - размер товара, но и href на персональную страницу
                # этого товара
                value = size_value
                href = None

                left_brace = size_value.find('{')
                right_brace = size_value.find('}')
                if 0 < left_brace < right_brace:

                    value = size_value[:left_brace]
                    href = size_value[left_brace + 1:right_brace]

                # Игнорируем вариацию, у которой нет ссылки. Такие ситуации
                # возникают, если конкретная вариация товара отсутствует
                if href is None or len(href) == 0:
                    continue

                # Копируем товар перед включением в список, чтобы
                # избежать множественных ссылок на объект в коллекции
                # и, соответственно, зависания
                copy_of_product = copy.deepcopy(product)
                copy_of_product.append_property_in_offer('Размер', value)

                # Issue #149. Заменяем свойства товара на специфические для
                # конкретной вариации
                props_dict = self.db.get_properties_for_variation(product_id, value)
                if props_dict is not None:
                    copy_of_product.replace_properties(props_dict)

                # Если описание вариации содержит URL, то подменяем URL
                # товара-вариации
                if href is not None:
                    copy_of_product.fix_url(self.get_shop_site(), href)

                # Добавляем информацию о товаре в коллекцию
                append_to.append(article, copy_of_product, ignore_duplicate=True)
        else:

            # Добавляем информацию о товаре в коллекцию
            append_to.append(article, product)

    # Private-функция включает данные о LEGO-товаре в контейнер append_to
    def _export_lego_playset(self, product_id, append_to):

        product = ProductContainer(self.db, product_id, self.get_shop_name())
        self._update_product(product, product_name_property='playset_name')

        article = product.get_property('Артикул')
        if article is None:
            article = "LEGO-" + ScrapingHelper.generator_random_string()

        # Добавляем информацию о товаре в коллекцию
        append_to.append(article, product)

    # Метод корректирует название товара, а так же ссылки на страницу товара
    def _update_product(self, product, product_name_property='short_name', modify_everything=True):
        """
        :param product: объект, хранящий информацию о товаре
        :param str product_name_property: Имя свойств, хранящего сокращённое название продукта
        :param bool modify_everything: если False, то не осуществлять дополнительную обработку данных
        """

        # Сохраняем название бренда
        brand_prop = product.get_property('Бренд')
        if brand_prop is not None:
            product.set_brand(brand_prop)

        # Корректируем название продукта
        product_name = product.get_property(product_name_property)
        if product_name is not None and len(product_name) > 0:
            # Учитываем особые случаи используемых названий моделей
            product_name = product_name.replace('ROEMER ', 'RÖMER ')
            product.set_product_name(product_name)

        if modify_everything:

            # Issue #90. Расширяем "хлебные крошки" названием магазина, как
            # это сделано в "Кораблике". Важно понимать, что к этому моменту,
            # хлебные крошки уже были скопированы в список 'Offers' и нам
            # нужно работать именно с этим контейнером, а не с 'Properties'.
            if product.container.get(product.OFFERS) is not None:
                if len(product.container[product.OFFERS]) == 1:
                    if product.container[product.OFFERS][0].get(product.PROPERTIES) is not None:

                        props = product.container[product.OFFERS][0][product.PROPERTIES]
                        if props.get('breadcrumb') is not None:
                            props['breadcrumb'] = self.get_shop_name().upper() + '/' + props['breadcrumb']

            # Issue #118. Корректируем названия свойств, добавляя к обычным
            # свойствам пробел в конце, а у вариаций убираем подчёркивание
            product.fix_issue118(self.db.unified_variations.keys())

            # Корректируем оригинальный URL товара
            product.fix_url(self.get_shop_site())

        pass

    # Экспериментальный код: Детали реализации описаны в find_any_models()
    # класса DetMirCatalogScraper
    def find_any_models(self, filter_name):

        self.db.preload_data(load_photos=False)

        temporary_dict = {}

        # Выполняем классификацию по: Бренд + Категория + Цена
        self.db.for_each_product(
            self._get_condition_by_name(filter_name),
            lambda cat_id, cat_name, prod_id, prod_name: self._extract_models(prod_id, temporary_dict)
        )

        # Выделяем общую часть названия товара из списка товаров одного бренда,
        # одной категории, идущих по одной цене
        dictionary_of_names = ScrapingHelper.extract_dictionary_of_models(temporary_dict, 'Бренд', 'short_names')

        # Выводим на экран JSON-данные с найденными моделями товаров
        print('ДОЧКИ-СЫНОЧКИ - mining названий моделей...\n')
        for brand in list(dictionary_of_names.keys()):
            models = ','.join(('"' + model + '"') for model in dictionary_of_names[brand])
            print('"{}": [\n\t{}\n],'.format(brand, models))

    # Private-функция выполняет классификацию товаров по схеме:
    # бренд + категория товара + цена
    def _extract_models(self, product_id, append_to):
        # Извлекаем описание товара из базы данных
        product = ProductContainer(self.db, product_id, self.get_shop_name())
        self._update_product(product)

        ScrapingHelper.group_products_by_price(product, append_to.get_products(), 'Бренд')

    # Исследовательская функция осуществляет чистку каждого свойства товара
    # магазина, по указанному правилу
    def clean_existing_data(self):
        print('ДОЧКИ-СЫНОЧКИ - Осуществляется чистка значений свойств...\n')
        self.db.update_each_property(ScrapingHelper.clean_property_value)

    # Исследовательская функция осуществляет импорт конкретного товара
    # из базы данных
    def import_special_details(self):
        pass

    def check_variations_existence(self, filter_name):

        # При запуске тестов, нужно учитывать, что по умолчанию в скрипт
        # добавляется условие: AND Photos.product_id IS NULL, которое
        # не позволит обработать данные корректно. Чтобы исключить
        # это условие изменяем специальный ключ - update_details
        self.db.update_details = True

        # Перед началом экспорта данных получаем полный список фотографий
        # и свойств для каждого товара в обрабатываемом магазине
        self.db.preload_data()

        # Собираем все идентификаторы продуктов и уникальные URL
        # из базы данных, по указанному фильтру
        self.all_urls = []
        self.all_products = []

        self.db.process_each_product(self._store_products_and_urls, self.so.scrape_details_by_count,
                                     additional_condition=self._get_condition_by_name(filter_name))

        print('Товаров к обработке: {}'.format(len(self.all_urls)))

        # Считываем свойства каждого продукта и если у продукта есть
        # список вариаций, то проверяем, если URL на эти вариации в
        # базе данных. Эта исследовательская функция позволяте ответить
        # на вопрос о том, как нужно обрабатывать вариации при
        # извлечении свойств товаров и формировании JSON-файлов
        has_variations_count = 0
        not_in_showcase_count = 0

        for product_id in self.all_products:

            product = ProductContainer(self.db, product_id, self.get_shop_name())

            clothes_size = product.get_property('productStickSizeAnchor')
            if clothes_size is not None:

                has_variations_count += 1

                not_in_showcase = False

                for size_value in clothes_size.split(';'):

                    # Для проведения исследований нам нужен только href
                    href = None

                    left_brace = size_value.find('{')
                    right_brace = size_value.find('}')
                    if 0 < left_brace < right_brace:
                        href = size_value[left_brace + 1:right_brace]

                    if href is None or len(href) == 0:
                        continue

                    if href not in self.all_urls:
                        not_in_showcase = True
                        print("Product_id = {} ссылается на вариацию, отсутствующую в базе: {}".format(product_id,
                                                                                                       href))

                if not not_in_showcase:
                    not_in_showcase_count += 1

        print('Товаров содержат вариации: {}. Из них без витрины: {}'.format(has_variations_count,
                                                                             not_in_showcase_count))

    def _store_products_and_urls(self, product_id, product_url):

        self.all_products.append(product_id)
        self.all_urls.append(product_url)
