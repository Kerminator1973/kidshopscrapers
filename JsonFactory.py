#!/usr/bin/python
# -*- coding: utf-8

import gc
import json
import logging
from collections import defaultdict
from FilterCollection import Filter                 # Класс-агрегатор атрибутов отбора данных
from ProductDescription import ProductContainer


# Issue #9: приоритеты магазинов используются при выборе наилучшего описания товара, критерием
# которого является магазин. Первый в списке магазин считается наиболее доверенным
PRIORITY_LIST = ['Дочки-Сыночки', 'Детский Мир', 'Кораблик', 'Мир Кубиков', 'Купи-Кубик']


# Вспомогательный класс Encoder, позволяющий экспортировать в JSON объекты
# в которые включены объекты класса ProductContainer
class ProductsEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ProductContainer):
            return obj.container
        return json.JSONEncoder.default(self, obj)


# Функция осуществляет подготовку данных для формирования JSON-файл
# с целью их экспорта в формат, удобный для использования на сайте:
# http://www.detskiyspisok.ru/
def export_products_to_json(products, db_helper, filter_name, optimize_urls=False):

    # Подготоваливаем данные о продуктах к группировке в JSON
    container = _prepare_products_container(products)

    # Если для операции указан фильтр товаров (Alias категории),
    # то пытаемся считать список вариаций специально для этого
    # конкретного фильтра.
    # Значение 999 в параметре index_shop_name не имеет значения,
    # т.е. мы имеем здесь частный случай применения класса Filter
    filters = Filter(999, filter_name)
    valuable_props = filters.get_variations_priority()

    # Если у универсального имени категории (Alias) не указан
    # список вариаций, то применяем общий для всех список
    if valuable_props is None:

        # Подготавливаем список общий вариацией, включая вариации
        # загруженные из файла 'variations3.json'
        valuable_props = db_helper.get_unified_variations()

    # Счётчик товаров, в которых объединены ценовые предложения разных магазинов
    merged_items_count = 0

    # После того, как структура товарных позиций сформирована,
    # осуществляем более глубокие структурные преобразования, цель
    # которых - добавление вспомогательных данных, таких как
    # variations и values
    for key in list(container.keys()):

        product = products[key]

        if container[key].get(product.OFFERS) is None:
            continue
        if len(container[key][product.OFFERS]) == 0:
            continue

        # Если товар представлен в разных магазинах, то увеличиваем
        # соответствующий счётчик
        if has_offers_of_different_shops(key, container[key][product.OFFERS]):
            merged_items_count += 1

        # Получаем список вариаций по всем товарным предложениям одного товара
        variations = _collect_variations(valuable_props, container[key][product.OFFERS])

        # К этому моменту у нас сгруппированы все свойства всех товарных предложений
        # в коллекцию, в которой ключём является свойство, а значением - массив
        # значений свойств.

        first_key, second_key = _specify_variation_keys(variations)

        # Следующий шаг - изменение структуры ценовых предложений,
        # объединяя их по набору свойств
        container[key]['prices'] = {}

        if first_key is None and second_key is None:

            # В случае, если варианты исполнения (вариации - наиболее ценные свойства
            # товара) для товара отсутствуют, то можно использовать наиболее простой
            # вариант реструктуризации объекта "товар"
            container[key]['prices']['relation'] = None
            container[key]['prices']['offers'] = container[key][product.OFFERS]

        elif first_key is not None and second_key is None:

            # Обрабатываем вариант с "single" variations
            container[key]['prices']['relation'] = first_key
            container[key]['prices']['variations'] = {}
            container[key]['prices']['variations'][first_key] = _sort_by_content(variations[first_key])

            # Проходим по всем возможным вариациям товара, а так же
            # включаем синтетическую вариацию 'any', к которой относим все
            # товарные предложения, в которых отсутствует ключевое свойство
            container[key]['prices']['values'] = {}

            count = 0
            for value in (variations[first_key] + ['any']):

                block = {'images': [], 'offers': []}

                # Ищем все ценовые предложения у которых в списке свойств
                # присутствует свойство с названием first_key и включаем
                # все эти предложения в отдельную группу 'offers'
                for offer in container[key][product.OFFERS]:

                    # Не включаем товарное предложение в текущую группу,
                    # если в offer.Properties отсутствует свойство с
                    # именем вариации.
                    # Например, если вариацией товарного предложения
                    # является 'Цвет', создаётся список предложений для
                    # цвета 'YELLOW', а у текущего предложения цвет 'RED',
                    # то оно в список добавлено не будет
                    if offer.get('Properties') is not None:

                        if offer['Properties'].get(first_key) is None:
                            if 'any' != value:
                                continue
                        else:
                            if offer['Properties'][first_key] != value:
                                continue

                        # Если значение вариации у товарного предложения соответствует
                        # названию формируемой, в текущей итерации, группы, то включаем
                        # предложение в эту группу
                        block['offers'].append(offer)

                # Если в блок попали хоть какие-то предложения, то сохраняем этот блок
                if len(block['offers']) > 0:
                    block['order'] = count
                    count += 1

                    # Issue #157. Mongo-совместимость: заменяем точку в ключе на запятую
                    mongo_compatible_var_name = value.replace(".", ',')
                    container[key]['prices']['values'][mongo_compatible_var_name] = block
        else:

            # Обрабатываем вариант с "double" variations
            container[key]['prices']['relation'] = "{}_{}".format(first_key, second_key)
            container[key]['prices']['values'] = {}

            # Включаем в объект значение обеих найденных вариаций
            container[key]['prices']['variations'] = {}

            container[key]['prices']['variations'][first_key] = []
            if variations[first_key] is not None:
                for first_value in _sort_by_content(variations[first_key]):
                    container[key]['prices']['variations'][first_key].append(first_value)

            if variations[second_key] is not None:
                container[key]['prices']['variations'][second_key] = []
                for second_value in _sort_by_content(variations[second_key]):
                    container[key]['prices']['variations'][second_key].append(second_value)

            # Имена вариаций объединяются посредством символа underscore.

            # Для каждого товарного предложения определяем, в какую группу
            # это предложение входит
            for offer in container[key][product.OFFERS]:

                first_key_value = 'any'
                second_key_value = 'any'

                if offer.get('Properties') is not None:

                    if offer['Properties'].get(first_key) is not None:
                        first_key_value = offer['Properties'][first_key]

                    if offer['Properties'].get(second_key) is not None:
                        second_key_value = offer['Properties'][second_key]

                # Значения вариацией разделяем символом underscore
                composed_key_name = first_key_value + '_' + second_key_value

                # Issue #157. Mongo-совместимость: заменяем точку в ключе на запятую
                mongo_compatible_key_name = composed_key_name.replace(".", ',')

                # Добавляем товарное предложение в одну из групп вариацией
                if container[key]['prices']['values'].get(mongo_compatible_key_name) is None:
                    container[key]['prices']['values'][mongo_compatible_key_name] = {}
                    container[key]['prices']['values'][mongo_compatible_key_name]['offers'] = []

                container[key]['prices']['values'][mongo_compatible_key_name]['offers'].append(offer)

        # Удаляем коллекцию ценовых предложений товара, поскольку они уже были
        # переструктурированы и перемещены в другое место
        del container[key][product.OFFERS]

        # Issue #157. Mongo-совместимость: В названиях вариаций заменяем точку на запятую
        if container[key].get('prices') is not None:
            if container[key]['prices'].get('variations') is not None:
                for variation_name in container[key]['prices']['variations']:
                    for i, value in enumerate(container[key]['prices']['variations'][variation_name]):
                        container[key]['prices']['variations'][variation_name][i] = value.replace(".", ',')

    # Перемещаем отдельные поля в структуре с учётом приоритета магазинов
    promote_properties(container)

    # Оптимизируем экспортный файл по размеру: для каждого товара
    # сравниваем значение поля "Description" - если их несколько,
    # то выбираем наиболее популярный вариант и сохраняем его на
    # самом верхнем уровне. Везде, где Description отличается,
    # указываем оригинальное значение
    _optimize_descriptions(container)

    # Осуществляем перенос наиболее популярных свойств товарных
    # предложений на уровень товара. Цель - уменьшить объём
    # памяти, необъходимый для хранения описания товара
    _optimize_properties(container)

    if optimize_urls:
        print('Осуществляется оптимизация URL-ов')
        _optimize_urls(container)

    # Выводим коэффициент объединения товаров. Этот коэффициент является
    # косвенным и характеризует качество группировки товаров, т.е. случаи,
    # когда система классифицирует разные ценовые предложения, как один товар
    if len(container) > 0:
        print('Merged coefficient = {0:.3f}%'.format(merged_items_count * 100 / (len(container) + merged_items_count)))

    # Сохраняем накопленные данные в файл в виде json-объекта. При экспорте
    # используем дополнительный объект - Encoder, который помогает обрабатывать
    # сложные типы данных
    with open('products.json', 'w', encoding='utf8') as outfile:
        json.dump(container, outfile, cls=ProductsEncoder, ensure_ascii=False, sort_keys=True)

    # Issue #95. Пробуем освободить память принудительно
    gc.collect()
    del gc.garbage[:]


# Issue #134. Функция переносит самый популярный Description на уровень
# основных свойств товара, при этом дубликаты описания удаляются из
# вариаций. Поскольку, чаще всего у товаров в вариациях Description-ы
# совпадают, это позволяет уменьшить объём файлов на ~15%
def _optimize_descriptions(container):

    for key in list(container.keys()):

        # ПОдсчитываем сколько разных вариантов описания товара существует
        stats = defaultdict(int)

        if container[key].get('prices') is None:
            continue

        if container[key]['prices'].get('values') is None:

            # Issue #154. Для безвариационных товаров, у уже есть
            # консолидированнвый Description на уровне prices - его
            # нужно перенести на уровень выше
            if container[key]['prices'].get('Description') is not None:
                container[key]['Description'] = container[key]['prices']['Description']
                del container[key]['prices']['Description']

            continue

        values = container[key]['prices']['values']

        # Собираем статистику популярности описаний товаров
        for variation in list(values.keys()):

            if values[variation].get('Description') is not None:
                stats[values[variation]['Description']] += 1

        # Если есть дублирующиеся описания товаров, то выносим наиболее
        # популярный вариант на уровень описания товара и удаляем его
        # дубликаты из вариантов
        max_count = 0
        champion_description = ''

        for stat_key in list(stats.keys()):
            if stats[stat_key] > max_count:
                max_count = stats[stat_key]
                champion_description = stat_key

        if max_count > 0:

            container[key]['Description'] = champion_description

            for variation in list(values.keys()):

                if values[variation].get('Description') is None:
                    continue

                if values[variation]['Description'] == champion_description:
                    del values[variation]['Description']


# Issue #134. Функция осуществляет подъём наиболее популярных
# свойств с уровня товарных предложений на уровень описания товара
def _optimize_properties(container):

    for key in list(container.keys()):

        # Для каждого товара собираем собственную статистику
        prop_stats = {}

        if container[key].get('prices') is None:
            continue

        if container[key]['prices'].get('values') is None:

            # Issue #154. Для безвариационных товаров, у уже есть
            # консолидированнвый Properties на уровне prices - его
            # нужно перенести на уровень выше
            if container[key]['prices'].get('Properties') is not None:
                container[key]['Properties'] = container[key]['prices']['Properties']
                del container[key]['prices']['Properties']

            continue

        values = container[key]['prices']['values']

        if len(values) == 0:
            continue

        # Проходим по каждому товарному предложению и подсчитываем
        # общее количество значений свойств
        for variation in list(values.keys()):

            if values[variation].get('Properties') is not None:

                for prop_key in list(values[variation]['Properties'].keys()):

                    if prop_key not in prop_stats:
                        prop_stats[prop_key] = defaultdict(int)

                    prop_value = values[variation]['Properties'][prop_key]
                    prop_stats[prop_key][prop_value] += 1

        # В этом месте у нас полностью сформирована статистика по наиболее
        # популярным свойствам товара. Теперь необходимо выбрать те свойства,
        # у которых есть значения с количеством совпавших значений свойств
        # больше, чем один раз
        values_to_up = {}
        for prop_key in list(prop_stats.keys()):

            max_count = 0
            champion_prop_value = ''

            for _prop_value in list(prop_stats[prop_key].keys()):
                if prop_stats[prop_key][_prop_value] > max_count:
                    max_count = prop_stats[prop_key][_prop_value]
                    champion_prop_value = _prop_value

            if max_count > 0:
                values_to_up[prop_key] = champion_prop_value

        # Список наиболее популярных свойств сформирован, теперь эти
        # свойства можно выносить на уровень товара
        if len(values_to_up) > 0:
            container[key]['Properties'] = values_to_up

        # Следующий этап - необходимо удалить продублированные свойства
        # из описания товарного предложения, но при расхождении фактических
        # значений, свойство товарного предложения нужно сохранить, т.к.
        # оно хранит специфику особенных товаров
        for variation in list(values.keys()):

            if values[variation].get('Properties') is not None:

                for prop_key in list(values[variation]['Properties'].keys()):

                    if prop_key in values_to_up:
                        if values[variation]['Properties'][prop_key] == values_to_up[prop_key]:
                            del values[variation]['Properties'][prop_key]
    pass


# Issue #134. Функция осуществляет подмену полных URL и путей
# к изображениям, на сокращённые написания. Этот подход позволяет
# уменьшить размер JSON-файла на 15%.
# Подход требует обратного действия на frontend-е
def _optimize_urls(container):

    with open('shorturls.json', encoding='utf8') as data_file:
        zip_urls = json.load(data_file)
        if zip_urls is None:
            return

        for key in list(container.keys()):

            if container[key].get('prices') is not None:
                _optimize_subroot_urls(zip_urls, container[key]['prices'])

            if container[key]['prices'].get('values') is None:
                continue

            # Собираем статистику популярности описаний товаров
            subroot = container[key]['prices']['values']
            for variation in list(subroot.keys()):
                _optimize_subroot_urls(zip_urls, subroot[variation])


# Вспомогательная функция, используется в _optimize_urls()
def _optimize_subroot_urls(zip_urls, subroot):

    if subroot.get('images') is not None:

        for index, image in enumerate(subroot['images']):
            for short_url in list(zip_urls.keys()):
                if image.find(short_url) == 0:

                    new_filename = zip_urls[short_url]
                    new_filename += image[len(short_url):]

                    subroot['images'][index] = new_filename
                    break

    if subroot.get('offers') is not None:

        for index, offer in enumerate(subroot['offers']):

            if offer.get('url') is None:
                continue

            for short_url in list(zip_urls.keys()):
                if offer['url'].find(short_url) == 0:

                    new_filename = zip_urls[short_url]
                    new_filename += offer['url'][len(short_url):]

                    subroot['offers'][index]['url'] = new_filename
                    break


# Функция сортирует список, основываясь на его содержимом. Другими словами,
# если в списке находятся строки с числами, то сортировать содержимое нужно
# как числа
def _sort_by_content(content):

    if ''.join(content).isdigit():

        try:
            numbers = [int(s) for s in content]
            numbers.sort()
            return [str(n) for n in numbers]

        except ValueError:
            # isdigit() считает, что некоторые unicode-символы, такие как '\u00B2'
            # (во второй степени), или '\u00BD' (половина единицы) являются цифрами,
            # но привести их к числу функция int() не может. Если мы столкнулись
            # с подобной ситуацией, то считаем, что содержимое списка всё-таки
            # было строками и сортируем его как строки
            pass

    content.sort()
    return content


# Осуществляем предварительную обработку данных: изменяем
# название свойств, переносим свойства из одного места в
# другое, консолидируем их, и.т.д.
def _prepare_products_container(products):

    container = {}

    for key in list(products.keys()):

        container[key] = {}

        # Сначала копируем ключевые свойства продукта
        product = products[key]

        # Копируем в JSON-контейнер ключевые свойства товара
        model = product.get_property('short_name')
        if model is not None and len(model) > 0:
            container[key]['Model'] = model.title()

        brand = product.get_brand()
        if brand is not None and len(brand) > 0:
            # Функция title() не всегда работает корректно. В частности, она
            # переводит символ идущий за апострофом в верхний регистр, например:
            # JOHNSON’S BABY => Johnson’S Baby
            # для компенсации данной особенности, заменяем типовые случаи.
            # Учитываем разные варианты апосторофов, которые могут быть
            # использованы в названии товара
            brand_title = brand.title().replace("’S", "’s")
            brand_title = brand_title.replace("'S", "’s")
            container[key]['Brand'] = brand_title

        # Копируем информацию о ценовых предложениях на товары
        if product.container.get(product.OFFERS) is not None:

            offers = product.container[product.OFFERS]

            # Тип продукта хранится в Offers и его нужно поднять на верхний уровень
            if len(offers) > 0:
                if offers[0].get('Properties') is not None:
                    if offers[0]['Properties'].get('product_type') is not None:

                        product_type = offers[0]['Properties']['product_type']
                        if len(product_type) > 0:
                            container[key]['ProductType'] = product_type.title()

            # Принудительно удаляем из названий свойств символ ".",
            # т.к. MongoDB в этом случае работает не корректно.
            # Пример: "Наличие в г. Москва"
            for offer in offers:
                if offer.get('Properties') is not None:
                    for property_name in list(offer['Properties'].keys()):

                        # Дополнительно, изменяем написание значений свойств
                        if property_name.find('.') >= 0:
                            new_prop_name = property_name.replace('.', '_')
                            offer['Properties'][new_prop_name] = offer['Properties'][property_name]
                            del offer['Properties'][property_name]

            # Группируем все категории и хлебные крошки на верхнем уровне, но в
            # виде массива. В таком виде записи будет удобнее связывать с эталонными
            # категориями.
            # ТРЮК: Избавиться от дублирования в списке можно сначала преобразовав
            # список в set(), а затем обратно в list()
            categories = collect_properties(offers, 'category')
            if categories is not None:
                container[key]['Categories'] = list(set(categories))

            breadcrumb = collect_properties(offers, 'breadcrumb')
            if breadcrumb is not None:
                container[key]['Breadcrumb'] = list(set(breadcrumb))

            delete_excess_properties(offers)

            container[key]['Offers'] = offers

    return container


def _collect_variations(valuable_props, offers):

    variations = {}

    for offer in offers:

        if offer.get('Properties') is not None:

            for prop_name in list(offer['Properties'].keys()):

                # Игнорируем свойства, которые не являются вариациями
                if prop_name.upper() in valuable_props:

                    if variations.get(prop_name) is None:
                        variations[prop_name] = []

                    # Добавляем значение в хранилище только в том случае,
                    # если оно уникально
                    if offer['Properties'][prop_name] not in variations[prop_name]:
                        variations[prop_name].append(offer['Properties'][prop_name])

    return variations


# Функция определяет имена вариаций, по которым будет осуществляться
# группировка товаров на витрине
def _specify_variation_keys(variations):

    first_key = None
    second_key = None

    variation_keys = list(variations.keys())
    if len(variation_keys) > 0:
        first_key = variation_keys[0]

        if len(variation_keys) > 1:
            second_key = variation_keys[1]

    # На текущий момент, максимальное количество вариаций, т.е. названий
    # свойств, по которым может быть уточнено товарное предложение,
    # ограничивается двумя. Если вариаций будет больше, то это перегрузит
    # пользовательский интерфейс витрины
    return first_key, second_key


# Переносим "свойства" на уровень "offers"
def promote_properties(container):

    for key in list(container.keys()):

        if container[key].get('prices') is None:
            continue

        # В зависимости от того, есть ли в иерархии параметр "values",
        # т.е вариации, выбираем иерархию, на которую следует поднимать
        # свойства.
        #
        # Цепочка свойств вот такая: key - prices - values - [варианты]
        if container[key]['prices'].get('values') is not None:

            for value_name in list(container[key]['prices']['values']):

                container_sub_ref = container[key]['prices']['values'][value_name]
                _promote_properties_to(container_sub_ref)   # key - prices - values - [value]

        else:
            _promote_properties_to(container[key]['prices'])

        pass


# Вспомогательный метод, позволяет исключить понятие "уровень иерархии"
# из алгоритма перемещения свойств
def _promote_properties_to(data_block):

    if data_block.get('offers') is None:

        # Issue #108. Вроде бы, ситуация, о которой сигнализирует идущее ниже
        # сообщение, является вполне нормальной ситуацией
        '''msg = 'Ошибка в работе алгоритма перемещения свойств товаров: {}'.format(data_block)
        logging.error(msg)'''
        return

    # Проходим по всем товарным предложениям и ищем наиболее приоритетный
    # магазин, данные из которого будут повышены в иерархии
    # Чтобы алгоритм приоритезации работал, необходимо сначала поставить
    # индекс на наименее приоритетный магазин
    priority_shop = PRIORITY_LIST[-1]
    ref_to_properties = None
    ref_to_description = None
    ref_to_photos = None

    for offer in data_block['offers']:

        if offer.get('shop') is None:
            logging.critical("Описание ценового предложения не содержит название магазина ({0})".format(offer))
            continue

        if priority_shop == offer['shop']:
            continue

        # В ценовом предложении содержится название магазина, которые мы сравниваем
        # с наиболее приоритетным из ранее проанализированных ценовых предложений.
        if PRIORITY_LIST.index(offer['shop']) < PRIORITY_LIST.index(priority_shop):

            # Считаем, что магазин является приоритетным только в том случае, если
            # в нём содержатся фотографии товаров
            if offer.get('Properties') is None:
                continue

            if len(offer['Properties']) > 0:
                priority_shop = offer['shop']
                ref_to_properties = offer['Properties']

                if offer.get('Description') is not None:
                    ref_to_description = offer['Description']

                if offer.get('images') is not None:
                    ref_to_photos = offer['images']
        pass

    # После того, как мы нашли свойства приоритетного магазина,
    # копируем их на уровень выше
    if ref_to_properties is None:

        # todo: провести исследование причин возникновения данной проблемы.
        # Поскольку сообщение пишется в логи слишком часто, временно блокирую его

        '''msg = 'Не найден магазин с приоритетными свойствами: {}'.format(data_block)
        logging.error(msg)'''
        return

    data_block['Properties'] = ref_to_properties

    # Подобным образом поступаем и с описанием товара (Description)
    if ref_to_description is not None:
        data_block['Description'] = ref_to_description

    # ...а так же с фотографиями, которые переименовываем в "images"
    if ref_to_photos is not None:
        data_block['images'] = ref_to_photos

    # Удаляем свойства всех остальных товарных предложений
    for offer in data_block['offers']:
        if offer.get('Properties') is not None:
            del offer['Properties']

        if offer.get('images') is not None:
            del offer['images']

        if offer.get('Description') is not None:
            del offer['Description']
    pass


# Функция анализирует информацию по товару и если он продаётся в разных
# магазинах, то возвращает значение True
def has_offers_of_different_shops(key, offers):

    if len(offers) < 2:
        return False

    first_shop = offers[0]['shop']

    for offer in offers:
        if first_shop != offer['shop']:

            print('Merged product: "{}"'.format(key))
            return True

    return False


# Функция собирает в один массив значения свойства из контейнера offers
def collect_properties(offers, property_name):

    collection = []

    for offer in offers:

        if offer.get('Properties') is not None:
            if offer['Properties'].get(property_name) is not None:
                collection.append(offer['Properties'][property_name])

    if len(collection) == 0:
        return None

    return collection


# Функция удаляет избыточные поля JSON-объекта
def delete_excess_properties(offers):

    prop_names = ['product_type', 'category', 'breadcrumb', 'Brand', 'Бренд', 'short_name']

    for offer in offers:
        if offer.get('Properties') is not None:

            for property_name in prop_names:
                if offer['Properties'].get(property_name):
                    del offer['Properties'][property_name]
