#!/usr/bin/python
# -*- coding: utf-8

import urllib.parse
import urllib.request
import urllib.error
import logging
from urllib.parse import urlparse, parse_qs
from bs4 import BeautifulSoup
import ScrapingHelper
from BaseScraper_Lego import BaseScraperLego


# Базовым классом является BaseScraperLego, который объединяет общие функции для
# магазинов "Мир Кубиков" и "Купи-Кубик"
class KupiKubikCatalogScraper(BaseScraperLego):

    def __init__(self, database_helper, options):

        # Вызываем конструктор базового класса
        super(KupiKubikCatalogScraper, self).__init__(database_helper, options)

        # Указываем название магазина, с которым должны быть связаны все операции
        # в базе данных, связанные с категориями магазина "Купи-Кубик"
        database_helper.activate_shop('KupiKubik')

    def get_shop_name(self):
        return 'Купи-Кубик'

    def get_shop_site(self):
        return 'http://www.kupikubik.ru/'

    # Public-функция осуществляет загрузку каталога товарных групп магазина "Купи Кубик"
    def scrape_catalog(self, filter_name):

        # При каждом запуске скрипта импортируем категории товаров. Это не занимает
        # много времени, но зато список категорий всегда находится в актуальном
        # состоянии
        self._scrape_categories()

        # Проверяем соответствие фильтра специализации магазина
        if "ALL" != filter_name and "LEGO" != filter_name:
            logging.info('Условия фильтрации исключают магазин Купи-Кубик из обработки')
            return

        # Осуществляем попытку получения данных по всем накопленным в базе
        # данных категориям
        self.db.process_categories(
            lambda rowid, url: self._scrape_the_showcase(rowid, url),
            self.so.scrape_categories_to_process)

        # Извлекаем список фотографий товаров
        if self.so.scrape_details_by_count > 0:
            print("Извлечение свойств товаров...")
            self.db.process_each_product(self._scrape_details, self.so.scrape_details_by_count)

    # Private-функция осуществляет разбор названий товарных категорий
    def _scrape_categories(self):
        try:

            # Загружаем главную страницу сайта kupikubik.ru и
            # ищем на ней описание групп наборов LEGO

            with urllib.request.urlopen(self.get_shop_site()) as response:
                # Загружаем html-документ в кодировке 1251 (8-бит, кириллица)
                html = response.read().decode('cp1251')
                soup = BeautifulSoup(html, "html.parser")

                # Ищем блок описания группы элементов (он должен быть один)
                group_elements = soup.findAll('div', attrs={'id': 'groupElements'})
                if len(group_elements) == 1:

                    for group in group_elements[0].findAll('a'):

                        # В блоке описания группы, в ссылке должно присутствовать
                        # изображение с классом 'groupImgElement'
                        tag = group.findAll('img', attrs={'class': 'groupImgElement'})
                        if len(tag) == 1:

                            name = str(tag[0]['alt'])   # Выделяем имя группы
                            href = str(group['href'])   # Выделяем идентификатор групы

                            # Добавляем в базу данных описание название группы товаров.
                            # Поскольку структура категорий сайта упрощённая, то
                            # считаем, что все категории являются частью отсуствующей
                            # корневой категории с идентификатором "1"
                            self.db.insert_category(name, href, '1')

        except urllib.error.URLError as e:
            logging.critical('Сбой в сетевой подсистеме', e.reason)
            exit()

    def _scrape_the_showcase(self, category_id, category_url):

        logging.info("Осуществляется обработка URL: {}".format(category_url))

        href = self.get_shop_site() + category_url
        try:
            # Загружаем страницу с товарами конкретной категории (серии)
            with urllib.request.urlopen(href) as response:
                html = response.read().decode('cp1251')
                soup = BeautifulSoup(html, "html.parser")

                # Обычно на странице два навигационных блока
                pagination = soup.findAll('div', attrs={'class': 'pgn'})
                if len(pagination) > 0:

                    # Типовые ситуации:
                    # - наборов в группе много и они не помещаются на одну страницу
                    # - все наборы в группе помещаются на одну страницу
                    links = pagination[0].findAll('a')
                    if len(links) == 2:
                        # Если все наборы помещаются на одну страницу,
                        # об этом нам говорит количество ссылок -2,
                        # т.е. ссылка на "Показать всё" и "1", то
                        # сразу же обрататываем страницу
                        self.db.begin_products_insertion()
                        self._collect_playsets(html, category_id)
                        self.db.end_products_insertion()

                    else:
                        # Ищем ссылку на страницу "Показать всё"
                        for link in links:
                            if link.getText() == "Показать все":
                                show_all = self.get_shop_site() + str(link['href'])

                                with urllib.request.urlopen(show_all) as response2:
                                    html = response2.read().decode('cp1251')

                                    self.db.begin_products_insertion()
                                    self._collect_playsets(html, category_id)
                                    self.db.end_products_insertion()
                                break

        except urllib.error.URLError as e:
            logging.critical('Сбой в сетевой подсистеме', e.reason)
            exit()

    def _collect_playsets(self, html, category_id):

        # ОПТИМИЗАЦИЯ ПО СКОРОСТИ:
        # Выделяем блок текста, который начинается с определённого
        # div(а) и заканчивается конкретным параграфом (p). Это нужно
        # для того, чтобы уменьшить объём обрабатываемых данных
        begin = html.find('<div id="page_headline">')
        end = html.find('<p class="MsoNormal"', begin)

        # Удаляем из документа все тэги <br> и </br> - они расставлены
        # в html-документе хаотично и нарушают разбор документа
        subhtml = html[begin:end].replace('<br>', '').replace('</br>', '')

        # Подготоваливаем документ для дальнейшей обработки:
        # будет исправлена структура документа, не поддерживаемые
        # символы будут закодированы CER-последовательностями HTML
        soup = BeautifulSoup(subhtml, "html.parser")

        # Далее, для всех наборов нам нужно получить следующие реквизиты:
        # артикул, название набора, цена, ссылку на блок детальной информации
        # и признак "Доступно" (In-Stock)

        # Особняком в html-коде стоят тэги, которые могут быть использованы для
        # определения доступности товара на складе. Выглядят они так:
        #   <a href="?mod=shop&id=3016&task=addtocart&item=-1">В корзину</a>
        # Соответственно мы ищем все ссылки, внутри которых находится текст
        # "В корзину", выделяем из аттрибута поле "href", находим в нём
        # параметр "id" и сохраняем его в соответствующий список - потом мы
        # будем проверять, есть ли на складе "id" разобранного товара
        links = soup.findAll('a', text="В корзину")

        availabilities = []
        for link in links:
            parsed = parse_qs(urlparse(str(link['href'])).query)
            available_id = parsed.get('id')
            if available_id is not None:
                availabilities.append(int(available_id[0]))

        # Каждый набор описывается в ряде div-областей. Эти области
        # объединены в блоки, описывающие одну товарную позицию
        for position in soup.findAll('div', attrs={'id': 'position'}):

            playset = ""
            url_details = ""
            in_stock = 0
            price = 0

            for divno, current in enumerate(position.findAll('div')):
                try:

                    # Первый div содержит название группы набора
                    # ИГНОРИРУЕМ

                    # Второй div содержит название набора
                    if divno == 1:
                        playset = current.getText().strip()

                    # Третий div содержит ссылку на изображение набора
                    # ИГНОРИРУЕМ

                    # Четвёртый div содержит дополнительную информацию (размер/возраст/количество деталей)
                    # ИГНОРИРУЕМ

                    # Пятый div содержит ссылку на подробную информацию о наборе
                    if divno == 4:
                        classpg = current.findAll('a', attrs={'class': 'pg'})
                        if classpg is not None:
                            url_details = str(classpg[0]['href'])

                            # Так же выделяем идентификатор набора - он нам нужен для
                            # определения наличия товара на складе (см. список availabilities)
                            parsed = parse_qs(urlparse(url_details).query)
                            playset_id = parsed.get('id')
                            if playset_id is not None:
                                if int(playset_id[0]) in availabilities:
                                    in_stock = 1

                    # Шестой div содержит стоимость набора
                    if divno == 5:
                        # Цена представлена в следующем формате: '7 399,00 руб'.
                        # Далее мы явно игнорируем копейки, т.е. цены всех остальных магазинов
                        # сохраняются в рублях. Потеря информации о копейках является не
                        # существенной
                        price = ScrapingHelper.parse_amount(current.getText().strip(), ',')

                    # Больше полезных div-ов нет

                except Exception as e:
                    logging.error("Ошибка разбора поля названия группы, или названия набора ({0})".format(str(e)))

            # Разделяем артикул набора и его название
            separator = playset.find(' ')
            if separator > 0:
                article = playset[0:separator]
                name = playset[separator+1:]

                # Добавляем товарную позицию в базу данных
                product_id = self.db.insert_product(category_id, name.strip(), price, url_details, force_insert=True)

                # Сохранять свойства будем через специальный кэширующий механизм
                self.db.begin_props_insertion()

                self.db.insert_property(product_id, "Артикул", article)

                # В явном виде сохраняем информацию о наличии товара на складе - это
                # может быть важно для последующей обработке данных
                self.db.insert_property(product_id, "На складе", str(in_stock))

                # Осуществляем пакетную запись в базу данных всех свойств сразу
                self.db.end_props_insertion()

    # Private-функция разбора страницы отдельного товара
    def _scrape_details(self, product_id, product_url):

        # Удаляем ранее извлечённые свойства товара
        self.db.delete_properties_and_photos(product_id)

        # Сохранять свойства будем через специальный кэширующий механизм
        self.db.begin_props_insertion()

        # Осуществляем попытку загрузки изображения с персональной страницы товара
        success = False
        try:

            # Формируем адрес страницы
            url = self.get_shop_site() + product_url
            with urllib.request.urlopen(url) as response:

                html = response.read().decode('cp1251')
                soup = BeautifulSoup(html, 'html.parser')

                all_magnifiers = soup.findAll('a', attrs={'rel': 'lightbox'})
                for magnifier in all_magnifiers:

                    fullsize_image = magnifier.find('img', attrs={'src': True})
                    if fullsize_image is None:
                        logging.error('Не найдено изображение товара. Страница: ', product_url)
                        continue

                    # Сохраняем ссылку на вспомогательные изображения товара
                    self.db.add_photo_of_product(product_id, fullsize_image['src'])
                    success = True

        except urllib.error.URLError as e:
            logging.error('Сбой в сетевой подсистеме', e.reason)
            pass

        # Если по каким-то причинам выделить ссылку на изображение не удалось, сохраняем
        # информацию об этом в базе данных для последующего разбора. Так же важно исключить
        # эту запись из повторной обработки, т.е. добавив ссылку на Products.rowid
        # из поля product_id таблицы Photos
        if not success:
            self.db.add_photo_of_product(product_id, product_url, successful=False)

        # Осуществляем пакетную запись в базу данных всех свойств сразу
        self.db.end_props_insertion()
