#!/usr/bin/python
# -*- coding: utf-8

import time
import logging
import argparse
import ScrapingDatabase
import DetMir_CatalogScraper
import Dochkisinochki_CatalogScraper
import Korablik_CatalogScraper
import MirKubikov_CatalogScraper
import KupiKubik_CatalogScraper
import Tester_CatalogScraper
from GroupingStrategy import GroupingByAlternativeIndexes
from JsonFactory import export_products_to_json


class Scrapers:
    def __init__(self):

        # Определяем параметры подсистемы трассировки времени исполнения.
        # ВАЖНЫЙ НЮАНС: системой логирования logging пользуются разные компоненты,
        # которые будут сохранять в логах свои данные. По умолчанию установлен
        # режим logging.INFO, который при необходимости можно изменить на
        # logging.DEBUG, или logging.WARNING
        root_logger = logging.getLogger()
        root_logger.setLevel(logging.INFO)
        handler = logging.FileHandler('scraping.log', 'w', 'utf-8')
        formatter = logging.Formatter('%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s] %(message)s')
        handler.setFormatter(formatter)
        root_logger.addHandler(handler)

        # Количество категорий, данные из которых импортируются за одну сессию
        self.scrape_categories_to_process = 3

        # Количество товаров, информация о которых должна быть импортирована в рамках одной сессии
        self.scrape_details_by_count = 5

        # Извлечению подлежит информация обо всех товарах в магазине
        self.filter = "ALL"

        # По умолчанию, считаем, что не нужно обновлять детальную информацию о ранее уже
        # извлечённых товарах
        self.update_details = False
        
        # По умолчанию, не извлекаем описание свойств отдельных вариаций товаров,
        # т.к. это может потребовать выполнения дополнительных запросов (http/https)
        # к магазину. Это может приводить к 2-3 кратному замедлению процесса
        # извлечения данных (замеры осуществлялись для "Дочек-сыночков")
        self.extract_var_props = False

        # По умолчанию, выполнять только те операции, которые не требуют непосредственного
        # доступа к сайтам магазинов
        self.offline = False

        # Не извлекать с сайта список подкатегории товаров. Это бывает полезно в том случае,
        # если осуществляется отладка кода scraper-а магазина, для извлечения списка категорий
        # которого требуется загрузка множество вложенных html-документов. Примером
        # такого магазина является "Дочки-сыночки", загрузка списка категорий которого может
        # занять 1,5-2 минуты
        self.ignore_subcats = False

        # В случае, когда в качестве фильтра отбора данных используется категория верхнего
        # уровня, могут обрабатываются все её дочерние подкатегории. Это может привести к
        # многократному извлечению одного и того же товара. При обработке абсолютно всех
        # дочерних подкатегорий, время работы scraper-а (а также количество запросов
        # к базе данных и сайту магазина) возрастает кратно (2-2,5 раза). Чтобы избежать
        # подобных затрат, можно обрабатывать только подкатегории, являющиеся прямым потомком
        # корневой подкатегории. Вместе с тем, алгоритм сокращения работы позволяет утверждать,
        # что "сокращённый вариант" приводит к потере 7-8% данных.
        self.ignore_children = False

        # Флаг близкий к ignore_children, но его точный антипод - игнорируются не дочерние,
        # а родительские картегории.
        self.ignore_parents = False

        # Определяем имя исследовательской функции, которая используется для экспериментальной
        # проверке гипотез и предположений
        self.research_function = None

        # Флаг определяет, какой из движков СУБД необходимо использовать:
        # sqlite - не требует администрирования, установки packages, и т.д.
        # mysql - производительная, мощная СУБД с кучей инструментов оптимизации запросов
        self.use_sqlite = True

        # Флаг определяет, нужно ли сохранять дополнительную информацию, помогающую
        # в поиске ошибок в отдельный файл
        self.dump_info = False

        # Флаг позволяет задать режим экспорта данных, в котором группировка товаров
        # не выполняется
        self.random_product_type = False

        # В случае, если необходимо только считывать данные из базы, но не добавлять
        # в неё данные, то можно использовать копию базы данных, полностью помещаемую
        # в память - это позволит значительно ускорить время получения данных из базы
        # если эту возможность поддерживает движок (SQLite поддерживает)
        self.use_in_memory_database = False

        # В случае, если необходимо уменьшить объём JSON-файла с продуктами для
        # эскпорта в MongoDB, можно заменить общие части URL с информацией о
        # товарах, или с ссылками на картинки на их сокращённый вариант. Это
        # позволяет уменьшить размер файлов на ~15%. К сожалению, процедура
        # требует восстановления данных на frontend-e, что не в радость
        # frontend-программистам. По этой причине, функция реализована как
        # опциональная и без указания особенного ключа - отключена
        self.optimize_urls = False

        # Параметр содержит два символа - характеризующие диапазон символов, с которых
        # могут начинаться выгружаемые в JSON индексы продуктов. Этот параметр введён
        # в Issue #93 для того, чтобы снизить объём экспортируемых файлов до
        # примелемого объёма (не более полутора гигабайт памяти)
        self.json_limit = None

        # Устанавливаем имя базы данных с которой будет осуществляться дальнейшая работа
        self.database_name = 'products'

        # Определяем настройки подключения к базе данных MySQL "по умолчанию"
        self.database_host = '127.0.0.1'
        self.database_user = 'admin'
        self.database_password = '12345678'
        self.database_port = 3306

        # Фиксируем список scraper-ов, которыми может оперировать система
        self.scrapers_list = [
            DetMir_CatalogScraper.DetMirCatalogScraper,                     # Детской Мир
            Dochkisinochki_CatalogScraper.DochkisinochkiCatalogScraper,     # Дочки-Сыночки
            Korablik_CatalogScraper.KorablikCatalogScraper,                 # Кораблик
            MirKubikov_CatalogScraper.MirKubikovCatalogScraper,             # Мир Кубиков
            KupiKubik_CatalogScraper.KupiKubikCatalogScraper                # Купи-Кубик
        ]

    # Функция осуществляет разбор командной строки и запускает одну из команд
    # на исполнение
    def run(self):

        # Первым параметром командной строки должна быть команда. Допустимы следующие
        # варианты:
        #   validate  - проверить актуальность screper-ов
        #   extract   - извлечь данные из магазинов
        #   json      - выгрузить данные в JSON-файл
        #
        # Второй обязательный параметр - имя базы данных. Методологическая
        # рекомендация: имеет смысл разделять базу данных для промышленного
        # извлечения данный, базу данных для проверки корректности извлечения
        # данных и исследовательскую базу данных. Это позволяет выполнять
        # разные задачи, без необходимость выполнения backup/restore
        # накопленных полезных данных.
        #
        # Дополнительные параметры:
        #   --shop=[название магазина:detmir|ds|korablik|mirkubikov|kupikubik|all]
        #   --filter=[какое подмножество необходимо выгружать]
        #   --categories=[максимальное количество категорий для обработки]
        #   --details=[максимальное количество загрузок страниц с атрибутами товаров]
        #   --update    Выполнять циключеское обновление детальной информации по товарам
        #   --offline   Выполнять только те действия, которые могут быть выполнены offline
        #   --ignore_subcats    Не извлекать подкатегории, если операция ресурсоёмкая (DEBUG)
        #   --json_limit        Ограничить экспортируем товары по первой букве
        #   --optimize_urls     Оптимизировать написание URL-ов в JSON-экспорте
        #
        # По умолчанию, приложение работает со встроенной базой данных на базе SQLite.
        # Вместе с тем, система может работать с MySQL, для подключения к которой необходимо
        # использовать дополнительные параметры подключения:
        #   --host=[адрес сервера базы данных] - актуально только для MySQL
        #   --user=[имя пользователя базы данных] - актуально только для MySQL
        #   --password=[пароль доступа к базе данных] - актуально только для MySQL
        #
        # Рекомендация: для проверки работоспособности базы данных, используйте
        # отдельную базу данных, для того, чтобы проверка не привела к потере
        # данных в "боевой" базе

        parser = argparse.ArgumentParser(description='Scraping.ArgumentParser')
        parser.add_argument('command')
        parser.add_argument('database', type=str, default='products')
        parser.add_argument('--shop')
        parser.add_argument('--filter')
        parser.add_argument('--categories', type=int, default=3)
        parser.add_argument('--details', type=int, default=5)
        parser.add_argument('--host')
        parser.add_argument('--user')
        parser.add_argument('--password')
        parser.add_argument('--port', type=int, default=3306)
        parser.add_argument('--json_limit', type=str, default=None)
        parser.add_argument('--function', type=str, default='clear_data')

        # Значение 'store_true' в параметре 'action' означает, что если в
        # командной строке будет найдена строка "--sqlite", то параметр
        # "sqlite" будет установлен в значение True
        parser.add_argument('--sqlite', action='store_true')
        parser.add_argument('--update', action='store_true')
        parser.add_argument('--no-update', action='store_false')
        parser.add_argument('--offline', action='store_true')
        parser.add_argument('--ignore_subcats', action='store_true')
        parser.add_argument('--dump_info', action='store_true')
        parser.add_argument('--random_product_type', action='store_true')
        parser.add_argument('--ignore_children', action='store_true')
        parser.add_argument('--ignore_parents', action='store_true')
        parser.add_argument('--optimize_urls', action='store_true')
        parser.add_argument('--extract_var_props', action='store_true')

        args = parser.parse_args()

        # Сначала обрабатываем дополнительные настройки
        if args.shop is not None:
            if args.shop.lower() == "detmir":
                self.scrapers_list = [DetMir_CatalogScraper.DetMirCatalogScraper]

            elif args.shop.lower() == "dochkisinochki" or args.shop.lower() == "ds":
                self.scrapers_list = [Dochkisinochki_CatalogScraper.DochkisinochkiCatalogScraper]

            elif args.shop.lower() == "korablik":
                self.scrapers_list = [Korablik_CatalogScraper.KorablikCatalogScraper]

            elif args.shop.lower() == "mirkubikov" or args.shop.lower() == "mk":
                self.scrapers_list = [MirKubikov_CatalogScraper.MirKubikovCatalogScraper]

            elif args.shop.lower() == "kupikubik" or args.shop.lower() == "kk":
                self.scrapers_list = [KupiKubik_CatalogScraper.KupiKubikCatalogScraper]

            # Параметр --shop=all может быть очень информативен
            elif args.shop.lower() == "all":
                pass

            # Предотвращаем возможность использования ошибочного названия магазина
            else:
                print('Ошибочное имя магазина - "{}"'.format(args.shop))
                exit(1)

        # Обрабатываем настройки, связанные с объёмом задач извлечения данных
        if args.categories is not None:
            self.scrape_categories_to_process = args.categories

        if args.details is not None:
            self.scrape_details_by_count = args.details

        if args.filter is None:
            self.filter = "ALL"
        else:
            self.filter = args.filter

        if args.update is None:
            self.update_details = False
        else:
            self.update_details = args.update

        if args.extract_var_props is None:
            self.extract_var_props = False
        else:
            self.extract_var_props = args.extract_var_props

        if args.offline is None:
            self.offline = False
        else:
            self.offline = args.offline

        if args.ignore_subcats is None:
            self.ignore_subcats = False
        else:
            self.ignore_subcats = args.ignore_subcats

        if args.optimize_urls is None:
            self.optimize_urls = False
        else:
            self.optimize_urls = args.optimize_urls

        if args.function is None:
            self.research_function = None
        else:
            self.research_function = args.function

        if args.dump_info is None:
            self.dump_info = False
        else:
            self.dump_info = args.dump_info

        if args.random_product_type is None:
            self.random_product_type = False
        else:
            self.random_product_type = args.random_product_type

        if args.ignore_children is None:
            self.ignore_children = False
        else:
            self.ignore_children = args.ignore_children

        if args.ignore_parents is None:
            self.ignore_parents = False
        else:
            self.ignore_parents = args.ignore_parents

        # Обрабатываем настройки, связанные с каналом связи с базой данных
        if args.database is not None:
            self.database_name = args.database

        if args.host is not None:

            self.use_sqlite = False
            self.database_host = args.host

            if args.user is not None:
                self.database_user = args.user

            if args.password is not None:
                self.database_password = args.password

            if args.port is not None:
                self.database_port = args.port

        # Обрабатываем настройки, отвечающие за ограничение выгружаемых
        # в JSON-данных
        if args.json_limit is not None:
            self.json_limit = args.json_limit

        # ТОЛЬКО ДЛЯ ОТЛАДКИ КОДА: чтобы не сбрасывать настройки командной строки для
        # подключения к MySQL (логин/пароль), в тех случаях, когда нужно использовать
        # движок SQLite, можно использовать специальный ключ "--sqlite", который
        # явно указывает, что будет использоваться именно SQLite
        if args.sqlite:
            self.use_sqlite = True

        # Выводим в логи условия выполнения скрипта
        logging.warning('Параметры запуска скрипта:')
        if self.use_sqlite:

            # Параметры подключения к базе данных SQLite
            logging.warning('  База данных: SQLite')
            if self.database_name.find('.') == -1:
                self.database_name += '.db'
            logging.warning('  Файл базы данных: "{}"'.format(self.database_name))

        else:

            # Параметры подключения к базе данных MySQL
            logging.warning('  База данных: MySQL')
            logging.warning('  Host: {}'.format(self.database_host))
            logging.warning('  Login: "{}"'.format('*' * len(self.database_user)))
            logging.warning('  Password: "{}"'.format('*' * len(self.database_password)))
            logging.warning('  Имя базы данных (schema): "{}"'.format(self.database_name))

        if len(self.scrapers_list) > 1:
            logging.warning('  Обрабатывать все магазины')
        else:
            # Нюанс: к этому моменту объект ещё не проинициализирован
            logging.warning('  Магазин: "{}"'.format(args.shop))

        logging.warning('  Фильтр отбора: "{}"'.format(self.filter))

        # Issue #103. Выводим информацию об ошибках в настройке параметров
        # запуска скрипта
        try:
            # Выполняем команды извлечения данных, или их экспорта
            if args.command.lower() == "validate" or args.command.lower() == "v":

                # Запускаем процесс проверки актуальности кода извлечения данных
                scrapers.validate()

            elif args.command.lower() == "extract" or args.command.lower() == "x":

                logging.warning('  Извлечь данные из {} категорий'.format(self.scrape_categories_to_process))
                logging.warning('  Извлечь атрибуты о {} товарах'.format(self.scrape_details_by_count))

                # Запускаем процесс извлечения данных из магазинов
                scrapers.extract()

            elif args.command.lower() == "research" or args.command.lower() == "r":

                # Команда осуществляет некоторые исследовательские действия, которые
                # периодически перерабатываются
                scrapers.research()

            elif args.command.lower() == "json" or args.command.lower() == "j":

                # Осуществляем экспорт всех ранее извлечённых данных в JSON-файл
                scrapers.json()

            elif args.command.lower() == "cleanup" or args.command.lower() == "c":

                # Осуществляем чистку базы данных от устаревших данных. Под устаревшими
                # подразумеваются записи, связанные с данными о продукте (таблица Products),
                # у которой поле outdated установлено в 1. Изменение значения поля
                # происходит в процессе актуализации данных о товаре
                scrapers.cleanup()

            else:
                print('Error: Incorrect value of the command')

        except ValueError as error:
            print(error.args[0])

    # Функция осуществляет проверку корректности извлечения данных из различных
    # магазинов. Проверка осуществляется следующим образом:
    # Из каждого магазина извлекаются категории и по несколько товарных групп.
    # В соответствующих базах данных должны быть какие-то данные. Если таблица
    # с данными - пустая, то считается, что данные не были успешно извлечены
    def validate(self):

        print("Начинается проверка корректности работы модулей извлечения данных...")
        start_time = time.time()

        # Используем имя базы данных с префиксом "check" - это временная
        # база данных, которую мы используем только в целях проверки
        # работоспособности классов извлечения данных из магазинов
        db_helper = ScrapingDatabase.DatabaseHelper(self)

        # Очищаем базу данных с тем, чтобы избежать "наложения" данных, т.е.
        # ситуаций, в которых ранее накопленные данные скрывают возникновение
        # ошибок при импорте данных
        db_helper.clear_tables()

        success = self._execute_database_tests(db_helper)
        if not success:
            print('Ошибка выполнения тестов взаимодействия с базой данных')
            return

        # Запускаем процедуру проверки работоспособности scraper-ов.
        # Извлечение осуществляем в отдельную базу данных с тем, чтобы
        # проверка не влияла на ранее накопленные "боевые" данные
        results = []
        for scrape_obj in self.scrapers_list:
            # Второй параметр вызова - ссылка на экземпляр объектра Scrapers,
            # в котором содержатся атрибуты, связанные с особенностями извлечения
            # данных из online-магазинов. Например: scrape_details_by_count,
            # scrape_categories_to_process
            scraper = scrape_obj(db_helper, self)

            print("Загрузка категорий магазина '{}'...".format(scraper.get_shop_name()))

            # Осуществляем тестовое извлечение данных из магазина.
            # Указываем пустой фильтр - отбираться должны случайные категории
            scraper.scrape_catalog(self.filter)

            # Добавляем в отчёт название магазина
            message = "Магазин '{}':".format(scraper.get_shop_name())
            results.append(message)

            # Проверяем наличие в базе хоть каких-нибудь данные. Даже при частичном
            # извлечении данных, что-нибудь в таблице должно быть
            db_helper.check_useful_data(results)

        # Выводим отчёт по результатам тестового извлечения данных
        print("--- ОТЧЁТ ПО ТЕСТОВОМУ ИЗВЛЕЧЕНИЮ ДАННЫХ ---")
        for message in results:
            print(message)

        print("Проверка актуальности кода приложения заняла: {}".format(
            int(round((time.time() - start_time) * 1000))))

    # Функция извлекает из магазинов информацию с учётом указанных дополнительных условий
    def extract(self):

        # Создаём объект работы с базой данных и параметризируем его именем
        # базы данных. Это позволяет использовать разные базы данных, например,
        # при поэтапном извлечении данных и при проверке работоспособности
        # scraper-ов
        db_helper = ScrapingDatabase.DatabaseHelper(self)

        # Загружаем в память справочник моделей товаров в связке с брендами
        db_helper.load_unified_models_json('models.json')

        # Загружаем в память справочник вариаций товаров
        db_helper.load_unified_variations_json('variations3.json')

        for scrape_obj in self.scrapers_list:

            # Запоминаем время начала операции импорта каталога товаров
            start_time = time.time()

            # Класс извлечения данных из магазина параметризуется объектом
            # взаимодействия с базой данных. В зависимости от сценариев использования,
            # могут применяться разные базы данных и алгоритмы хранения
            scraper = scrape_obj(db_helper, self)

            print("Начинается извлечение информации о товарах магазина '{}'...".format(scraper.get_shop_name()))

            # Осуществляем извлечение данных с использованием фильтра. Использование
            # фильтра позволяет управлять частотой обновления данных конкретных категорий:
            # в случае, если информация о товарах в категории обновляется редко, то нет
            # смысла напрасно расходовать ценные вычислительные ресурсы
            if not self.offline:
                scraper.scrape_catalog(self.filter)

            # Выделяем дополнительные атрибуты из извлечённых данных. В частности, может
            # быть важным найти "короткое имя товара", в котором не содержится название бренда,
            # цвет, вес и что-нибудь ещё, что уже накоплено в других атрибутах
            extract_attrs_time = time.time()

            print("Начинается извлечение атрибутов товаров...")
            scraper.extract_attributes(self.filter)
            print("Извлечение атрибутов заняло: {} мс.".format(int(round((time.time() - extract_attrs_time) * 1000))))

            # Фиксируем общее время обработки сайта - эта информация может быть
            # полезна в дальнейшей работе, когда необходимо будет планировать
            # систематический сбор информации о товарах и ценах
            print("Извлечение данных ('{}') заняло: {} мс.".format(scraper.get_shop_name(),
                                                                   int(round((time.time() - start_time) * 1000))))

    # Исследовательская функция, позволяет извлечь какие-то дополнительные данные
    # в исследовательском режиме
    def research(self):

        db_helper = ScrapingDatabase.DatabaseHelper(self)

        print("Начинается исследовательская деятельность, по фильтру '{}'...".format(self.filter))
        start_time = time.time()

        # Данные исследовательский блок генерирует список моделей по текущей базе данных
        for scrape_obj in self.scrapers_list:
            scraper = scrape_obj(db_helper, self)

            try:

                if self.research_function is None or "CLEAR_DATA" == self.research_function.upper():

                    # Применить очистку данных ко всем scraper-ам
                    scraper.clean_existing_data()

                elif "FIND_ANY_MODELS" == self.research_function.upper():

                    # Добавить отдельный флаг для указания конкретного набора
                    # исследовательских действий
                    scraper.find_any_models(self.filter)

                elif "IMPORT_SPECIAL_DETAILS" == self.research_function.upper():

                    # Добавить отдельный флаг для указания конкретного набора
                    # исследовательских действий
                    scraper.import_special_details()

                elif "CHECK_VARIATIONS_EXISTENCE" == self.research_function.upper():

                    # Осуществляем проверку наличия в базе данных прямых ссылок
                    # на все вариации зарегистрированных товаров. Необходимо
                    # явным образом указывать фильтр для товаров
                    scraper.check_variations_existence(self.filter)

                else:

                    print("Исследовательская функция не распознана: '{}'".format(self.research_function))
                    return

            except AttributeError:
                # Если у объекта класса отсутствует метод find_any_models(),
                # то это нормально. Игнорируем исключение.
                pass

        # Вот этот исследовательский блок осуществляет генерацию словаря цветов, которые
        # явным образом упоминались в свойствах товаров, именно как цвета
        # Формируем SQL-запрос отбора товаров конкретного магазина

        # Сначала считываем из базы данных все известные уникальные цвета
        '''sql_command = "SELECT DISTINCT prop_value FROM Properties " \
                      "INNER JOIN PropNames ON Properties.prop_key_id = PropNames.prop_name_id " \
                      "WHERE PropNames.Prop_Name = 'Цвет'"

        cur = db_helper.connection.cursor()
        cur.execute(sql_command)

        all_colors = []
        for row in cur.fetchall():
            all_colors.append(row[0])

        # Сортируем все цвета в алфавитном порядке
        all_colors.sort()

        # Распечатываем цвета в консоли в JSON-формате
        json_colors = ',\n\t'.join(('"' + color + '"') for color in all_colors)
        print('"[\n{}\n],'.format(json_colors))'''

        print("Обработка данных заняла: {}".format(int(round((time.time() - start_time) * 1000))))

    # Функция осуществляет экспорт накопленных через scraper-ы отдельных магазинов,
    # данных через callback-функцию, в JSON-файл, который затем предполагается
    # поместить в базу данных на движке MongoDB. После того, как данные
    # импортированы из магазинов, они подвергаются дополнительной обработке
    # с целью выполнения требований к формату "variations"
    def json(self):

        # Явным образом указываем, что данные из базы будем только считывать,
        # что позволит разместить базу данных в памяти и значительно ускорить
        # работу с ней
        self.use_in_memory_database = True

        db_helper = ScrapingDatabase.DatabaseHelper(self)

        # Загружаем в память справочник моделей товаров в связке с брендами
        db_helper.load_unified_models_json('models.json')

        # Загружаем в память справочник вариаций товаров
        db_helper.load_unified_variations_json('variations3.json')

        print("Начинается экспорт данных о товарах, по фильтру '{}'...".format(self.filter))

        # Организуем хранилище данных как словарь
        grouping = GroupingByAlternativeIndexes()

        # Первым действием получаем у каждого из магазинов список товаров
        # для экспорта
        start_time = time.time()
        for scrape_obj in self.scrapers_list:
            scraper = scrape_obj(db_helper, self)
            scraper.export_products(self.filter, grouping)

        print("Сериализация данных из БД в Python-объект: {} мс.".format(int(round((time.time() - start_time) * 1000))))

        # Сформированные данные передаём в специализированную функцию-строитель,
        # которая осуществит подготовку данных для экспорта в JSON, в формате
        # с использованием "variations"
        start_time = time.time()
        export_products_to_json(grouping.get_products(), db_helper, self.filter, optimize_urls=self.optimize_urls)

        print('Экспорт данных в json-файл занял: {}'.format(int(round((time.time() - start_time) * 1000))))
        logging.warning('Экспорт данных в json-файл занял: "{}"'.format(int(round((time.time() - start_time) * 1000))))

    # Функция осуществляет очистку базы от устаревших данных (записи о товарах, которые
    # уже не актуальны)
    def cleanup(self):

        print('Осуществляется чистка базы данных от устаревших данных')

        # Метод cascade_delete_outdated() формирует SQL-запросы без привязки
        # к конкретному магазину, что позволяет не создавать экземпляры
        # классов scraper-ов, а обращаться непосредственно к DatabaseHelper-у
        db_helper = ScrapingDatabase.DatabaseHelper(self)
        db_helper.cascade_delete_outdated()

    # Выполняем набор тестов связанных с корректностью работы класса ScrapingDatabase
    def _execute_database_tests(self, db_helper):

        # Выполняем набор тестов в специализированном Scraper-е
        scraper = Tester_CatalogScraper.TesterCatalogScraper(db_helper, self)
        return scraper.execute_tests()


if __name__ == "__main__":
    # Создаём объект для извлечения данных из online-магазинов
    scrapers = Scrapers()
    scrapers.run()
