#!/usr/bin/python
# -*- coding: utf-8

import re
import string
import random
import logging
from ProductDescription import ProductContainer


# Функция осуществляет преобразование суммы из строчного формата
# в числовой, с учётом символа разделителей тысяч рублей
def parse_amount(price_as_string, separator='?'):

    result = ""
    for char in price_as_string:
        if char.isdigit():
            result += char
        else:

            # Символ-разделитель нужен для того, чтобы прекратить преобразование
            # текста в сумму, когда доходим до разделителя копеек
            if char == separator:
                break

    # Конвертируем символическое значение в цифровое
    try:
        return int(result)
    except ValueError:
        return 0


# Функция возвращает True, если указанное слово может быть
# артикулом товара (применяется к LEGO)
def could_be_an_article(word):

    # Считаем, что слово может быть артикулом, если
    # оно состоит не менее, чем из 4 цифр, среди между
    # которыми может быть использован символ тире (минус)
    if len(word) < 4:
        return False

    for char in word:

        if not char.isdigit():
            if '-' != char:
                return False

    return True


# Функция выделяет текст, который размещается в круглых скобках
# и формирует список из двух элементов, первых из которых содержит
# текст вне скобок, а второй - текст внутри скобок
def split_embraced_text(text, left_separator='(', right_separator=')'):

    left_brace = text.find(left_separator)
    if left_brace >= 0:
        right_brace = text.find(right_separator, left_brace+1)
        if right_brace > 0:

            resulting_text = text[:left_brace].strip()
            text_inside = text[left_brace + 1:right_brace]

            if len(text) > right_brace + 1:
                resulting_text += ' '
                resulting_text += text[right_brace + 1:].strip()

            return [resulting_text, text_inside]

        # Issue #65. В названии товара может встретится открывающая скобка
        # без соответствующей закрывающей. В этом случае, нужно убрать из
        # названия открывающую скобку, чтобы не уйти в вечный цикл
        text = text.replace(left_separator, '')

    return [text, '']


# Функция выделяет максимально длинную общую часть двух строк с учётом символов
# разделителей. Первоначальная реализация алгоритма была взята с сайта
#   https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring
# К сожалению, оригинальный вариант позволял случайным совпадениям критически
# влиять на качество работы, что приводило к курьёзам. По этой причине, алгоритм
# был значительно упрощён
def longest_common_substring(s1, s2):

    c1 = re.findall(r"[\w']+", s1.upper())
    c2 = re.findall(r"[\w']+", s2.upper())

    min_count = min(len(c1), len(c2))

    common_part = ''
    index = 0
    while index < min_count:

        # Особое условие - слово 'ЦВЕТ' в явном виде означает начало
        # блока дополнительных параметров товара, т.е. название модели
        # закончилось
        if c1[index] == "ЦВЕТ":
            break

        if c1[index] != c2[index]:
            break

        if index > 0:
            common_part += ' '

        common_part += c1[index]

        index += 1

    return common_part

'''
    # Оригинальный вариант
    m = [[0] * (1 + len(s2)) for i in range(1 + len(s1))]
    longest, x_longest = 0, 0
    for x in range(1, 1 + len(s1)):
        for y in range(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
                    x_longest = x
            else:
                m[x][y] = 0
    return s1[x_longest - longest: x_longest]
'''


# Функция выделяет реальное (trademark) название товара, из комбинированной
# строки, которая может содержать название товара, а так же другие атрибуты,
# такие как цвет
def get_real_product_name(dictionary_of_names, brand, name):

    if dictionary_of_names.get(brand) is not None:

        for real_name in dictionary_of_names[brand]:

            if longest_common_substring(name, real_name) == real_name:
                return real_name

    return name


# Функция удаляет из biggest_string подстроку, соответствующую shortest_string.
# Практическая ценность: для того, чтобы выделить атрибуты товара, из строки
# с названием товара и его атрибутами нужно исключить название
def get_difference(biggest_string, shortest_string):

    if biggest_string is None or shortest_string is None:
        return None

    # Вводим ограничение по длине подстроки (shortest_string). Названием
    # товара может являться, например "Я" и в этом случае, алгоритм
    # работы сломается полностью
    if len(shortest_string) < 3:
        return biggest_string

    pos = biggest_string.find(shortest_string)
    if pos == -1:
        return None

    left_part = ''
    if pos > 0:
        left_part = biggest_string[:pos].strip() + ' '

    return left_part + biggest_string[pos+len(shortest_string):].strip()


# Функция создаёт случайную строку указанного размера, состоящую из цифр и символов
# латинского алфавита.
# Можно использовать random.SystemRandom(), который использует криптографические
# алгоритмы для генерации псевдо-случайных чисел: /dev/urandom на Linux-машинах и
# CryptGenRandom() в операционной среде Microsoft Windows
def generator_random_string(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# Функция объединяет название сайта с локальным путём к ресурсу таким образом, чтобы
# избежать двойного слеша (/)
def concat_path(site, relative_path):

    useful_url = site
    if site[-1:] == '/' and relative_path[:1] == '/':
        useful_url = site[:-1]

    useful_url += relative_path
    return useful_url


# Функция осуществляет поиск вариаций товара в строке названия товара,
# используя унифицированные правила поиска
def find_unified_attrs(product_name, unified_dict):

    if unified_dict is None:
        return product_name, None

    if len(product_name) == 0:
        return product_name, None

    # Выполняем дополнительную обработку названия товара. См. #115:
    # - в названии встречаются двойные кавычки
    # - текст сообщения может начинаться и заканчиваться двойными кавычками
    product_name = product_name.replace('""', '"')
    if len(product_name) > 1 and product_name[:1] == '"' and product_name[-1:] == '"':
        product_name = product_name[1:-1]

    # Также корректируем вероятные распространённые ошибки: в частности, использование
    # латинской буквы 'Пи' вместо русской 'Рэ'
    product_name = product_name.replace(' P. ', ' Р. ')

    # Возвращаемое значение функции - массив кортежей (tuples), содержащих
    # пару значений - имя свойства и его значение
    properties = []

    # Левая позиция свойства, наиболее близко расположенная к началу описания товара
    left_attr_pos = len(product_name)

    # Формируем возможные варианты нарезки названия товара на свойства.
    # Разделяем как по специальным символам...
    elements = re.split('[(),.:;"]', product_name)

    # ...так и с выборкой комбинаций слов идущих в конце текста.
    # Такой вариант встречается, например, тогда, когда цвет
    # товара указывается без символов разделителей в конце текста
    words = product_name.split(' ')
    max_len = len(words)
    if max_len > 5:
        max_len = 5

    for i in range(max_len, 0, -1):
        for j in range(1, 4, 1):

            if -i + j > 0:
                break

            if -i + j == 0:
                elements.append(' '.join(words[-i:]))
            else:
                elements.append(' '.join(words[-i: -i + j]))

    # Выполняем итерацию по всем свойствам, указанным в ''
    for property_name in list(unified_dict.keys()):

        if 'RE' in unified_dict[property_name]:

            # Поскольку регулярные выражения могут быть достаточно ресурсоёмкими,
            # следует предусмотреть возможность не выполнять их, если такой потребности
            # можно избежать. Именно для подобных целей и используется поле 'PRECHECK',
            # которое содержит редко встречающуюся подстроку, которая должна обязательно
            # быть в названии продукта, если он содержит искомое свойство
            if 'PRECHECK' in unified_dict[property_name]:
                if unified_dict[property_name]['PRECHECK'] not in product_name:
                    continue

            # Атрибут будет выделяться посредством регулярного выражения
            count = len(unified_dict[property_name]['RE'])

            if count % 2 != 0:
                print('В поле "rule" блока "{}" должно быть кратное двум кол-во значений'.format(property_name))
                continue

            for index in range(0, count, 2):

                # Первое регулярное выражение позволяет выделить атрибут
                # вместе с маркерами
                first_re = unified_dict[property_name]['RE'][index]
                stage_one = re.findall(first_re, product_name.upper())
                if len(stage_one) == 0:
                    continue

                # Запоминаем позицию начала маркера, вместе с атрибутом
                pos = product_name.upper().find(stage_one[0])
                if pos < left_attr_pos:
                    left_attr_pos = pos

                # Второе регулярное выражение позволяет убрать маркер и
                # оставить только фактическое значение выражения
                second_re = unified_dict[property_name]['RE'][index + 1]
                stage_two = re.findall(second_re, stage_one[0])
                if len(stage_two) == 0:
                    continue

                # Следующую строку кода я сохранил, поскольку решил, что
                # главная задача - извлечение полезного свойства из названия
                # товара решена. Классификацию товаров, всё-таки лучше
                # делать не пытаясь удалять то, в чём мы сомневаемся (цифры,
                # знаки препинания, акронимы), а выделяя то, в чём мы точно
                # не сомневаеся, например, название модели определённого бренда
                # product_name = product_name.replace(stage_one[0], '')

                if _is_unique_property(properties, property_name, stage_two[0]):
                    properties.append((property_name, stage_two[0]))
                break

            # Продолжаем искать другие свойства в названии товара
            continue

        if 'INPUT' not in unified_dict[property_name]:
            print('Отсутствует поле "input" в блоке: "{}"'.format(property_name))
            continue

        # Перебираем варианты названий свойств - ищем варианты свойств
        for text_block in elements:

            product_prop = text_block.strip()
            if len(product_prop) == 0:
                continue

            container = unified_dict[property_name]['INPUT']

            if isinstance(container, dict):

                for unified_value in list(container.keys()):
                    if product_prop in container[unified_value]:

                        if _is_unique_property(properties, property_name, unified_value):
                            properties.append((property_name, unified_value))

                        # Запоминаем левую позицию найденного свойства
                        pos = product_name.upper().find(product_prop)
                        if pos < left_attr_pos:
                            left_attr_pos = pos

                        # Дальше над этим свойством не работаем - оно получено
                        break

            elif isinstance(container, list):

                if product_prop in container:

                    # Запоминаем левую позицию найденного свойства
                    pos = product_name.upper().find(product_prop)
                    if pos < left_attr_pos:
                        left_attr_pos = pos

                    # Добавляем свойство в хранилище
                    if _is_unique_property(properties, property_name, product_prop):
                        properties.append((property_name, product_prop))

                    # Дальше над этим свойством не работаем - оно получено
                    break

            else:
                print('Не корректный тип поля input: "{}"'.format(property_name))

    # Удаляем все символы, которые находятся после особенных символов-разделителей
    blocks = re.split('[():]', product_name[:left_attr_pos])
    if len(blocks) > 0:
        left_attr_pos = len(blocks[0])

    # Двигаясь с конца, доходим до первого легального символа (цифра, символ алфавита,
    # либо одинарная/двойная кавычка)
    for i in range(left_attr_pos - 1, 0, -1):
        if product_name[i].isalpha() or product_name[i].isdigit() or product_name[i] == '"' or product_name[i] == "'":
            break
        left_attr_pos = i

    # Удаляем особенные слова из названия модели, например, слово "ЦВЕТ" в
    # самом конце. Очевидно, что такие слова - остатки от названий свойств
    # товара
    short_name = product_name[:left_attr_pos].strip()
    words = short_name.split(' ')

    if len(words) > 0:
        if words[-1:][0] == 'ЦВЕТ':
            short_name = ' '.join(words[:-1])

    # Важный момент состоит в том, что у нас может встретиться в названии
    # товара не парная ковычка. В этом случае, её необходимо убирать.
    # Такая ситуация может возникнуть, когда значение свойства обёрнуто
    # в кавычки, например:
    #   КОЛЯСКА CHICCO SUPER FLEX "AUTUMN GOLD"
    if short_name.count("'") % 2 != 0:
        pos = short_name.rfind("'")
        if pos > 0:
            short_name = short_name[:pos].strip()

    if short_name.count('"') % 2 != 0:
        pos = short_name.rfind('"')
        if pos > 0:
            short_name = short_name[:pos].strip()

    # Если после названия бренда шла запятая, или какой-то другой служебный символ, то его
    # нужно исключить из строки
    has_any_acceptable_char = False
    first_acceptable_char = 0
    length = len(short_name)
    for i in range(0, length, 1):
        if short_name[i].isalpha() or short_name[i].isdigit() or short_name[i] == '"' or short_name[i] == "'":
            has_any_acceptable_char = True
            break
        first_acceptable_char = i

    if has_any_acceptable_char:

        if first_acceptable_char > 0:
            short_name = short_name[first_acceptable_char:].strip()
    else:

        # В названии модели могло не остаться ни одного приемлемого символа
        # (например - ','). В этом случае, считаем, что модель не указана
        short_name = ''

    # Issue #74. Если последний символ запятая, то удаляем его
    if len(short_name) > 0:
        if short_name[-1:] == ',':
            short_name = short_name[:-1].strip()

    # Возвращаем как название товара без атрибутов, так и выделенный набор свойств
    return short_name, properties


# Вспомогательная Функция проверяет, содержится ли свойство с именем
# new_property_name и значением new_product_value в контейнере
# properties. Другими словами - отвечает на вопрос: является ли
# указанное значение новым?
# Потребность связана с наличием в базе данных таких товаров как:
#   Подгузники Merries S (4-8 кг) 82 шт
#   Подгузники Merries M (6-11 кг) 22 шт.
# В обоих случаях, вес ребёнка S и 4-8 кг будет продублирован
def _is_unique_property(properties, new_property_name, new_product_value):

    for prop in properties:
        if prop[0] == new_property_name and prop[1] == new_product_value:
            return False

    return True


# Функция формирует унифицированный артикул товара, состоящий из
# типа товара, бренда и названия модели
def gen_article(product_type, brand, real_name):
    """
    :param str product_type: строка - описание типа товара (например: "КАНЦЕЛЯРСКИЕ ТОВАРЫ")
    :param str brand: компания-производитель товара, или бренд
    :param str real_name: название модели, часть названия за исключением бренда, типа товара и вариаций
    :return str
    """

    # Корректируем тип продукта
    if product_type is not None and len(product_type) > 0:
        product_type = product_type.replace('2В1', '2 В 1')
        product_type = product_type.replace('3В1', '3 В 1')

    # Объединяем все слова, описывающие товар в отдельный массив слов,
    # при этом, бренд на слова не разбиваем, т.к. встречаются бренды
    # состоящие из нескольких слов, например: "THINKING MACHINES"
    words = []
    if brand is not None:
        words.append(brand)

    if product_type is not None:
        elements = re.split('[ :,]', product_type.strip())
        words.extend(elements)

    if real_name is not None:
        elements = re.split('[ :,]', real_name.strip())
        words.extend(elements)

    # Удаляем из списка пустые элементы
    words = filter(len, words)

    # Удаляем из списка символ амперсанд
    words = filter(lambda x: x != '&', words)

    # Удаляем возможные дублирования слов из словаря. Дублирования
    # приводят к ухудшению работы классификатора
    words = list(set(words))

    # Сортировка позволяет компенсировать случаи, в которых тип продукта
    # указывается в модели, или наоборот
    words.sort()

    # Формируем артикул товара
    article = '-'.join(words)
    return article


# Функция преобразовывает тип товара в строку, которую можно использовать
# как индекс в случаях, когда порядок слов может изменяться, например,
# тип товара может называться как 'Коляска Прогулочная', так и 'прогулочная
# коляска'
def product_type_as_sorted_str(product_type):
    """
    :param str product_type: строка - описание типа товара (например: "КАНЦЕЛЯРСКИЕ ТОВАРЫ")
    :return str
    """

    if product_type is None or len(product_type) == 0:
        return ''

    words = product_type.upper().split(' ')

    # Удаляем из списка пустые элементы
    words = filter(len, words)

    # Удаляем возможные дублирования слов из словаря. Дублирования
    # приводят к ухудшению работы классификатора
    words = list(set(words))

    # Сортировка позволяет компенсировать случаи, в которых порядок слов
    # приводит к ухудшению качества группировки товаров
    words.sort()

    return '-'.join(words)


# Функция обрабатывает список товаров одного производителя, в одной категории, с
# одинаковой ценой с целью выделить общую часть названия товара, если она есть
def extract_dictionary_of_models(temporary_dict, prop_name_brand='brand', prop_name_aux='short_names'):

    dictionary_of_names = {}

    for product in list(temporary_dict.values()):

        if product.get(prop_name_aux) is None:
            logging.critical('У продукта отсутствует название')
            return

        if product.get(prop_name_brand) is None:
            logging.critical('У продукта отсутствует производитель')
            return

        brand = product[prop_name_brand]

        real_product_name = get_common_part_of_model(product[prop_name_aux])
        if real_product_name is not None:

            if dictionary_of_names.get(brand) is None:
                dictionary_of_names[brand] = []

            # В список добавляем только уникальные названия товаров (моделей)
            if real_product_name not in dictionary_of_names[brand]:
                dictionary_of_names[brand].append(real_product_name)

    # После формирования списка актуальных для магазина брендов, осуществляется
    # обновление списка продуктовых линеек сформированных оператором вручную
    # и хранящихся в базе данных. Это, теоретически, позвляет улучшить качество
    # обработки и классификации товаров
    for brand in list(dictionary_of_names.keys()):
        dictionary_of_names[brand].sort(key=len, reverse=True)

    return dictionary_of_names


def group_products_by_price(product, append_to, prop_name_brand='brand'):

    brand = product.get_brand()
    category = product.get_property('category')

    # Применяем условия применимость полученных данных для выделения торговой марки
    if brand is None or category is None or product.offers_count() != 1:
        logging.warning('Не выполнены условия обработки товарной позиции')
        return

    price = product.container[product.OFFERS][0]['price']

    # Формируем составной ключ товара: бренд + категория + цена товара
    article = brand + '_' + category + '_' + str(price)

    # После того, как свойства продукта получены и сформирован ключ
    # (артикул) товар, выполняем непосредственное объединение данных
    if append_to.get(article) is None:
        append_to[article] = {}

    # Сохраняем информацию о бренде - она будет нужна при выделении
    # названий товаров выпускаемых данным брендом
    append_to[article][prop_name_brand] = brand

    # Получаем название товара - очищенный вариант должен хранится
    # в свойстве 'short_name' и если его нет, то в основной записе о товаре
    short_name = product.get_property('short_name')
    if short_name is None:
        short_name = product.get_product_name()

    # Из строки с избыточным названием товара исключаем название бренда - это
    # избыточная информация
    pos = short_name.find(brand)
    if pos >= 0:
        short_name = short_name[pos + len(brand):]

    short_names_field_name = 'short_names'

    # Включаем название товара в общий список названий товаров
    if append_to[article].get(short_names_field_name) is None:
        append_to[article][short_names_field_name] = []

    append_to[article][short_names_field_name].append(short_name)


# Реализация алгоритма выделения названия модели продукта
# по описанию товара, включая дополнительные реквизиты
def get_common_part_of_model(product_complex_names):

    if len(product_complex_names) < 2:
        return None

    # Для информации: раньше использовался алгоритм "Longest Common Substring"
    # который не учитывал наличие символов разделителей, что приводило к различным
    # казусным ситуациям, когда случайные совпадения критично влияли на
    # качество результата, например:
    # CHICCO ACTIVE 3 КРАСНАЯ и CHICCO ACTIVE 3 КРАСНЫЙ, могут дать
    # максимально длинное общее: "CHICCO ACTIVE 3 КРАСН"

    # Сортируем названия товаров по длине для того, чтобы наиболее длинные шли первыми в списке.
    # В этом случае, удасться исключить варианты, когда в списке есть, например, и 'Trilogy',
    # и 'Trilogy Quad'
    product_names = product_complex_names
    product_names.sort(key=len, reverse=True)

    common_part = product_names[0]
    for name in product_names:

        # Убираем из названия текст в круглых скобках
        result = split_embraced_text(name)
        if len(result) != 2:
            continue

        # Выделяем максимально длинную общую часть двух строк
        substring = longest_common_substring(common_part, result[0])
        if len(substring) == 0:
            return None

        common_part = substring

    # Убираем из названия специфические сокращения, такие как: "цвет", "цв.".
    common_part = common_part.replace('ЦВЕТ', ' ')
    common_part = common_part.replace('ЦВ.', ' ')
    common_part = common_part.replace('3 В 1', ' ')
    common_part = common_part.replace('/', ' ').strip()

    if len(common_part) < 2:
        return None

    return common_part


# Функция приводит написание бренда к виду, в котором вокруг символа-амперсанда
# нет окружающих его пробелов. Эти пробелы - наиболее частая причина ошибок
# разделения названия товара на отдельные составляющие
def normalize_brand(brand_name):
    return brand_name.replace('& ', '&').replace(' &', '&')


def clean_description_text(content):

    content = content.strip()

    # Можно было бы выкашивать и неразрывный пробел (non-breaking space),
    # но, в целом, он обычно очень разумно используется магазинами
    #content = content.replace(u'\u00a0', ' ')

    content = content.replace('\n', '').replace('\t', '')
    content = content.replace('<strong>', '').replace('</strong>', '')
    content = content.replace('<div>', '').replace('</div>', '')
    content = content.replace('<span>', '').replace('</span>', '')
    content = content.replace('<li>', '').replace('</li>', ' ')
    content = content.replace('<ul>', '').replace('</ul>', '')
    content = content.replace('<ol>', '').replace('</ol>', '')
    content = content.replace('<br>', '').replace('</br>', '').replace('<br/>', ' ').replace('<br />', ' ')

    # Заменяем повторно идущие пробелы на один пробел
    content = content.replace('  ', ' ')
    return content


def clean_property_value(content):

    content = content.strip()

    content = content.replace('\n', '').replace('\t', '')
    content = content.replace('<br>', '').replace('</br>', '').replace('<br/>', '; ').replace('<br />', '; ')

    # Заменяем повторно идущие пробелы на один пробел
    content = content.replace('  ', ' ')

    return content


def clean_description_in_database(db, shop_name, product_id):

    product = ProductContainer(db, product_id, shop_name)

    if product.container.get(product.OFFERS) is not None:
        if len(product.container[product.OFFERS]) == 1:
            if product.container[product.OFFERS][0].get(product.PROPERTIES) is not None:

                description = product.container[product.OFFERS][0]['Description']
                if description is not None:
                    if len(description) > 0:
                        description = clean_description_text(description)

                    db.update_product_description(product_id, description)
