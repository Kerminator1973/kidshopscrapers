#!/usr/bin/python
# -*- coding: utf-8

import logging
import sqlite3
import html
import time
import json
import codecs
import mysql.connector
from time import localtime, strftime
import ScrapingHelper       #

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO


class DatabaseHelper:

    IGNORE_OLD_PRICE = 0

    def __init__(self, options):

        # Устанавливаем shop_id, привязывающим текущие операции к магазину,
        # находящемуся в обработке
        self.shop_id = -1

        # Запоминаем, какой синтаксис SQL-запросов нужно использовать (SQLite/MySQL)
        self.use_sqlite_syntax = options.use_sqlite

        # Сохраняем настройку, в соответствии с которой при импорте дополнительных
        # атрибутов товара (details) ранее обработанные товары обновляются, либо
        # игнорируются
        self.update_details = options.update_details

        # Если необходимо сохранять дополнительную трассировочную информацию
        # в отдельный файл для последующего анализа, то создаём такой файл
        self.dump_file = None
        if options.dump_info:
            self.dump_file = open("dump.txt", "w")

        # Создаём кэш имён свойств - он позволит уменьшить количество запросов
        # к базе данных, что повысит производительность работы скриптов
        self.cached_prop_names = {}

        # Словарь in_memory_photos используется для ускорения формирования
        # JSON посредством предварительной выборки данных из базы
        self.in_memory_photos = {}

        # Смысл такой же, как и у in_memory_photos
        self.in_memory_properties = {}

        # Issue #122. В отдельных случаях, можно кэшировать описание товара
        # и таким образом снижать нагрузку на базу данных
        self.cached_product_description = None

        # Инициализируем член-класса, чтобы синтаксические валидаторы (JetBrains
        # PyCharm) не ругался
        self.insertion_data = None

        # Issue #64. Создаём контейнер для промежуточного хранения информации
        # о фотографиях, связанных с товаром. Повышает производительность
        self.insertion_photos = None

        # Issue #127. Создаём контейнер для промежуточного хранения SQL-команд
        # для обновления данных
        self.pack_of_sql_commands = None

        # Issue #38. Создаём контейнер для промежуточного хранения информации
        # о добавляемых товарах. Повышает производительность
        self.insertion_products = ''

        # Создаём словарь названий моделей товаров - они используются при обработке
        # и группировке товаров
        self.unified_models = {}

        # Создаём словарь значений вариаций, посредством которого можно попытаться
        # выделить из названия товара не только строку с вариацией, но и имя свойства,
        # к которому относится полученное значение
        self.unified_variations = {}

        self.connection = None

        if self.use_sqlite_syntax:

            # sqlite - не требует администрирования, установки packages, и т.д.
            # Вариант для ознакомления с проектом, начала изучения кода.
            # Так же в SQLite очень просто осуществлять backup/restore
            # базы данных - посредством файлового копирования

            # Если расширение имени файла базы данных не указано, то добавляем его
            # явным образом - ".db"
            if options.database_name.find('.') == -1:
                options.database_name += '.db'

            # Создаём соединение с базой данных
            if options.use_in_memory_database:

                # Рецепт взят вот отсюда:
                # http://stackoverflow.com/questions/3850022/how-to-load-existing-db-file-to-memory-in-python-sqlite3

                # В случае, если база данных используется только для чтения, то
                # копируем базу из файла во временный накопитель (память)
                con = sqlite3.connect(options.database_name)
                temporary_file = StringIO()
                for line in con.iterdump():
                    temporary_file.write('%s\n' % line)
                con.close()
                temporary_file.seek(0)

                # Создаём базу данных в оперативной памяти и заполняем её данными
                # из физической базы данных. Вся дальнейшая работа будет вестись
                # исключительно через ОЗУ, что позволит повысить скорость работы в разы
                self.connection = sqlite3.connect(":memory:")
                self.connection.cursor().executescript(temporary_file.read())
                self.connection.commit()
                self.connection.row_factory = sqlite3.Row

            else:

                # Используем традиционный подход - файл с базой данных
                self.connection = sqlite3.connect(options.database_name)

        else:

            # MySql - производительный, мощный движок СУБД с инструментами
            # оптимизации запросов

            # Базы данных должны быть предварительно созданы через управляющую
            # консоль, либо инструмент типа MySQL Workbench.

            # Создаём соединение с базой данных
            try:
                self.connection = mysql.connector.connect(host=options.database_host,
                                                          port=options.database_port,
                                                          database=options.database_name,
                                                          user=options.database_user,
                                                          password=options.database_password,
                                                          charset='utf8',
                                                          buffered=True)
                if not self.connection.is_connected():
                    logging.critical("не удалось подключиться к базе данных MySQL {}".format(options.database_name))
                    exit(1)

            except mysql.connector.Error as er:
                logging.error(er)
                print(er)
                exit(1)

        # Создаём базовый набор таблиц, хранящих результаты извлечения
        # данных из online-магазинов
        self._create_tables()

        # Вспомогательный список, служит для выделения "потенциально устаревших" ссылок
        self.current_category_list = []

    # Деструктор - сохраняет накопленные изменения в базе данных
    def __del__(self):

        # commit делаем в самом конце обработки данных.
        # Предположение: такой подход должен снизить издержки связанные
        # с физической записью данных на жёсткий диск
        if self.connection is not None:
            self.connection.commit()
            self.connection.close()

        # Если был открыт файл с дополнительной трассировочной информацией,
        # то закрываем этот файл при завершении скрипта
        if self.dump_file is not None:
            self.dump_file.close()

        # Функция осуществляет попытку загрузки словаря моделей товаров
    # из файла "models.json". Этот словарь используется для улучшения
    # качества обработки названий товаров
    def load_unified_models_json(self, filename):

        print('Осуществляется попытка загрузки данных из файла "{}"'.format(filename))

        self.unified_models = json.load(codecs.open(filename, 'r', 'utf-8-sig'))
        if self.unified_models is None:
            print('Данные о моделях не были загружены.')
            return

        # Сортируем названия моделей по длине, помещая самые длиные в начало
        # списка. Это делается для того, чтобы при последовательном переборе,
        # сначала рассматривались производные названия, а потом базовые, например:
        # "TRIP ECU", "TRIP", или "WD DELUXE", "WD".
        for brand in self.unified_models.keys():
            self.unified_models[brand].sort(key=len, reverse=True)

        print('Данные о моделях товаров были успешно загружены.')

    # Функция осуществляет попытку загрузки словаря вариаций
    # из файла "variations.json". Этот словарь используется для
    # определения вариаций товаров
    def load_unified_variations_json(self, filename):

        print('Осуществляется попытка загрузки данных из файла "{}"'.format(filename))

        self.unified_variations = json.load(codecs.open(filename, 'r', 'utf-8-sig'))
        if self.unified_variations is None:
            print('Данные о вариациях не были загружены.')
            return

        # Issue #71. Все допустимые значения вариаций должны быть приведены
        # к верхнему регистру
        for property_name in list(self.unified_variations.keys()):

            if 'INPUT' in self.unified_variations[property_name]:

                container = self.unified_variations[property_name]['INPUT']

                if isinstance(container, dict):
                    for unified_value in list(container.keys()):
                        for i, element in enumerate(container[unified_value]):
                            container[unified_value][i] = element.upper()

                elif isinstance(container, list):
                    for i, element in enumerate(container):
                        container[i] = element.upper()

        print('Данные о вариациях свойств товаров были успешно загружены.')

    # Функция возвращает список названий вариаций, используя как
    # предопределённые названия свойств, так и данные извлечённые
    # из 'variations3.json'
    def get_unified_variations(self):

        # Issue #118. Легко заметить, что названия свойств определённые
        # ниже в списке, частично дублируют имена свойств из файла. Это
        # сделано не просто так - порядок следования названий свойств
        # в списке valuable_props определяют приоритет свойств при
        # выгрузке. Чем раньше идёт название свойства, тем большим
        # приоритетом оно обладает
        valuable_props = ['ЦВЕТ', 'КОЛ-ВО', 'РАЗМЕР']

        # Issue #71. Расширяем список наиболее ценных свойств за счёт данных,
        # извлечённых из справочника 'variations3.json'
        if self.unified_variations is not None:
            for property_name in list(self.unified_variations.keys()):
                property_name_upper = property_name.upper()
                if property_name_upper not in valuable_props:
                    valuable_props.append(property_name_upper)

        return valuable_props

    # Private-метод, осуществляеющий создание всех необходимых таблиц базы данных
    def _create_tables(self):

        logging.info('Проверка наличия и создание таблиц')

        # Ключевое слово "AutoIncrement" пишется по разному в MySQL и SQLite
        auto_increment = 'AUTO_INCREMENT'
        if self.use_sqlite_syntax:
            auto_increment = 'AUTOINCREMENT'

        # Определяем таблицу, в которой будут сохранены названия магазинов. Таблица
        # используется для того, чтобы при операциях с категориями, автоматически
        # добавлять фильтр по магазину, в котором осуществляется импорт данных
        self.connection.cursor().execute('''
        CREATE TABLE IF NOT EXISTS Shops (
            shop_id INTEGER NOT NULL PRIMARY KEY {},
            shop_name VARCHAR(200) NOT NULL
        );
        '''.format(auto_increment))

        # Online-магазины могут использовать много-компонентную схему описания
        # категории. Типовое решение состоит из родительской категории,
        # дочерней и дочерней второго уровня, например:
        #   "Игры и игрушки" + "LEGO" + "Lego Juniors".
        #
        # Для хранения связей родительской и дочерних категорий в реляционной
        # базе данных используется модель Materialized Path, в которой идентификаторы
        # родительских элементов перечисляются через символ-разделитель,
        # например: "1.78.165".
        #
        # Ещё один нюанс связан с полем "date_of_scraping". Поскольку извлечение
        # данных по всем категориям может занять много времени, эту задачу
        # предполагается решать по-частям. При этом, в первую очередь обрабатываются
        # те категории, данные из которые извлекались раньше остальных, либо ещё
        # не извлекались.
        self.connection.cursor().execute('''
        CREATE TABLE IF NOT EXISTS Categories (
            cat_id INTEGER NOT NULL PRIMARY KEY {},
            shop_id INTEGER NOT NULL,
            group_name VARCHAR(200) NOT NULL,
            detailed_info_url VARCHAR(255) UNIQUE NOT NULL,
            parents VARCHAR(100) NOT NULL,
            is_parent INTEGER NOT NULL,
            date_of_scraping DATETIME DEFAULT CURRENT_TIMESTAMP
        );
        '''.format(auto_increment))

        # Определяем таблицу, в которой будут сохранены товарные позиции
        # полученные в результате обработки online-магазинов
        try:
            self.connection.cursor().execute('''
            CREATE TABLE IF NOT EXISTS Products (
                product_id INTEGER NOT NULL PRIMARY KEY {},
                shop_id INTEGER NOT NULL,
                category_id INTEGER NOT NULL,
                product_name VARCHAR(300) NOT NULL,
                price INTEGER NOT NULL,
                price_old INTEGER,
                url_details VARCHAR(255) UNIQUE NOT NULL,
                outdated INTEGER,
                instock INTEGER DEFAULT 1
            );
            '''.format(auto_increment))

        except mysql.connector.Error as er:
            logging.error('Ошибка создания таблицы "Products". Details: {}'.format(er))
            pass
        except sqlite3.Error as er:
            logging.error('Ошибка создания таблицы "Products". Details: {}'.format(er.args[0]))

        # Добавляем дополнительную колонку в базу данных: полное описание товара
        try:

            if self.use_sqlite_syntax:
                # todo: для SQLite переделать поле на BLOB, т.е. 4000 байт может не хватать
                # для сохранения описания товара
                self.connection.cursor().execute('ALTER TABLE Products ADD COLUMN description VARCHAR(4000)')
            else:
                self.connection.cursor().execute('ALTER TABLE Products ADD COLUMN description TEXT')

        # Игнорируем ошибку в случае, если не удасться создать колонку
        except mysql.connector.Error:
            pass
        except sqlite3.OperationalError:
            pass

        # Добавляем дополнительную колонку в базу данных: дата обновления товара
        try:
            self.connection.cursor().execute(
                'ALTER TABLE Products ADD COLUMN date_of_scraping DATETIME DEFAULT CURRENT_TIMESTAMP')

        # Игнорируем ошибку в случае, если не удасться создать колонку
        except mysql.connector.Error:
            pass
        except sqlite3.OperationalError:
            pass

        # Определяем таблицу содержащую URL-ссылки на фотографии товаров
        try:
            sqlite_specific = ''
            if self.use_sqlite_syntax:
                sqlite_specific = ', UNIQUE(product_id, photo_url) ON CONFLICT REPLACE'

            self.connection.cursor().execute('''
            CREATE TABLE IF NOT EXISTS Photos (
                photo_id INTEGER NOT NULL PRIMARY KEY {},
                shop_id INTEGER NOT NULL,
                product_id INTEGER NOT NULL,
                photo_url VARCHAR(255) NOT NULL,
                is_main INTEGER,
                is_successful INTEGER NOT NULL
                {}
            );
            '''.format(auto_increment, sqlite_specific))

        except sqlite3.Error as er:
            logging.error('Ошибка создания таблицы "Photos". Details: {}'.format(er.args[0]))
        except mysql.connector.Error as er:
            logging.error('Ошибка создания таблицы "Photos". Details: {}'.format(er))

        # Issue #61. Необходимо избежать дублирования изображений у товара
        if not self.use_sqlite_syntax:

            try:
                # todo: делать проверку на существование CONSTRAINT-а
                self.connection.cursor().execute('ALTER TABLE Photos '
                                             'ADD CONSTRAINT ucPhotoURL UNIQUE (product_id, photo_url)')
            except mysql.connector.Error as er:
                logging.warning('Не удалось скорректировать constraint для таблицы Photos: {}'.format(er))

        # Для ускорения операций, связанных с добавлением и извлечением данных
        # о фотографиях, используется covering index
        self.create_index('Photos', 'idx_covering_index', 'photo_id, product_id, photo_url, is_main')

        # Создаём две отдельные таблицы со свойствами товаров:
        # Первая - названия свойств, например: "Артикул", "Бренд", "Габариты", и т.д.
        # Вторая - содержит непосредственно значение свойства, связанного с
        # конкретным товаром
        self.connection.cursor().execute('''
        CREATE TABLE IF NOT EXISTS PropNames (
            prop_name_id INTEGER NOT NULL PRIMARY KEY {},
            prop_name NVARCHAR(100)
        );
        '''.format(auto_increment))

        self.create_index('PropNames', 'idx_covering_index', 'prop_name_id, prop_name')

        self.connection.cursor().execute('''
        CREATE TABLE IF NOT EXISTS Properties (
            prop_value_id INTEGER NOT NULL PRIMARY KEY {},
            shop_id INTEGER NOT NULL,
            product_id INTEGER NOT NULL,
            prop_key_id INTEGER NOT NULL,
            variation VARCHAR(60) NOT NULL DEFAULT "DEFAULT",
            prop_value NVARCHAR(510)
        );
        '''.format(auto_increment))

        # Добавляем дополнительный индекс, благодаря которому можно ускорить
        # добавление записи в базу
        self.create_index('Properties', 'idx_property_id', 'shop_id, product_id, prop_key_id, variation', True)
        self.create_index('Properties', 'idx_covering_index', 'product_id, prop_value')

    # Функция позволяет сформировать и выполнить команду добавления индекса к таблице
    # базы данных
    def create_index(self, table_name, index_name, fields, is_unique=False):

        keyword = ''
        if is_unique:
            keyword = 'UNIQUE'

        # Добавляем дополнительный индекс, благодаря которому можно
        # значительно ускорить работу системы
        if self.use_sqlite_syntax:

            sql_command = 'CREATE {} INDEX IF NOT EXISTS {} ON {}({});'.format(keyword, index_name, table_name, fields)
            self.connection.cursor().execute(sql_command)

        else:

            # Проверяем, есть ли в базе данных нужный нам индекс. Если индекс
            # отсутствует, то добавляем его в базу данных
            cur = self.connection.cursor()

            sql_command = "SHOW INDEX FROM {} WHERE KEY_NAME = '{}';".format(table_name, index_name)
            cur.execute(sql_command)
            row = cur.fetchone()
            if row is None:

                sql_command = "ALTER TABLE {} ADD {} INDEX {}({});".format(table_name, keyword, index_name, fields)
                self.connection.cursor().execute(sql_command)

    # Функция сохраняет идентификатор названия магазина, который используется
    # в качестве фильтра при выполнении операций с категориями
    def activate_shop(self, shop_name):

        cur = self.connection.cursor()

        try:
            # Если в базе данных уже есть запись с указанным URL, то
            # делаем эту запись родительской по отношению к добавляемой
            cur.execute('SELECT shop_id FROM Shops WHERE shop_name = "{}"'.format(shop_name))
            row = cur.fetchone()
            if row is not None:
                self.shop_id = row[0]

            else:
                # Добавляем уникальную товарную категорию в базу данных
                sql_command = 'INSERT INTO Shops (shop_name) VALUES ("{}")'.format(shop_name)
                cur.execute(sql_command)
                self.shop_id = cur.lastrowid

        except mysql.connector.Error as er:
            logging.error('Ошибка активации магазина: name = {}. Details: {}'.format(shop_name, er))
            self.shop_id = -1

        except sqlite3.IntegrityError:
            logging.error('Ошибка активации магазина: name = {}'.format(shop_name))
            self.shop_id = -1

    # Функция очищает данные из таблиц базы данных
    def clear_tables(self):

        for table in ['Shops', 'Categories', 'Products', 'PropNames', 'Properties', 'Photos']:

            try:
                sql_command = 'DELETE FROM {}'.format(table)
                cur = self.connection.cursor()
                cur.execute(sql_command)

            # Если была ошибка при очистке одной из таблиц, то всё равно пытаемся очистить другие таблицы
            except mysql.connector.Error as er:
                logging.error('Ошибка очистки таблицы "{}". Details: {}'.format(table, er))
                pass

            except sqlite3.Error as er:
                logging.error('Ошибка очистки таблицы "{}". Details: {}'.format(table, er.args[0]))
                pass

    # Функция возвращает список брендов и связанных с ними торговых марок
    def get_brands_and_trademarks(self):
        return self.unified_models

    # Функция добавляет запись о категории товаров в базу данных
    def insert_category(self, name, url_details, parents):

        cur = self.connection.cursor()
        try:
            # Если в базе данных уже есть запись с указанным URL, то
            # делаем эту запись родительской по отношению к добавляемой
            cur.execute('SELECT cat_id FROM Categories WHERE detailed_info_url = "{}" AND shop_id = {}'.format(
                url_details, self.shop_id))
            row = cur.fetchone()
            if row is not None:
                return row[0]

            is_parent = 0
            if parents == "0":
                is_parent = 1

            # Выполняем минимальную обработку названия набора для того,
            # чтобы исключить сбои при добавлении записи в базу данных
            name = name.replace("'", '&rsquo;')
            name = name.replace('"', '&quot;')

            # Добавляем уникальную товарную категорию в базу данных
            sql_command = ('INSERT INTO Categories '
                           '(shop_id, group_name, detailed_info_url, parents, is_parent) '
                           'VALUES ({}, "{}","{}","{}", {})'
                           ).format(self.shop_id, name, url_details, parents, is_parent)
            cur.execute(sql_command)

            # Возвращаем идентификатор добавленной записи - он используется
            # для формирования определения всех родительских элементов
            # в названии категории товара
            return cur.lastrowid

        except sqlite3.IntegrityError:
            logging.warning('Дублирование записи: shop_id = {}, name = {}, url = {}'.format(
                self.shop_id, name, url_details))
            return -1

        except mysql.connector.Error as er:
            logging.warning('shop_id = {}, name = {}, url = {}. Details: {}'.format(
                self.shop_id, name, url_details, er))
            return -1

    # Функция возвращает идентификатор категории по её названию
    def get_caterory_id_by_name(self, name):

        cur = self.connection.cursor()
        cur.execute('SELECT cat_id FROM Categories WHERE group_name = "{}" AND shop_id = {}'.format(name, self.shop_id))
        row = cur.fetchone()
        if row is None:
            logging.error('Категория с названием "{}" не найдена'.format(name))
            return None

        return row[0]

    # Функция возвращает materialized path (способ хранения древовидной структуры
    # в реляционной модели), включающий как указанную категорию (хранящуюся в rowid),
    # так и её родительские категории
    def get_materialized_path(self, rowid):

        cur = self.connection.cursor()
        cur.execute('SELECT parents FROM Categories WHERE cat_id = {}'.format(rowid))
        row = cur.fetchone()
        if row is None:
            logging.error('Родительский элемент меню с cat_id={} не найден'.format(rowid))
            return "0"

        # Особый случай ("КОРАБЛИК", категория "Подгузники"). В случае, если родительской
        # категорией является корневая директория, то возвращаем только идентификатор
        # родительской подкатегории
        if "0" == row[0]:
            return str(rowid)

        # Возвращаем список родительских категорий, разделённых точкой. Например: "1.34"
        return "{}.{}".format(row[0], rowid)

    # Функция ищет категорию по указанному имени и возвращает её materialized path. Это
    # нужно для того, чтобы сформировать условие фильтрации категорий при извлечении
    # данных. Например, задача может требовать импорта только информации по наборам LEGO
    def get_materialized_path_by_name(self, category_name):

        cur = self.connection.cursor()
        cur.execute('SELECT cat_id FROM Categories WHERE group_name = "{}" AND shop_id = {}'.format(
            category_name, self.shop_id))
        row = cur.fetchone()
        if row is None:
            logging.error('Категория "{}" не найдена. Магазин: {}'.format(category_name, self.shop_id))
            return None

        return self.get_materialized_path(row[0])

    # Функция возвращает URL к категории по условию
    def get_category_url(self, condition):

        cur = self.connection.cursor()
        cur.execute('SELECT detailed_info_url FROM Categories WHERE {}'.format(condition))
        row = cur.fetchone()
        if row is None:
            logging.error('Путь к категории по условию "{}" не найден'.format(condition))
            return None

        return row[0]

    # Функция явно устанавливает факт наличия дочерных категорий и отсутствия
    # необходимости обработки URL-ссылки для извлечения товарных позиций по
    # указанной ссылке. Товары будут извлечены посредством URL-ссылок на дочерние
    # категории
    def detmir_establish_category_paternity(self, cat_id):

        try:
            sql_command = 'UPDATE Categories SET is_parent = 1 WHERE cat_id = {}'.format(cat_id)
            self.connection.cursor().execute(sql_command)

        except sqlite3.Error as er:
            logging.error('База данных: {}'.format(er.args[0]))
            pass

    # Функция вызывает переданную в качестве аргумента функцию (func) для извлечения данных о товарах
    # в каждой категории.
    # Параметр max_count позволяет явно указать, сколько категорий следует обработать - это важно,
    # если обработка нужна только для проверки корректности алгоритмов извлечения данных - в подобном
    # случае, можно указать лишь 1-2 категории, а затем проверить - появились какие-нибудь данные
    # в базе, или нет.
    # Параметр additional_condition позволяет указать условие фильтрации данных, с тем, чтобы,
    # например, обработать только категории товаров "LEGO".
    def process_categories(self, func, max_count, additional_condition=None,
                           ignore_children=False, ignore_parents=False):

        # Если указано нулевое значение, то импортировать данные о товарах не нужно.
        # Смысл нулевого значение - импортировать только детали товарных позиций
        if max_count == 0:
            return

        # Предполагаем, что категории товаров извлечены и эту информацию
        # следует добавить в базу данных
        self.connection.commit()

        # Формируем запрос с учётом дополнительных условий и максимального количества
        # импортируемых данных. Явным образом указываем магазин, данные из которого
        # извлекаются
        sql_command = 'SELECT cat_id, detailed_info_url, group_name, parents, is_parent FROM Categories ' \
                      'WHERE shop_id = {} '.format(self.shop_id)

        if additional_condition is None or len(additional_condition) == 0:

            # Явным образом исключаем корневые категории (категории высшего уровня)
            sql_command += 'AND is_parent = 0 '

        else:

            # Включаем дополнительные условия для отбора данных, например: импортировать
            # только LEGO-наборы
            if len(additional_condition) > 0:
                sql_command += 'AND ({}) '.format(additional_condition)

        # Сортируем по дате - раньше должны обрабатываться категории, которые
        # обрабатывались раньше всего
        if self.use_sqlite_syntax:
            sql_command += 'ORDER BY datetime(date_of_scraping) ASC '
        else:
            sql_command += 'ORDER BY date_of_scraping ASC '

        # Если количество запрашиваемых категорий меньше тысячи, то добавляем
        # в SQL-команду ключевое слово, ограничивающую количество категорий
        if max_count < 1000:
            sql_command += 'LIMIT {}'.format(max_count)

        # Выполняем сформированный SQL-запрос
        cur = self.connection.cursor()
        cur.execute(sql_command)

        # Сохраняем полученные данные в виде списка кортежей
        categories = []
        for category in cur.fetchall():
            categories.append(category)

        # Сначала удаляем из списка все корневые категории, которые не являются
        # витринами сами по себе, а содержат только список подкатегорий. Затем
        # удаляем из списка все дочерние категории, если в списке есть их
        # родительская подкатегория
        if ignore_children:

            just_children = [s for s in categories if s[4] == 0]
            categories = [s for s in just_children if not self._parent_category_has_found(just_children, s)]

        if ignore_parents:

            just_children = [s for s in categories if s[4] == 0]
            parents = [s for s in just_children if not self._parent_category_has_found(just_children, s)]
            categories = [s for s in just_children if not self._is_category_in(parents, s)]

        # С целью упрощения кода классов извлечения данных из online-магазинов,
        # вызывается функция переданная в качестве callback-аргумента. Этой функции
        # передаются ей два параметра - идентификатор записи в таблице Categories и URL,
        # по которому находятся товарные позиции указанной категории. При разработке
        # нового кода, данный подход не создаёт каких-либо существенных преимуществ,
        # но он упрощает сопровождение уже существующего кода, т.к. код извлечения данных
        # оперирует не с анонимными кортежами (tuples), а с вполне осмысленными
        # наименованиями полей
        suspended_updates = []

        for category in categories:

            # Сохраняем URL всех товаров, находившихся в товарной категории
            # в отдельный список - по мере импорта данных с сайта магазина,
            # данный код будет исключать из списка те товары, которые находятся
            # на сайте в данный момент - то, что останется в списке - устаревшие
            # данные
            self._product_urls_to_list(category[0])

            # Вызываем функцию импорта дочерней категории
            func(category[0], category[1])

            # Корректируем дату последнего импорта данных обработанной категории.
            if self.use_sqlite_syntax:
                sql_command = ('UPDATE Categories SET date_of_scraping=datetime("now") '
                               'WHERE cat_id = {}').format(category[0])
            else:
                sql_command = ('UPDATE Categories SET date_of_scraping=NOW() '
                               'WHERE cat_id = {}').format(category[0])
            suspended_updates.append(sql_command)

            # Помещаем товары, URL-которых не был обнаружен при обработке
            # витрины товаров конкретной категории в группу "потенциально
            # устаревшие".
            # Важный нюанс - механизм удаления применяется только к подмножеству
            # данных, ограниченных условием additional_condition.
            for outdated_url in self.current_category_list:
                sql_command = self.mark_as_outdated(outdated_url, execute_now=False)
                suspended_updates.append(sql_command)

        # Выполняем отложенные SQL-запросы
        for sql_command in suspended_updates:
            cur.execute(sql_command)

    # Функция позволяет определить, если ли указанная категория в списке, или нет
    @staticmethod
    def _is_category_in(categories, one):

        for category in categories:
            if category[0] == one[0]:
                return True

        return False

    # Функция позволяет исключить дочернюю категорию, если найдена родительская
    # категория, в которую она входит. Соответственно, функция возвращает True,
    # если у указанной подкатегории есть родительская категория в списке
    @staticmethod
    def _parent_category_has_found(categories, one):

        # Список категорий заполняется следующим SQL-запросом:
        # SELECT cat_id, detailed_info_url, group_name, parents, is_parent'
        # Соответственно, установить, что у указанной категории
        # есть предок в списке категорий очень просто - разбираем
        # поле parents на составляющие его идентификаторы
        # родительских категорий и сравниваем по всем cat_id
        # в списке
        if one[3] is None:
            return False

        all_parents = []
        for value in one[3].split('.'):
            try:
                all_parents.append(int(value))
            except ValueError:
                pass

        for category in categories:
            if category[0] in all_parents:
                return True

        return False

    # Функция сохраняет в отдельном списке URL всех товаров определённой категории.
    # Это позволяет находить товары, на которые отсутствуют ссылки на витрине.
    # Нас не интересуют записи о товарах, которые уже были помечены, как устаревшие
    def _product_urls_to_list(self, category_id):

        sql_command = 'SELECT url_details FROM Products WHERE category_id = {} AND outdated <> 1'.format(category_id)

        products_cur = self.connection.cursor()
        products_cur.execute(sql_command)

        for link in products_cur.fetchall():
            self.current_category_list.append(link[0])

    # Функция помечает товар в базе данных, как "потенциально устаревший".
    # Команда может быть выполнена отложено, с целью повышения производительности
    # работы scraper-ов
    def mark_as_outdated(self, outdated_url, execute_now=True):

        sql_command = ('UPDATE Products SET outdated=1 '
                       'WHERE url_details = "{}"').format(outdated_url)

        if execute_now:
            outdated_cur = self.connection.cursor()
            outdated_cur.execute(sql_command)

        return sql_command

    # Функция отбирает все записи о продуктах в товарных группах, указанных
    # в условии sql_condition и вызывает внешнюю функцию для того, чтобы
    # обработать эти результаты.
    #
    # Issue #122. С целью минимизации нагрузки на базу данных, мы можем осуществлять
    # кэширование данных из таблицы описания товара (Products) в память, а затем
    # использовать этот кэш при выполнении функции get_product_description()
    def for_each_product(self, sql_condition, func, include_outdated=False, cache_product_fields=False):

        # Формируем SQL-запрос отбора товаров конкретного магазина
        sql_command = 'SELECT Categories.cat_id, Categories.group_name, product_id, product_name'

        if cache_product_fields:
            sql_command += ', price, price_old, url_details, description, instock, outdated '
            self.cached_product_description = {}
        else:
            self.cached_product_description = None

        sql_command += ' FROM Products ' \
                       'INNER JOIN Categories ON Products.category_id = Categories.cat_id ' \
                       'WHERE Products.shop_id = {}'.format(self.shop_id)

        if not include_outdated:
            sql_command += " AND outdated=0 "

        if sql_condition is not None and len(sql_condition) > 0:
            sql_command += " AND ("
            sql_command += sql_condition
            sql_command += ") "

        cur = self.connection.cursor()
        cur.execute(sql_command)

        for row in cur.fetchall():
            # При обработке товара, заменяем escaped-символы на их обычные аналоги
            product_name = html.unescape(row[3])

            # Сохраняем в cache информацию о товаре. Обязательно сохраняем product_id,
            # так как он позволяет нам проверять корректность данных в кэше
            if cache_product_fields:
                self.cached_product_description['product_name'] = product_name
                self.cached_product_description['product_id'] = row[2]
                self.cached_product_description['price'] = row[4]
                self.cached_product_description['price_old'] = row[5]
                self.cached_product_description['url_details'] = row[6]
                self.cached_product_description['description'] = row[7]
                self.cached_product_description['instock'] = row[8]
                self.cached_product_description['outdated'] = row[9]

            func(row[0], row[1], row[2], product_name)
        pass

    # Функция подготовки к пакетированию данных для вставки сразу нескольких
    # записей в базу данных. Актуально только для MySQL
    def begin_products_insertion(self):
        self.insertion_products = ""

    # После того, как сформирован пакет данных для вставки в базу данных, передаём
    # всю команду MySQL
    def end_products_insertion(self, update_product_name=True, ignore_old_price=False):

        if not self.use_sqlite_syntax:

            # Если не было найдено никаких товаров, то не выполняем SQL-запрос
            if self.insertion_products is not None and len(self.insertion_products) > 0:

                # В случае MySQL используем конструкцию INSERT INTO ... ON DUPLICATE KEY UPDATE,
                # которая по смыслу близка к MERGE в Microsoft SQL Server. Это даёт выигрыш
                # в скорости на порядок
                # Используем конструкцию INSERT INTO ... ON DUPLICATE KEY UPDATE (MySQL)
                sql_command = 'INSERT INTO Products (shop_id, category_id, ' \
                              'product_name, price, price_old, url_details, outdated) VALUES '
                sql_command += self.insertion_products
                sql_command += '\nON DUPLICATE KEY UPDATE ' \
                               'category_id=VALUES(category_id), price=VALUES(price), date_of_scraping=NOW(), '

                # Issue #152 (Rebate-price)
                if not ignore_old_price:
                    sql_command += 'price_old=VALUES(price_old), '

                if update_product_name:
                    sql_command += 'product_name=VALUES(product_name), '

                sql_command += 'outdated=0;'

                try:
                    self.connection.cursor().execute(sql_command)

                except mysql.connector.Error as er:
                    logging.error('База данных: ', er)
                    logging.error('SQL-команда (выполнена с ошибкой): ', sql_command)

        # После извлечения группы товаров, добавляем из в базу данных явным образом
        self.connection.commit()

    # Функция позволяет добавить, или обновить товарную позицию в базе данных.
    # Не очевидные аргументы:
    # force_insert - не использовать пакетирование, сразу добавить запись в базу (для UNIT-теcтов)
    # update_product_name - обновлять поле update_product_name. В ряде случаев, например, в
    #   магазине "Кораблик", название товара извлекаемого с витрины может быть искажено.
    #   В подобных случаях имеет смысл использовать product_name извлекаемый со страниц
    #   содержащих детальную информацию о товаре
    def insert_product(self, category_id, name, price, url_details, price_old=None, force_insert=False,
                       update_product_name=True):
        """
        :param int category_id: идентификатор подкатегории товара
        :param str name: название товара, извлечённое с витрины 
        :param int price: цена товара с учётом применённой скидки 
        :param str url_details: адрес страницы с детальной информацией (URL) 
        :param int price_old: цена товара до применения скидки. Может быть IGNORE_OLD_PRICE
        :param bool force_insert: True - сразу писать данные базу, не пакетировать их (для тестов) 
        :param bool update_product_name: True - обновлять имя товара. False - имя нужно брать со страницы деталей
        :return: int
        """

        # Если базовая цена на товар не указана, то считаем, что товар продаётся без скидки
        if price_old is None:
            price_old = price

        # Удаяем из списка товаров, полученных во время предыдущего извлечения
        # данных, те, которые есть и на актуальной витрине
        if url_details in self.current_category_list:
            self.current_category_list.remove(url_details)

        # Исключаем из названия товара кавычки, поскольку они нарушают
        # корректность SQL-запроса
        name = name.replace("'", '&rsquo;')
        name = name.replace('"', '&quot;')

        if self.use_sqlite_syntax:

            try:
                # В SQLite мы не можем применять операцию REPLACE для таблицы Products,
                # поскольку при существовании записи в базе данных, REPLACE удаляет
                # старую запись и создаёт новую с новым product_id. Это приводит к тому,
                # что внешние ссылки нарушаются и подсистема хранения перестаёт работать

                cur = self.connection.cursor()
                cur.execute('SELECT product_id FROM Products WHERE url_details = "{}" AND shop_id = {}'.format(
                    url_details, self.shop_id))
                row = cur.fetchone()
                if row is None:

                    # Если запись новая, то добавить её в базу данных
                    sql_command = ('INSERT INTO Products '
                                   '(shop_id, category_id, product_name, price, price_old, url_details, outdated) '
                                   'VALUES ({}, {},"{}",{},{},"{}",0)'
                                   ).format(self.shop_id, category_id, name, price, price_old, url_details)
                    cur.execute(sql_command)
                    return cur.lastrowid

                else:

                    # Issue #152 (Rebate-price). Если параметр price_old установлен
                    # в специальное значение, то не обновляем поле "цена без скидки".
                    # Это делается для того, чтобы при обновлении витрины не затереть
                    # корректные значения, которые извлекаются при обработке страницы
                    # содержащей детальную информацию
                    update_old_price = ''
                    if self.IGNORE_OLD_PRICE != price_old:
                        update_old_price = 'price_old={}, '.format(price_old)

                    # Если запись с подобным URL уже была в базе данных, то обновляем поля записи
                    if update_product_name:

                        sql_command = ('UPDATE Products '
                                       'SET category_id={}, product_name="{}", price={}, {}outdated=0, '
                                       'date_of_scraping=datetime("now") '
                                       'WHERE url_details = "{}"'
                                       ).format(category_id, name, price, update_old_price, url_details)
                    else:

                        # Issue #127. Не обновляем поле 'product_name' (учитываем особенность "Кораблика")
                        sql_command = ('UPDATE Products '
                                       'SET category_id={}, price={}, {}outdated=0, date_of_scraping=datetime("now") '
                                       'WHERE url_details = "{}"'
                                       ).format(category_id, price, update_old_price, url_details)

                    cur.execute(sql_command)

                    # При выполнении операции обновления записи (Update) lastrowid
                    # установлен в None и его нельзя использовать. Вместо lastrowid
                    # используем ранее полученный идентификатор записи в таблице Products
                    return row[0]

            except sqlite3.Error as er:
                logging.error('База данных: ', er.args[0])
                return None

        else:

            if len(self.insertion_products) > 0:
                self.insertion_products += ',\n'

            self.insertion_products += '({},{},"{}",{},{},"{}",0)'.format(self.shop_id, category_id, name,
                                                                          price, price_old, url_details)
            # В ряде случаев, пакетное сохранение данных в базу является
            # не допустимым. Это случается тогда, когда product_id нужен
            # сразу же после вставки товара в базу, например, в Unit-тестах
            # и в scraper-е магазина "Купи-Кубик"
            if force_insert:

                # В случае MySQL используем конструкцию INSERT INTO ... ON DUPLICATE KEY UPDATE,
                # которая по смыслу близка к MERGE в Microsoft SQL Server. Это даёт выигрыш
                # в скорости на порядок
                # Используем конструкцию INSERT INTO ... ON DUPLICATE KEY UPDATE (MySQL)
                sql_command = 'INSERT INTO Products (shop_id, category_id, ' \
                              'product_name, price, price_old, url_details, outdated) VALUES '
                sql_command += self.insertion_products
                self.insertion_products = ''

                sql_command += '\nON DUPLICATE KEY UPDATE ' \
                               'category_id=VALUES(category_id), price=VALUES(price), date_of_scraping=NOW(), '

                # Issue #152 (Rebate-price)
                if self.IGNORE_OLD_PRICE != price_old:
                    sql_command += 'price_old=VALUES(price_old), '

                if update_product_name:
                    sql_command += 'product_name=VALUES(product_name), '

                sql_command += 'outdated=0;'
                try:

                    cur = self.connection.cursor()
                    cur.execute(sql_command)
                    return cur.lastrowid

                except mysql.connector.Error as er:
                    logging.error('База данных: ', er)
                    logging.error('SQL-команда (выполнена с ошибкой): ', sql_command)
        return None

    # Функция позволяет обновить поле "товар в наличии" у указанного товара.
    def update_instock_status(self, product_id, instock):
        """
        :param int product_id: идентификатор товара в базе данных
        :param int instock: флаг наличия товара на складе/магазине
        """
        try:
            sql_command = 'UPDATE Products SET instock={} WHERE product_id = {}'.format(instock, product_id)
            cur = self.connection.cursor()
            cur.execute(sql_command)

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

    # Функция позволяет обновить поле "описание товара". Потребность в этой
    # операции чаще всего возникает при извлечении детальной информации о товаре
    def update_product_description(self, product_id, description):
        """
        :param int product_id: идентификатор товара в базе данных
        :param str description: детальное описание товара
        """

        description = description.replace("'", '&rsquo;')
        description = description.replace('"', '&quot;')

        try:
            sql_command = 'UPDATE Products SET description="{}" WHERE product_id = {}'.format(description,
                                                                                              product_id)
            cur = self.connection.cursor()
            cur.execute(sql_command)

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

    def preload_data(self, load_photos=True):

        msg_to_indicate_load_photos = ''
        if load_photos:
            msg_to_indicate_load_photos = 'и фотографий '

        print("Загрузка свойств {}в кэш, для shop_id = {}...".format(msg_to_indicate_load_photos, self.shop_id))
        start_time = time.time()

        # Помещаем все фотографии определённых товаров в один словарь, в котором
        # ключем является идентификатор товара, а значением - его список фотографий
        self.in_memory_photos = {}
        self.in_memory_properties = {}

        try:

            # В некоторых ситуациях, например, когда данные используются для анализа
            # информация о фотографиях не нужна. Соответственно, не тратим на это
            # процессорное время
            if load_photos:

                # Загружаем информацию о фотографиях товаров
                cur = self.connection.cursor()
                cur.execute('SELECT product_id, photo_url FROM Photos WHERE shop_id = {}'.format(self.shop_id))

                for row in cur.fetchall():

                    product_id = row[0]
                    photo_url = row[1]

                    if self.in_memory_photos.get(product_id) is None:
                        self.in_memory_photos[product_id] = []

                    self.in_memory_photos[product_id].append(photo_url)

            # Получаем из базы данных коллекцию свойств конкретного товара.
            # Issue #149. Не выгрузаем свойства отдельных вариаций
            cur = self.connection.cursor()
            sql_command = "SELECT Properties.product_id, PropNames.prop_name, Properties.prop_value " \
                          "FROM Properties " \
                          "INNER JOIN PropNames ON Properties.prop_key_id = PropNames.prop_name_id " \
                          "WHERE shop_id = {} AND Properties.variation = 'DEFAULT'".format(self.shop_id)
            cur.execute(sql_command)

            for row in cur.fetchall():

                product_id = row[0]
                prop_name = row[1]
                prop_value = row[2]

                if self.in_memory_properties.get(product_id) is None:
                    self.in_memory_properties[product_id] = {}

                self.in_memory_properties[product_id][prop_name] = prop_value

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

        print("Загрузка свойств и изображений заняло: {}".format(
            int(round((time.time() - start_time) * 1000))))

    # Функция возвращает свойства конкретной вариации товара
    def get_properties_for_variation(self, product_id, variation):

        results = None

        try:

            cur = self.connection.cursor()
            sql_command = "SELECT PropNames.prop_name, Properties.prop_value FROM Properties " \
                          "INNER JOIN PropNames ON Properties.prop_key_id = PropNames.prop_name_id " \
                          "WHERE product_id = {} AND Properties.variation = '{}'".format(product_id, variation)
            cur.execute(sql_command)

            for row in cur.fetchall():

                prop_name = row[0]
                prop_value = row[1]

                if results is None:
                    results = {}

                results[prop_name] = prop_value

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

        return results

    # Функция сбрасывает предварительно загруженные данные о товарах
    def clear_preloaded_data(self):

        self.in_memory_photos = {}
        self.in_memory_properties = {}

    # Функция заполняет объект product данными о товаре product_id.
    # Параметр shop должен содержать название магазина, удобное для
    # восприятие человеком, т.е. "Детский мир", а не "Detmir"
    def get_product_description(self, product_id, shop, product):

        try:

            # Если данные о товаре есть в кэше, то не запрашиваем их
            # из базы данных
            have_in_cache = False
            if self.cached_product_description is not None:
                if self.cached_product_description['product_id'] == product_id:

                    product_name = self.cached_product_description['product_name']
                    price = self.cached_product_description['price']
                    price_old = self.cached_product_description['price_old']
                    url_details = self.cached_product_description['url_details']
                    description = self.cached_product_description['description']
                    instock = self.cached_product_description['instock']
                    outdated = self.cached_product_description['outdated']
                    have_in_cache = True

                else:
                    logging.error('Ошибка работы с кэшем. Запрошен product_id = {}, '
                                  'а в cache находится {}'.format(product_id,
                                                                  self.cached_product_description['product_id']))
            # Включаем в описание товара ценовое предложение
            if not have_in_cache:

                cur = self.connection.cursor()
                cur.execute(
                    'SELECT price, price_old, product_name, url_details, description, instock, outdated '
                    'FROM Products WHERE product_id = {}'.format(product_id))
                product_row = cur.fetchone()
                if product_row is None:
                    return None

                product_name = html.unescape(product_row[2])
                price = product_row[0]
                price_old = product_row[1]
                url_details = product_row[3]
                description = product_row[4]
                instock = product_row[5]
                outdated = product_row[6]       # Поле добавлено по Issue #64

            # Сохраняем в объекте название товара, попутно декодируя
            # escape-последовательности
            product.set_product_name(product_name)
            product.append_excessive_name(product_name)

            # Если данные о изображениях товара находятся в словаре пред-обработанных
            # данных, то добавляем эти изображения в описание товара
            if self.in_memory_photos is None or self.in_memory_photos.get(product_id) is None:

                photos = []

            else:

                photos = self.in_memory_photos[product_id]

            # Раньше использовался следующий код (оставил для сравнения производительности):
            '''cur = self.connection.cursor()
            cur.execute('SELECT photo_url FROM Photos WHERE product_id = {}'.format(product_id))
            photos = [row[0] for row in cur.fetchall()]'''

            # Получаем из базы данных коллекцию свойств конкретного товара
            properties = {}

            if self.in_memory_properties is not None and self.in_memory_properties.get(product_id) is not None:

                properties = self.in_memory_properties[product_id]

                # Issue #141. В списке свойств встречаются как вариации, по которым
                # осуществляется группировка товаров, так и одноименные свойства,
                # добытые со страницы детальной информации, а не из названия товара.
                # Если определена вариации, то обычное свойство нужно из списка
                # исключить
                self._remove_usual_props_if_variation_exist(properties)

                for prop_name in list(properties.keys()):

                    # Свойства приходится добавлять не только в ценовое предложение, но
                    # и в описание товара, поскольку среди свойств есть критичные для
                    # дальнейшего анализа данных, но уникальные для конкретного магазина,
                    # например: 'Бренд' и 'Brand'.
                    prop_value = html.unescape(properties[prop_name])
                    product.append_property(prop_name, prop_value)

            # Раньше использовался следующий код (оставил для сравнения производительности):
            '''cur = self.connection.cursor()
            sql_command = 'SELECT PropNames.prop_name, Properties.prop_value FROM Properties ' \
                          'INNER JOIN PropNames ON Properties.prop_key_id = PropNames.prop_name_id ' \
                          'WHERE product_id = {} '.format(product_id)
            cur.execute(sql_command)

            for row in cur.fetchall():
                properties[row[0]] = row[1]
                product.append_property(row[0], row[1])'''

            # Добавляем собранную информацию об одном товарном предложении
            # в объект описания продукта
            product.append_offer(shop, price, price_old, url_details, photos, properties, description,
                                 instock, outdated)

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

    # Устраняем из списка свойств те, для которых в этом же списке
    # определена одноименная вариация. Вариация, называется также,
    # как и свойство, но содержит underscore в конце
    @staticmethod
    def _remove_usual_props_if_variation_exist(properties):

        variations = []

        # Выделяем из списка свойств имена найденных вариаций
        all_keys = list(properties.keys())
        for prop_name in all_keys:
            if prop_name[-1:] == '_':
                variations.append(prop_name[:-1].upper())

        # Удаляем из списка обычные свойства, названия которых содержат
        # вариации
        for prop_name in all_keys:
            if prop_name.upper() in variations:
                del properties[prop_name]

    # Функция осуществляет вызов callback-функции, осуществляющей обработку
    # отдельной странице, целиком посвящённой товару, с целью извлечения
    # свойств товара и его фотографий.
    # Параметры: max_count - максимальное количество страниц, обрабатываемых за
    # одну сессию (фактически - self.so.scrape_details_by_count).
    # commit_periodically - периодически сохранять результаты обработки данных,
    # выполняя операцию commit в базе данных
    def process_each_product(self, func, max_count, additional_condition=None, commit_periodically=True):

        # КЛЮЧЕВОЕ ТРЕБОВАНИЕ К АЛГОРИТМУ:
        # При обработке данных о товарах, в первую очередь следует обрабатывать
        # товары, которые ещё не отработывались. Следующий приоритет - товары,
        # которые ранее уже были обработаны.
        # Признаком ранее обработанных страниц является наличие на ссылок на запись
        # в таблице Products из таблицы Photos (поле product_id).
        # Выполнением этого требования является сортировка ORDER BY с использованием
        # процедуры IFNULL(). У этой процедуры два параметра: имя поля по которому
        # осуществляется сортировка и значение, которое нужно подставить, если
        # поле будет содержать значение NULL. В нашем случае подставляем значение ' '.

        # Технический нюанс: используется LEFT OUTER JOIN, который сформирует результат
        # базируясь на записях в таблицы Products. Если для записи в таблице Products
        # не будет связанной записи в таблице Photos, то поле Photos.product_id будет
        # нулевым.
        # Важный технический нюанс связан с ситуацией, когда данные в таблицах Products
        # и Photos будут иметь отношение один ко многим (у одного товара может быть
        # несколько изображений). В этом случае, использование LEFT JOIN может привести
        # к появлению дубликатов. Чтобы избежать этого, я использую ключевое слово
        # DISTINCT сразу же за командой SELECT

        if self.update_details:

            # ВНИМАНИЕ! В версиях MySQL начиная с 5.7.5 появилась новая "default"-ная
            # настройка ONLY_FULL_GROUP_BY, которая блокирует возможность использования
            # DISTINCT и ORDER BY одновременно. Идущий ниже запрос приводит к сбросу
            # исключения: "DatabaseError: 3065 (HY000): Expression #1 of ORDER BY
            # clause is not in SELECT list..."
            # Решение проблемы - конфигурирования сервера баз данных, см:
            #   https://github.com/Piwigo/Piwigo/issues/376
            #
            # Получить глобальные установки SQL-сервер можно вот так:
            #   SELECT @@global.sql_mode;
            #
            # Если в полученном списке есть ONLY_FULL_GROUP_BY, то проблема именно
            # в этом. Решение, сконфигурировать файл "/etc/my.cnf" вот так:
            #   [mysqld]
            #   sql-mode=""
            #
            # todo: добавить проверку флага при запуске скриптов scraper-а

            sql_command = 'SELECT DISTINCT Products.product_id, Products.url_details FROM Products ' \
                          'LEFT JOIN Photos ON Products.product_id = Photos.product_id ' \
                          'INNER JOIN Categories ON Products.category_id = Categories.cat_id ' \
                          'WHERE Products.outdated = 0 AND Products.shop_id = {} '.format(self.shop_id)

            # Извлечение детальной информации может осуществляться с использованием
            # фильтров товаров (т.е. можно указать DIAPERS, LEGO, или PRAM)
            if additional_condition is not None and len(additional_condition) > 0:
                sql_command += ' AND ('
                sql_command += additional_condition
                sql_command += ')'

            sql_command += ' ORDER BY IFNULL(Photos.product_id,\' \') DESC'

        else:
            # Выбираем из таблицы Products только те записи, на которые нет ссылок в таблице
            # Photos, а поле product_id. Этот признак позволяет разделить описания товаров
            # по которым уже была извлечена детальная информация и те, которых были
            # получены с витрины. Те, которые были извлечены с витрины, нуждаются в
            # в дальнейшей обработке, в частности, в извлечении свойств товаров
            sql_command = 'SELECT DISTINCT Products.product_id, Products.url_details FROM Products ' \
                          'LEFT JOIN Photos ON Products.product_id = Photos.product_id ' \
                          'INNER JOIN Categories ON Products.category_id = Categories.cat_id ' \
                          'WHERE Products.outdated = 0 AND Photos.product_id IS NULL ' \
                          'AND Products.shop_id = {}'.format(self.shop_id)

            if additional_condition is not None and len(additional_condition) > 0:
                sql_command += " AND ("
                sql_command += additional_condition
                sql_command += ')'

        cur = self.connection.cursor()
        cur.execute(sql_command)

        # Извлекаем все необходимые нам URL-товаров в отдельный массив с тем, чтобы
        # не допустить одновременного выполняется СУБД нескольких запросов. Для
        # MySQL это может давать ускорение в 1,5-2 раза
        urls_list = []
        for input_data in cur.fetchall():
            urls_list.append((input_data[0], input_data[1]))

        # Обрабатываем все накопленные URL в одном цикле
        count = commit_count = 0
        for url in urls_list:

            # Прекращаем обработку, если достигли установленного пользователем
            # количества обработанных страниц
            if count >= max_count:
                break

            count += 1

            # Через каждые 20 страниц сохраняем извлечённые данные на жёстком диске
            if commit_periodically:

                commit_count += 1
                if commit_count >= 20:

                    if commit_periodically:
                        # Выводим информацию о количестве обработанных страниц с тем, чтобы
                        # оператор видел, что приложение на зависло
                        print("Обработано {} страниц. Время: {}".format(count, strftime("%H:%M:%S", localtime())))

                    commit_count = 0
                    self.connection.commit()

            # Вызываем callback-функцию, осуществляющую загрузку HTML-страницы
            # и извлечение данных с использованием DOM
            func(url[0], url[1])

        # Сохраняем извлечённую информацию по изображениям и свойствам товаров
        self.connection.commit()

    # Функция подготовки к пакетированию данных для вставки сразу нескольких
    # записей в базу данных. Актуально только для MySQL
    def begin_props_insertion(self):
        self.insertion_data = ""
        self.insertion_photos = ""
        self.pack_of_sql_commands = ""

    # Функция добавляет новое свойство в базу данных. В случае SQLite, это делается
    # выполнением команды INSERT, а в случае MySQL применяется пакетирование записей
    # и используется конструкция INSERT INTO ... ON DUPLICATE KEY UPDATE
    def include_prop(self, product_id, property_name, value, variation='DEFAULT'):
        """
        :param product_id: идентификатор товара
        :type product_id: int
        :param property_name: название свойства
        :type property_name: str
        :param value: значение свойства
        :type value: str
        :param variation: вариация товара, к которой относится данное свойств 
        :type variation: str
        """

        value = value.replace("'", '&rsquo;')
        value = value.replace('"', '&quot;')

        if self.use_sqlite_syntax:
            self.insert_property(product_id, property_name, value, variation)
        else:

            property_name_id = self._insert_property_name(property_name)

            if len(self.insertion_data) > 0:
                self.insertion_data += ',\n'
            self.insertion_data += '({},{},{},"{}","{}")'.format(self.shop_id, product_id,
                                                                 property_name_id, value, variation)

    # Функция добавляет значение свойств-вариаций из указанной коллекции
    # (collected_props). Определение того, является ли свойство вариацией
    # принимается на основании списка valuable_prop_names. Функция
    # возвращает строку, которая является расширением имени модели и
    # содержит важные свойства, не являющиеся вариациями. Issue #119
    def insert_prop_variations(self, product_id, collected_props, valuable_prop_names):
        """
        :param product_id: идентификатор товара в таблице "Products"
        :type product_id: int
        :param collected_props: свойства, выделенные при обработке названия товара
        :type collected_props: dict
        :param valuable_prop_names: список вариации, которые могут быть применены к обрабатываемой категории
        :type valuable_prop_names: list
        """

        model_extension = ''

        for pair in collected_props:

            if pair[0].upper() in valuable_prop_names:

                # Issue #118. В конце названия вариации (не обычного свойства)
                # добавляем специальный символ - 'underscore'
                self.include_prop(product_id, pair[0] + '_', pair[1])

            else:

                if len(model_extension) > 0:
                    model_extension += ', '

                model_extension += pair[0] + ': ' + pair[1]

        return model_extension

    # Вспомогательная функция, которая позволяет выполнить SQL-запрос,
    # или добавить его в пакет запросов, если это является возможным
    def _execute_or_pack_sql(self, sql_command):
        """
        :param sql_command: SQL-команда INSERT, или UPDATE, которая может быть выполнена в пакетном задании
        :type sql_command: str
        """

        if self.use_sqlite_syntax:

            try:
                self.connection.cursor().execute(sql_command)

            except sqlite3.Error as er:
                logging.error('База данных: {}'.format(er.args[0]))

        else:

            self.pack_of_sql_commands += sql_command
            self.pack_of_sql_commands += ';\n'

    # Функция добавляет в пакет SQL-команд для записи в базу, команду
    # обновления названия товара.
    # Преимущественно используется в "Кораблике", см. #127
    def update_product_name_pack(self, product_id, product_name):
        """
        :param product_id: идентификатор товара
        :type product_id: int
        :param product_name: строка с названием товара, включая вариации
        :type product_name: str
        """

        product_name = product_name.replace("'", '&rsquo;')
        product_name = product_name.replace('"', '&quot;')

        sql_command = 'UPDATE Products SET product_name="{}" WHERE product_id={}'.format(product_name, product_id)
        self._execute_or_pack_sql(sql_command)

    # Issue #152 (rebate_price). Помещаем в пакет SQL-команд данные
    # о "старой" цене товара, т.е. о цене до применения скидок
    def update_product_old_price(self, product_id, old_price):
        """
        :param product_id: идентификатор товара
        :type product_id: int
        :param old_price: цена товара до применения скидок
        :type old_price: int
        """

        sql_command = 'UPDATE Products SET price_old={} WHERE product_id={}'.format(old_price, product_id)
        self._execute_or_pack_sql(sql_command)

    # После того, как сформирован пакет данных для вставки в базу данных, передаём
    # всю команду MySQL
    def end_props_insertion(self):

        if not self.use_sqlite_syntax:

            # Если у товара не найдено никаких свойств, то не выполняем SQL-запрос
            if self.insertion_data is not None and len(self.insertion_data) > 0:

                # В случае MySQL используем конструкцию INSERT INTO ... ON DUPLICATE KEY UPDATE,
                # которая по смыслу близка к MERGE в Microsoft SQL Server. Это даёт выигрыш
                # в скорости на порядок
                sql_command = 'INSERT INTO Properties (shop_id, product_id, prop_key_id, prop_value, variation) '
                sql_command += 'VALUES\n'
                sql_command += self.insertion_data
                sql_command += '\nON DUPLICATE KEY UPDATE prop_value=VALUES(prop_value);\n'

                # Сохраняем извлечённые свойства товара
                try:
                    self.connection.cursor().execute(sql_command)

                except mysql.connector.Error as er:
                    logging.error('База данных: ', er)
                    logging.error('SQL-команда (выполнена с ошибкой): ', sql_command)

            # Issue #64. Если фотографии товара не доступны, то не добавляем в базу
            # данных чего-либо
            if self.insertion_photos is not None and len(self.insertion_photos) > 0:

                # В случае совпадения уникального ключа (product_id + photo_url), изменяем
                # значение полей is_main и is_successful
                sql_command = 'INSERT INTO Photos (shop_id, product_id, photo_url, is_successful, is_main) VALUES '
                sql_command += self.insertion_photos
                sql_command += '\nON DUPLICATE KEY UPDATE ' \
                               'is_successful=VALUES(is_successful), is_main=VALUES(is_main);\n'

                # Сохраняем извлечённые фотографии товара
                try:
                    self.connection.cursor().execute(sql_command)

                except mysql.connector.Error as er:
                    logging.error('База данных: ', er)
                    logging.error('SQL-команда (выполнена с ошибкой): ', sql_command)

            # Issue #127. Отдельно выполняем отложенные SQL-команды
            if self.pack_of_sql_commands is not None and len(self.pack_of_sql_commands) > 0:

                try:
                    self.connection.cursor().execute(self.pack_of_sql_commands)
                    self.pack_of_sql_commands = None

                except mysql.connector.Error as er:
                    logging.error('База данных: ', er)
                    logging.error('SQL-команда (выполнена с ошибкой): ', self.pack_of_sql_commands)

    # Функция позволяет сохранить в базе данных имя свойства товара.
    # Используется преимущественно при проведении Unit-тестирования
    def insert_property(self, product_id, property_name, value, variation='DEFAULT'):

        try:
            property_name_id = self._insert_property_name(property_name)
            self._insert_property_value(product_id, property_name_id, value, variation)

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

    # Public-Функция возвращает указанное свойство товара
    def get_property(self, product_id, property_name):

        try:
            sql_command = 'SELECT prop_value FROM Properties ' \
                          'INNER JOIN PropNames ON Properties.prop_key_id = PropNames.prop_name_id ' \
                          'WHERE product_id = {} AND PropNames.prop_name = "{}"'.format(product_id, property_name)
            cur = self.connection.cursor()
            cur.execute(sql_command)
            row = cur.fetchone()
            if row is not None:
                return row[0]

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

        return None

    # Функция возвращает значение свойства закэшированного в памяти
    def get_cached_property(self, product_id, property_name):

        if self.in_memory_properties is None or self.in_memory_properties.get(product_id) is None:
            return None

        if self.in_memory_properties[product_id].get(property_name) is None:
            return None

        return self.in_memory_properties[product_id][property_name]

    # Private-функция: добавить имя свойства в базу данных
    def _insert_property_name(self, property_name):

        # Если идентификатор имени свойства есть в кэше,
        # то возвращаем идентификатор имени свойства
        if property_name in self.cached_prop_names:
            return self.cached_prop_names[property_name]

        # Issue #159. Поиск ошибки в работе scraper-а
        if len(property_name) >= 100:
            logging.error('Очень длиное имя свойства: "{}"', property_name)

        try:
            # Если имя свойства уже зарегистрировано в базе данных, то
            # возвращаем её идентификатор
            cur = self.connection.cursor()
            cur.execute('SELECT prop_name_id FROM PropNames WHERE prop_name = "{}"'.format(property_name))
            row = cur.fetchone()
            if row is not None:
                # Сохраняем полученный идентификатор свойства в кэше
                self.cached_prop_names[property_name] = row[0]
                return row[0]

            sql_command = 'INSERT INTO PropNames (prop_name) VALUES ("{}")'.format(property_name)
            cur.execute(sql_command)

            # Сохраняем полученный идентификатор свойства в кэше
            self.cached_prop_names[property_name] = cur.lastrowid

            return cur.lastrowid

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

        return None

    # Private-функция: добавить/обновить значение свойства конкретного товара в базе данных
    def _insert_property_value(self, product_id, property_name_id, value, variation):

        sql_command = None

        if self.use_sqlite_syntax:

            # Для добавления/обновления записи в базу данных SQLite используем
            # "классический" синтаксис INSERT/UPDATE
            try:

                sql_command = ('REPLACE INTO Properties (shop_id, product_id, prop_key_id, prop_value, variation) '
                               'VALUES({}, {}, {}, "{}", "{}");'
                               ).format(self.shop_id, product_id, property_name_id, value, variation)
                self.connection.cursor().execute(sql_command)

            except sqlite3.Error as er:
                logging.error('База данных: ', er.args[0])
                logging.error('SQL Statement: ', sql_command)
        else:

            # В случае MySQL используем конструкцию REPLACE INTO, которая по смыслу
            # близка к MERGE в Microsoft SQL Server. Это даёт выигрыш в скорости
            # на порядок
            try:
                sql_command = ('REPLACE INTO Properties '
                               'SET shop_id = {}, product_id = {}, prop_key_id = {}, prop_value = "{}", '
                               'variation = "{}"'
                               ).format(self.shop_id, product_id, property_name_id, value, variation)
                self.connection.cursor().execute(sql_command)

            except mysql.connector.Error as er:
                logging.error('База данных: ', er)

    # Issue #101. Выполнить корректировку всех свойств товаров магазина
    def update_each_property(self, func):

        try:
            cur = self.connection.cursor()
            cur.execute('SELECT prop_value_id, prop_value FROM Properties WHERE shop_id = {} '.format(self.shop_id))
            for row in cur.fetchall():

                # Если после обработки значение свойства изменилось, то
                # обновляем его в базе данных
                updated_value = func(row[1])
                if updated_value != row[1]:

                    sql_command = 'UPDATE Properties SET prop_value = "{}" WHERE prop_value_id = {}'.format(
                        updated_value, row[0])
                    self.connection.cursor().execute(sql_command)

        except sqlite3.Error as er:
            logging.error('База данных: {}'.format(er.args[0]))
            pass

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

    # Issue #64. В случае использования MySQL сохраняем фотографии пачкой, чтобы
    # повысить скорость выполнения процедуры импорта данных (записи в базу)
    def add_photo_of_product(self, product_id, photo_url, successful=True, is_main=1):

        success = True

        if self.use_sqlite_syntax:
            success = self._add_photo_direct(product_id, photo_url, successful, is_main)
        else:

            # Issue #64. Предотвращаем повторную запись URL-а фотографии, что
            # связано со свойством карусели в магазине "Дочки-сыночки"
            if self.insertion_photos.find('"{}"'.format(photo_url)) > 0:
                return True

            if len(self.insertion_photos) > 0:
                self.insertion_photos += ',\n'

            if successful:
                self.insertion_photos += '({},{},"{}",1,{})'.format(self.shop_id, product_id, photo_url, is_main)
            else:
                self.insertion_photos += '({},{},"[Incorrect link] - {}",0,{})'.format(self.shop_id,
                                                                                       product_id, photo_url, is_main)
        return success

    # Функция сохраняет ссылку на оригинальное изображение товара в базе данных. Если извлечение
    # данных оказалось не удачным, то в записи о фотографии сохраняется пометка об неудачном
    # импорте фотографий продукта
    def _add_photo_direct(self, product_id, photo_url, successful=True, is_main=1):

        try:

            # Если открыт трассировочный файл, то помещаем в него информацию об
            # извлечённой ссылке на фотографию
            if self.dump_file is not None:
                print('product id = {}, photo = "{}"'.format(product_id, photo_url), file=self.dump_file)

            # Формируем SQL-запрос. Адрес фотографии, начинающийся с текста [Incorrect link] -
            # означает, что, фактически, извлечь данные с сайта не удалось
            sql_command = 'INSERT INTO Photos (shop_id, product_id, photo_url, is_successful, is_main) VALUES '
            if successful:
                sql_command += '({},{},"{}",1,{})'.format(self.shop_id, product_id, photo_url, is_main)
            else:
                sql_command += '({},{},"[Incorrect link] - {}",0,{})'.format(self.shop_id,
                                                                             product_id, photo_url, is_main)
            self.connection.cursor().execute(sql_command)
            return True

        except sqlite3.IntegrityError:
            # Фотография уже может быть в базе данных. Игнорируем.
            # Уведомляем управляющий код о том, что обновление информации
            # о фотографии прошло вполне успешно
            return True

        except sqlite3.Error as er:
            logging.error('База данных: ', er.args[0])

        except mysql.connector.IntegrityError:
            return True

        except mysql.connector.Error as er:
            logging.error('База данных: ', er)

        return False

    # Функция возвращает True, если в базе данных накоплены полезные данные в каждой
    # из таблиц, за исключением таблицы фотографий
    def check_useful_data(self, results):

        message = ""

        try:

            # Проверяем, если в таблицах с данными о товарах какие-либо данные.
            # После тестового извлечения данных, должно быть хоть что-нибудь
            for table in ['Categories', 'Products', 'Properties', 'Photos']:

                message = '  "{}": '.format(table)

                sql_command = 'SELECT * FROM {} WHERE shop_id = {} '.format(table, self.shop_id)
                if 'Photos' == table:
                    sql_command += "AND is_successful = 1"

                cur = self.connection.cursor()
                cur.execute(sql_command)
                row = cur.fetchone()

                if row is None:
                    message += 'Empty'
                else:
                    message += 'Success'
                results.append(message)

        except sqlite3.Error as er:
            message += 'Failure: ' + er.args[0]
            results.append(message)

        except mysql.connector.Error as er:
            message += 'Failure: ' + str(er)
            results.append(message)

    # Функция осуществляет удалении всех свойств определённого товара. Операция
    # обычно выполняется до извлечения наиболее актуальных данных
    def delete_properties_and_photos(self, property_id):

        try:
            sql_command = 'DELETE FROM Properties WHERE product_id={}'.format(property_id)
            self.connection.cursor().execute(sql_command)

        except sqlite3.Error as er:
            logging.error('Ошибка удаления свойств: ' + er.args[0])

        except mysql.connector.Error as er:
            logging.error('Ошибка удаления свойств: ' + str(er))

        try:
            sql_command = 'DELETE FROM Photos WHERE product_id={}'.format(property_id)
            self.connection.cursor().execute(sql_command)

        except sqlite3.Error as er:
            logging.error('Ошибка удаления изображения товара: ' + er.args[0])

        except mysql.connector.Error as er:
            logging.error('Ошибка удаления изображения товара: ' + str(er))

    # Функция осуществляет каскадное удаление всех записей о товарах, которые
    # помечены как "потенциально устаревшие" (outdated)
    def cascade_delete_outdated(self):

        cur = self.connection.cursor()
        cur.execute('SELECT * FROM Products WHERE outdated = 1')

        for product in cur.fetchall():
            cursor_to_delete = self.connection.cursor()

            # Выделяем идентификатор продукта и удаляем записи из таблиц,
            # в которых идентификатор.
            #
            # Причина, по которой здесь не используется cascade delete
            # состоит в том, что SQLite может не поддерживать FOREIGN KEY
            # в распространённых сборках (см. ключи SQLITE_OMIT_TRIGGER и
            # SQL_OMIT_FOREIGN_KEY). В будущем, имеет смысл исследовать этот
            # вопрос более внимательно, либо отказаться от использования SQLite

            product_id = product[0]
            sql_command = 'DELETE FROM Properties WHERE product_id = {}'.format(product_id)
            cursor_to_delete.execute(sql_command)

            sql_command = 'DELETE FROM Photos WHERE product_id = {}'.format(product_id)
            cursor_to_delete.execute(sql_command)

            sql_command = 'DELETE FROM Products WHERE product_id = {}'.format(product_id)
            cursor_to_delete.execute(sql_command)
