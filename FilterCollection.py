#!/usr/bin/python
# -*- coding: utf-8

import re
import logging
import xml.etree.ElementTree as ElementTree
from xml.etree.ElementTree import ParseError


# Класс обеспечивает поиск условий отбора данных для обработки
# подмножества связанного со именем фильтра
class Filter:

    # Словарь, в котором по названию магазина и имени фильтра можно найти
    # название категории и условие поиска. Это статический член класса
    conditions_dict = {}

    # Словарь, в котором название фильтра связано со списком вариаций и их
    # приоритета при проведении экспорта данных в JSON. Issue #119
    variations_priority = {}

    # todo: параметр index_shop_name плохо совместим с использованием
    # параметра variations в JsonFactory. Нужно будет запланировать
    # рефакторинг
    def __init__(self, index_shop_name, index_filter_name):

        # Если данные ещё не загружены из XML-документа, то загружаем их.
        # Поскольку conditions_dict является статическим атрибутом
        # класса, загрузка словаря осуществляется только один раз
        if not Filter.conditions_dict:

            try:
                tree = ElementTree.parse('conditions.xml')
                for child in tree.getroot():

                    # Проверяем, есть ли атрибут "name" у названия магазина и
                    # сохраняем его значение - оно будет использовано в качестве
                    # составной части индекса в conditions_dict
                    if 'name' not in child.attrib:
                        continue
                    shop_name = child.attrib['name'].upper()

                    for category in child:

                        # Если у названия категории есть синоним, то выделяем его
                        if 'alias' in category.attrib:
                            category_name = category.attrib['alias'].upper()

                            # Выделяем значения тэгов "name" и "sql", в которых
                            # находятся описание категории и условие отбора конкретных
                            # записей о товарах, характерной для категории
                            russian_name = category.find('name').text
                            sql_condition = category.find('sql').text

                            # Выделяем список вариаций товара, применимых к данной
                            # категории
                            variations_list = category.find('variations')
                            if variations_list is not None:

                                if category_name in Filter.variations_priority:
                                    message = '"conditions.xml" содержит дубликат вариаций в категории "{}"'.format(
                                        category_name)
                                    logging.error(message)

                                else:

                                    # Список вариаций категории представлен в файле
                                    # 'conditions.xml' в виде строки - её нужно представить
                                    # как список (массив), в котором все названия приведены
                                    # в врехнем регистре
                                    var_list = variations_list.text.split(',')
                                    for i, element in enumerate(var_list):
                                        var_list[i] = var_list[i].strip().upper()

                                    Filter.variations_priority[category_name] = var_list

                            # Выделяем значение свойства each_product, которое определяет,
                            # устанавливать ли товарам в данной категории уникальный индекс
                            # при осуществлении экспорта в JSON, или комбинированный индекс,
                            # "тип продукта + бренд + модель"
                            each_product_in_json = None
                            if category.find('each_product') is not None:
                                each_product_in_json = category.find('each_product').text

                            index = shop_name + '-' + category_name
                            Filter.conditions_dict[index] = {'category': russian_name, 'condition': sql_condition}

                            if each_product_in_json is not None:
                                Filter.conditions_dict[index]['each_product'] = each_product_in_json

            except ParseError as err:

                # Сохраняем в логах информацию об ошибке
                message = 'Ошибка разбора документа "conditions.xml": {}'.format(str(err))
                logging.critical(message)
                exit()

            except OSError as e:
                message = 'Ошибка чтения при работе с файлом "conditions.xml": {}'.format(e.errno)
                logging.critical(message)
                exit()

        # При вызове конструктора сохраняем индекс для поиска элементов в словаре
        self.dict_key = '{}-{}'.format(index_shop_name, index_filter_name).upper()
        self.trace_shop_name = index_shop_name
        self.trace_filter_name = index_filter_name

    def get_conditions(self, db):

        # Issue #103. Проверяем корректность значения фильтра и если
        # он не корректен, то выводим диагностическую информацию
        if self.dict_key not in Filter.conditions_dict:
            message = 'Фильтр "{}" ({}) не найден в файле "conditions.xml"'.format(self.trace_filter_name,
                                                                                   self.trace_shop_name)
            raise ValueError(message)

        # Если явным образом указано имя категории в фильтре, то находим
        # materialized path этой категории, комбинируем его как единственный
        # параметр строки с запросом поля 'sql' И считаем, что на этом
        # формирование строки дополнительных параметров SQL-запроса завершилось
        sql_conditions = ''

        category_name = self.get_category_name()
        if category_name is not None:

            materialized_path = db.get_materialized_path_by_name(category_name)
            if materialized_path is None:

                logging.error('Не удалось найти materialized path для категории "{}"'.format(category_name))

            else:

                sql_conditions = self._get_condition(materialized_path)

        else:

            # Если имя категории не указано, то возвращаем подстроку SQL-запроса,
            # указанную в тэге 'sql'
            sql_conditions = self._get_condition(None)

        # А вот здесь начинается самое интересное: в получившейся строке
        # условия отбора записей в SQL-запросе все вхождения типа [имя категории]
        # заменяются на явно определённое условие отбора как категории с
        # указанным именем, так и всех дочерних подкатегорий
        top_level_categories = re.findall(r'\[(.+?)\]', sql_conditions)
        for tlc in top_level_categories:

            # Первым символом в условии может быть '~', что будет означать, что
            # категорию с указанным именем и все подкатегории нужно исключить
            # из результататов отбора
            is_positive_condition = True
            if len(tlc) > 0:
                if tlc[0] == '~':
                    is_positive_condition = False
                    tlc = tlc[1:]

            materialized_path = db.get_materialized_path_by_name(tlc)
            if materialized_path is None:

                # Данная ошибка может произойти в том случае, если, например,
                # категории данного магазина ещё не были обработаны. Такая ситуация
                # может возникнуть, если, например, мы пытаемся экспортировать (JSON)
                # данные магазина "Детский мир", а этот магазин ещё не был обработан
                logging.error('Подкатегория с названием "{}" в базе данных не найдена'.format(tlc))
                continue

            if is_positive_condition:

                complex_condition = "group_name='{}' OR parents='{}' OR " \
                                    "parents LIKE '{}.%' ".format(tlc, materialized_path, materialized_path)
                sql_conditions = sql_conditions.replace('[{}]'.format(tlc), complex_condition)

            else:

                complex_condition = "(group_name <> '{}' AND parents <> '{}' AND " \
                                    "parents NOT LIKE '{}.%') ".format(tlc, materialized_path, materialized_path)
                sql_conditions = sql_conditions.replace('[~{}]'.format(tlc), complex_condition)

        return sql_conditions

    # Функция возвращает русскоязычное имя категории, которой соответствует фильтр
    # заданный в конструкторе класса
    def is_unique_each_product(self):
        if self.dict_key in Filter.conditions_dict:
            if 'each_product' in Filter.conditions_dict[self.dict_key]:
                if 'UNIQUE' == Filter.conditions_dict[self.dict_key]['each_product'].upper():
                    return True

        return False

    # Функция возвращает русскоязычное имя категории, которой соответствует фильтр,
    # заданный в конструкторе класса
    def get_category_name(self):
        if self.dict_key in Filter.conditions_dict:
            if 'category' in Filter.conditions_dict[self.dict_key]:
                return Filter.conditions_dict[self.dict_key]['category']

        logging.error('Имя категории по ключу "{}" не найдено'.format(self.dict_key))
        return None

    # Функция возвращает часть SQL-запроса, размещаемого после оператора
    # условия WHERE
    def _get_condition(self, materialized_path):
        if self.dict_key in Filter.conditions_dict:
            if 'condition' in Filter.conditions_dict[self.dict_key]:
                if materialized_path is not None:
                    return Filter.conditions_dict[self.dict_key]['condition'].format(materialized_path)
                return Filter.conditions_dict[self.dict_key]['condition']

        logging.error('Условие отбора по ключу "{}" не найдено'.format(self.dict_key))
        return None

    # Функиця возвращает список вариаций товара, по указанному
    # универсальному имени категории (alias).
    def get_variations_priority(self):
        if self.trace_filter_name in Filter.variations_priority:
            return Filter.variations_priority[self.trace_filter_name]

        return None
