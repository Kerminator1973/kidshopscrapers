#!/usr/bin/python
# -*- coding: utf-8

import logging
from ProductDescription import ProductContainer # Класс, используемый для объединения информации о товаре
import ScrapingHelper                           # Набор вспомогательных функций


class BaseScraperLego:

    def __init__(self, database_helper, options):

        # Сохраняем ссылку на класс работы с базой данных. Запоминаем
        # настройки, полученные от пользователя класса
        self.db = database_helper
        self.so = options

    def get_shop_name(self):
        logging.critical('Не определена реализация абстрактного метода')
        return None

    def get_shop_site(self):
        logging.critical('Не определена реализация абстрактного метода')
        return None

    # Дополнительные свойства из названия товара не извлекаются
    def extract_attributes(self, filter_name):
        pass

    # Public-функция, помещает в коллекцию append_to информацию о товарах
    # отобранных по фильтру filter_name, в иерархическом, структурированном виде
    def export_products(self, filter_name, append_to):

        if "LEGO" == filter_name or "ALL" == filter_name:

            # todo: рассмотреть возможность применения свойства outdated и для LEGO-магазинов
            self.db.for_each_product(None, lambda cat_id, cat_name,
                                                  prod_id, prod_name: self._export_playset(prod_id, append_to))
        else:
            logging.info('Фильтр отбора данных ("{}") не соответствует специализации магазина'.format(filter_name))

    # Private-функция включает данные об указанном товаре в контейнер append_to
    def _export_playset(self, product_id, append_to):

        product = ProductContainer(self.db, product_id, self.get_shop_name())
        product.fix_url(self.get_shop_site())
        product.fix_photos(self.get_shop_site())

        article = product.get_property('Артикул')
        if article is None:
            article = "LEGO-" + ScrapingHelper.generator_random_string()

        # Добавляем информацию о товаре в коллекцию
        if append_to.get(article) is None:
            append_to[article] = product
        else:
            append_to[article].merge_from(product)

