#!/usr/bin/python
# -*- coding: utf-8

import re
import urllib.parse
import urllib.request
import urllib.error
import logging
from bs4 import BeautifulSoup
import ScrapingHelper
from BaseScraper_Lego import BaseScraperLego


# Базовым классом является BaseScraperLego, который объединяет общие функции для
# магазинов "Мир Кубиков" и "Купи-Кубик"
class MirKubikovCatalogScraper(BaseScraperLego):

    def __init__(self, database_helper, options):

        # Вызываем конструктор базового класса
        super(MirKubikovCatalogScraper, self).__init__(database_helper, options)

        # Указываем название магазина, с которым должны быть связаны все операции
        # в базе данных, связанные с категориями магазина "Мир Кубиков"
        database_helper.activate_shop('MirKubikov')

    def get_shop_name(self):
        return 'Мир Кубиков'

    def get_shop_site(self):
        return 'http://www.mir-kubikov.ru/'

    # Public-функция осуществляет загрузку каталога товарных групп магазина "Мир Кубиков"
    def scrape_catalog(self, filter_name):

        self._scrape_categories()

        # Проверяем соответствие фильтра специализации магазина
        if "ALL" != filter_name and "LEGO" != filter_name:
            logging.info('Условия фильтрации исключают данные магазина Купи-Кубик из обработки')
            return

        # Осуществляем попытку получения данных по всем накопленным в базе
        # данных категориям
        self.db.process_categories(
            lambda rowid, url: self._scrape_the_showcase(rowid, url),
            self.so.scrape_categories_to_process)

        # Извлекаем список фотографий товаров
        if self.so.scrape_details_by_count > 0:
            print("Извлечение свойств товаров...")
            self.db.process_each_product(self._scrape_details, self.so.scrape_details_by_count)

    # Private-функция осуществляет разбор названий товарных категорий
    def _scrape_categories(self):
        try:

            self.categories = []

            # Загружаем страницу сайта, посвящённую наборам LEGO,
            # и ищем на ней описание групп
            url = self.get_shop_site() + "lego/"
            with urllib.request.urlopen(url) as response:
                # Загружаем html-документ в кодировке UTF-8
                html = response.read().decode('utf8')

                # Приводим документ в виду, удобному для дальнейшей обработки
                soup = BeautifulSoup(html, "html.parser")

                # Ищем блок описания группы элементов.
                for group in soup.findAll('article', attrs={'itemprop': 'itemListElement'}):

                    onclick = group['onclick']
                    if onclick is not None:

                        href = onclick[(onclick.find('=') + 2):-1]

                        meta_name = group.find('meta', attrs={'itemprop': 'name'})
                        if meta_name is None:
                            continue

                        if not meta_name.has_attr('content'):
                            continue

                        name = meta_name['content']

                        # В магазине "Мир Кубиков" на один и тот же набор могут ссылаться
                        # разные ссылки, например:
                        #   http://www.mir-kubikov.ru/lego/star-wars/derevnya-evokov/
                        #   http://www.mir-kubikov.ru/lego/eksklyuzivnye-nabory-lego/derevnya-evokov/
                        # Кажется разумным игнорировать ссылки с подстрокой "/eksklyuzivnye-nabory",
                        # т.к. это приводит к дублированию товара
                        if re.match('.*/eksklyuzivnye-nabory.*', href) is not None:

                            logging.info('Найдена категория "эксклюзивных" наборов: {} - Игнорируется'.format(url))
                            continue

                        # Добавляем в базу данных описание название группы товаров.
                        # Поскольку структура категорий сайта упрощённая, то
                        # считаем, что все категории являются частью отсуствующей
                        # корневой категории с идентификатором "1"
                        self.db.insert_category(name, href, "1")

        except urllib.error.URLError as e:
            logging.critical('Сбой в сетевой подсистеме', e.reason)
            exit()

    def _scrape_the_showcase(self, category_id, category_url):

        logging.info("Осуществляется обработка URL: {}".format(category_url))

        # На сайте "Мир-Кубиков" нет кнопки "Показать всё". На странице с наборами
        # может быть, или может не быть навигационная панель, которую можно использовать
        # для перехода между страницами сайта

        # Загружаем первую страницу и ищем на ней paginator <a class="pagination_i_a">
        # в котором есть параметр с ссылкой на страницу
        try:
            # В конце url(а) добавляем специальный флаг, которым мы указываем, что
            # мы хотим получать по 48 товаров на странице. Эта оптимизация
            # ускоряет обработку сайта на ~40%
            url = self.get_shop_site() + category_url + '/?onpage=48'

            with urllib.request.urlopen(url) as response:
                html = response.read().decode('utf8')
                soup = BeautifulSoup(html, "html.parser")

                # Обрабатываем текущую страницу
                self.db.begin_products_insertion()
                self._collect_playsets(category_id, soup)
                self.db.end_products_insertion()

                # Если на странице есть навигационная панель, то обрабатываем
                # все страницы, на которые есть ссылки в навигационной панели
                pages = soup.findAll('a', attrs={'class': 'pagination_i_a'})
                if len(pages) > 0:
                    for page in pages:
                        # Предполагаю, что это особенность Bitrix - в навигационной
                        # панели может быть доступна ссылка на первую страницу,
                        # которая уже была нами обработана. Признак текущей страницы -
                        # дополнительный класс "__current"
                        if "__current" in page.attrs['class']:
                            logging.debug("Пропускае ранее обработанную первую страницу")
                            pass
                        else:
                            if page['href'] is not None:
                                link = self.get_shop_site() + page['href']
                                with urllib.request.urlopen(link) as response2:

                                    soup = BeautifulSoup(response2.read().decode('utf8'), "html.parser")

                                    self.db.begin_products_insertion()
                                    self._collect_playsets(category_id, soup)
                                    self.db.end_products_insertion()

        except urllib.error.URLError as e:
            logging.critical('Сбой в сетевой подсистеме', e.reason)
            exit()

    # В функцию _collect_playsets передаётся soup, а не html для того, чтобы
    # избежать двойного построения дерева элементов html для обработки
    # paginator(a) и для обработки первой страницы группы LEGO элементов
    def _collect_playsets(self, category_id, soup):

        articles = soup.findAll('div', attrs={'class': 'catalog_i_number'})
        names = soup.findAll('span', attrs={'itemprop': 'name'})
        url_details = soup.findAll('a', attrs={'itemprop': 'url'})
        prices = soup.findAll('div', attrs={'itemprop': 'price'})

        if len(articles) != len(names) or len(names) != len(url_details) or len(url_details) != len(prices):
            logging.error('Расхождение в количестве ключевых атрибутов товаров')
            return

        if (articles is not None) and (names is not None) and (prices is not None) and (url_details is not None):

            for i, element in enumerate(articles):

                article = element.find('span').getText()
                name = names[i].getText().replace('"', "'")
                price = ScrapingHelper.parse_amount(prices[i].getText())

                if not url_details[i].has_attr('href'):
                    logging.error('Набор "{}" не содержит ссылки на страницу атрибутов'.format(name))
                    continue

                url = url_details[i]['href']

                product_id = self.db.insert_product(category_id, name, price, url, force_insert=True)
                self.db.insert_property(product_id, "Артикул", article)

        else:
            logging.error('Не найдено ни одного элемента')

    # Private-функция разбора страницы отдельного товара
    def _scrape_details(self, product_id, product_url):

        # Удаляем ранее извлечённые свойства товара
        self.db.delete_properties_and_photos(product_id)

        # Сохранять свойства будем через специальный кэширующий механизм
        self.db.begin_props_insertion()

        # Осуществляем попытку загрузки изображения с персональной страницы товара
        success = False
        try:

            # Формируем адрес страницы
            url = self.get_shop_site() + product_url
            with urllib.request.urlopen(url) as response:

                html = response.read().decode('utf8')
                soup = BeautifulSoup(html, 'html.parser')

                gallery_block = soup.find('div', attrs={'class': 'gallery'})
                if gallery_block is None:
                    logging.error('Не найден атрибут блок "gallery". Страница: ', product_url)
                    return

                images = gallery_block.findAll('div', attrs={'class': 'gallery_view_i'})
                for img_block in images:

                    image = img_block.find('img', attrs={'itemprop': 'image'})
                    if not image.has_attr('data-zoom-image'):
                        continue

                    is_main = 0
                    if '__current' in img_block['class']:
                        is_main = 1

                    if not image.has_attr('src'):
                        logging.error('Не найден атрибут "src". Страница: ', product_url)
                        continue

                    # Сохраняем ссылку на вспомогательные изображения товара
                    self.db.add_photo_of_product(product_id, image['src'], True, is_main)
                    success = True

                # Пытаемся извлечь дополнительные атрибуты товара: в этом магазине
                # дополнительные атрибуты хранятся в блоках <article class='tx __xs'>,
                # внутри парных тэгов <p></p>. При этом название свойства и значение
                # свойства разделяются посредством двоеточия
                for block in soup.findAll('article', attrs={'class': '__xs'}):
                    for paragraph in block.findAll('p'):

                        fields = paragraph.getText().strip().split(':')
                        if len(fields) > 1:

                            prop_key = fields[0].strip()
                            prop_value = fields[1].strip()

                            # Если длина поля слишком большая, то, скорее всего, мы извлекли
                            # не свойство, а пояснительный текст, который так же размещается
                            # в блоках <article class='tx __xs'>
                            if len(prop_key) > 20 or len(prop_value) > 20:
                                continue

                            if len(prop_key) > 0 and len(prop_value) > 0:
                                self.db.insert_property(product_id, prop_key, prop_value)

        except urllib.error.URLError as e:
            logging.error('Сбой в сетевой подсистеме', e.reason)
            pass

        # Если по каким-то причинам выделить ссылку на изображение не удалось, сохраняем
        # информацию об этом в базе данных для последующего разбора. Так же важно исключить
        # эту запись из повторной обработки, т.е. добавив ссылку на Products.rowid
        # из поля product_id таблицы Photos
        if not success:
            self.db.add_photo_of_product(product_id, product_url, successful=False)

        # Осуществляем пакетную запись в базу данных всех свойств сразу
        self.db.end_props_insertion()
