#!/usr/bin/python
# -*- coding: utf-8

import logging
from ProductDescription import ProductContainer  # Класс, используемый для объединения информации о товаре
import ScrapingHelper


class TesterCatalogScraper:
    DUMMY_CATEGORY = 'Детские стульчики'
    DUMMY_CATEGORY_URL = '/catalog/chairs'
    DUMMY_PRODUCT_URL = '/catalog/product/6700'
    DUMMY_OUTDATED_URL = '/catalog/outdated/6704'

    def __init__(self, database_helper, options):

        # Сохраняем ссылку на класс работы с базой данных. Запоминаем
        # настройки, полученные от пользователя класса
        self.db = database_helper
        self.so = options

        # Указываем условное название магазина, с которым будут связан операции
        # осуществляющие проверку корректность работы кода ScrapingDatabase
        database_helper.activate_shop('dbTester')

    def get_shop_name(self):
        return 'dbТестер'

    def get_shop_site(self):
        return 'http://www.db-tester.ru/'

    def execute_tests(self):

        # Выполняем тесты вспомогательного инструментария

        if not self._execute_cleaning_helper_test():
            return False

        # Тесты сломались после изменения алгоритма извлечения
        # вариаций товара из названия
        # if not self._execute_scraping_helper_test():
        #   return False

        # Добавляем корневую категорию
        category_id = self.db.insert_category('Детские товары', '/catalog/kids_needs', '0')
        if category_id is None:
            logging.error('Не была добавлена новая товарная категория')
            return False

        # Добавляем вспомогательную категорию (она будет использоваться в тестах)
        category_id = self.db.insert_category(self.DUMMY_CATEGORY, self.DUMMY_CATEGORY_URL, str(category_id))
        if category_id is None:
            logging.error('Не была добавлена вспомогательная товарная категория')
            return False

        # Выполняем тесты работы с базой данных
        if not self._execute_test_inserts(category_id):
            return False

        if not self._execute_test_process_categories(category_id):
            return False

        if not self._execute_test_outdated_links(category_id):
            return False

        # Явным образом проверяем режим обновления информации о деталях при импорте,
        # поскольку под это оптимизирован тест _execute_test_outer_join()
        if self.so.update_details:
            if not self._execute_test_outer_join(category_id):
                return False
        else:
            logging.error('Тест _execute_test_outer_join() необходимо запускать с параметром --update')

        return True

    # Тесты ВСПОМОГАТЕЛЬНОГО ИНСТРУМЕНТАРИЯ - честка текста от всякого шлака,
    # в частности, от html-текста
    def _execute_cleaning_helper_test(self):

        result = ScrapingHelper.clean_description_text('\n\n<p>\n<strong>Машина на радиоупр Audi A6L</strong></p>')
        if result != '<p>Машина на радиоупр Audi A6L</p>':
            return False

        result = ScrapingHelper.clean_description_text('<li>\n\t\tМалыш быстро научится управлять авто.</li>')
        if result != 'Малыш быстро научится управлять авто. ':
            return False

        result = ScrapingHelper.clean_property_value('дневные<br />ночные')
        if result != 'дневные; ночные':
            return False

        return True

    # Тесты ВСПОМОГАТЕЛЬНОГО ИНСТРУМЕНТАРИЯ - разделение текста
    # на тот, которых находится внутри скобок и тот, который находится вне скобок
    def _execute_scraping_helper_test(self):

        # Самый простой текст
        results = ScrapingHelper.split_embraced_text('Simple')
        if len(results) != 2:
            logging.error('Не работает split_embraced_text("Simple") с текстом без скобок')
            return False

        if results[0] != 'Simple':
            logging.error('Не корректное выделение параметра вне скобок в split_embraced_text("Simple")')
            return False

        if len(results[1]) != 0:
            logging.error('Не корректное выделение параметра в скобках в split_embraced_text("Simple")')
            return False

        # Тест с реальным разделением на составляющие
        results = ScrapingHelper.split_embraced_text('Simple(Easy)')
        if len(results) != 2:
            logging.error('Не работает split_embraced_text("Simple(Easy)")')
            return False

        if results[0] != 'Simple':
            logging.error('Не корректное выделение параметра вне скобок в split_embraced_text("Simple(Easy)")')
            return False

        if results[1] != 'Easy':
            logging.error('Не корректное выделение параметра в скобках в split_embraced_text("Simple(Easy)")')
            return False

        # Тест нормализации названия бренда
        result = ScrapingHelper.normalize_brand('ME & WE')
        if result != 'ME&WE':
            logging.error('Ошибка в нормализации названия бренда (проблема с амперсандом)')
            return False

        # Тест с реальным разделением на составляющие - тройной элемент
        results = ScrapingHelper.split_embraced_text('Simple(Easy)Clear')
        if results[0] != 'Simple Clear':
            logging.error('Не корректное выделение параметра вне скобок в split_embraced_text("Simple(Easy)")')
            return False

        if results[1] != 'Easy':
            logging.error('Не корректное выделение параметра в скобках в split_embraced_text("Simple(Easy)")')
            return False

        # Для проверки разбора свойств по справочнику вариаций товаров,
        # загружаем этот справочник
        self.db.load_unified_variations_json('variations3.json')

        # Выполняю тест получения нескольких параметров в строке
        sample = 'НОСКИ БОРДЕЛЬМАНН РАДОСТЬ НЕВЕСТЫ JUNGLE GREEN 6 ШТ.'
        short_name, result = ScrapingHelper.find_unified_attrs(sample, self.db.unified_variations)
        if result is None or len(result) != 2:
            logging.error('Не найдены свойства в строке: {}'.format(sample))
            return False

        if result[0] != ('Цвет', 'JUNGLE GREEN') and result[1] != ('Цвет', 'JUNGLE GREEN'):
            logging.error('Параметры "{}" извлечёны не корректно из строки "{}"'.format(result, sample))
            return False

        if result[0] != ('Кол-во шт', '6') and result[1] != ('Кол-во шт', '6'):
            logging.error('Параметры "{}" извлечёны не корректно из строки "{}"'.format(result, sample))
            return False

        print('В тестовой строке "{}" найдены свойства: {}'.format(sample, result))

        test_samples = [
            (', БЕЛАЯ С РИСУНКОМ "РОМБ", РАЗМЕР: 42', ('Размер', '42'), 'БЕЛАЯ С РИСУНКОМ "РОМБ"'),
            ('ПОДГУЗНИКИ S (4-8 КГ)', ('Вес ребенка', 'S'), 'ПОДГУЗНИКИ'),
            ('"NICOL, ПЕСОЧНИК ""Зебра"", (желт), p. 74, Польша"', ('Размер', '74'),
                'NICOL, ПЕСОЧНИК "ЗЕБРА"'),
            ('BONY KIDS, КОМПЛЕКТ (БЛУЗКА+ШОРТЫ), Р. 9 М', ('Размер', '9'),
                'BONY KIDS, КОМПЛЕКТ'),
            ('КОМПЛЕКТ КУРТА / БРЮКИ БАТИК ДАСТИ 2, ЦВЕТ: СИНИЙ', ('Цвет', 'СИНИЙ'),
                'КОМПЛЕКТ КУРТА / БРЮКИ БАТИК ДАСТИ 2'),
            (', ЦВЕТ: ГОЛУБОЙ', ('Цвет', 'ГОЛУБОЙ'), ''),
            ('LEADER KIDS, ПОДУШКА "КУБИКИ", (40*60), (БЕЛЫЙ) (бязь)', ('Цвет', 'БЕЛЫЙ'),
                'LEADER KIDS, ПОДУШКА "КУБИКИ"'),
            ('HEXBUG, РОБОТ-МИКРО "СКАРАБЕЙ" ЧЕРНЫЙ', ('Цвет', 'ЧЕРНЫЙ'), 'HEXBUG, РОБОТ-МИКРО "СКАРАБЕЙ"'),
            ('RÖMER KIDFIX XP-SICT STONE GREY', ('Цвет', 'STONE GREY'), 'RÖMER KIDFIX XP-SICT'),
            ('КОЛЬЦО MICROSOFT MEGARU, BLACK FRAME GREY, FOCUS', ('Цвет', 'BLACK FRAME GREY'),
                'КОЛЬЦО MICROSOFT MEGARU'),
            ('КОЛЯСКА ADIDAS SUPER SPEED "AUTUMN GOLD" ЧТО-ТО ЕЩЁ', ('Цвет', 'AUTUMN GOLD'),
                'КОЛЯСКА ADIDAS SUPER SPEED'),
            ('ПОДГУЗНИКИ ЗАССЫХА НЕПРОССАТЬ (ОТ 4 ДО 8 КГ)', ('Вес ребенка', 'S'), 'ПОДГУЗНИКИ ЗАССЫХА НЕПРОССАТЬ'),
            ('ПОДГУЗНИКИ ДОСУХА PREMIUM 40 ШТ.', ('Кол-во шт', '40'), 'ПОДГУЗНИКИ ДОСУХА PREMIUM'),
            ('ПОДГУЗНИКИ ДОСУХА CARE LITE ШТ. 52, ПРИНЦЕССЫ И ДРАКОНЫ', ('Кол-во шт', '52'),
                'ПОДГУЗНИКИ ДОСУХА CARE LITE'),
            ('ПОДГУЗНИКИ ДЕТКИ PREMIUM 48 ШТ.', ('Кол-во шт', '48'), 'ПОДГУЗНИКИ ДЕТКИ PREMIUM'),
            ('ПОДГУЗНИКИ ДЕТКИ СОКОЛ 24 ШТУК, ОЗИМЫЕ КУЛЬТУРЫ', ('Кол-во шт', '24'), 'ПОДГУЗНИКИ ДЕТКИ СОКОЛ'),
            ('ПОДГУЗНИКИ BELLA BABY HAPPY MAXI PLUS 62ШТ', ('Кол-во шт', '62'), 'ПОДГУЗНИКИ BELLA BABY HAPPY MAXI PLUS'),
            ('КОДРА УМБЕР СТЭК ЦВЕТ: КРАСНЫЙ', ('Цвет', 'КРАСНЫЙ'), 'КОДРА УМБЕР СТЭК'),
            ('ВОЛТА ЭТО БАХУС (ТРАНКВИЛИЗАТОР) ШТ. 40', ('Кол-во шт', '40'), 'ВОЛТА ЭТО БАХУС')
        ]

        for sample in test_samples:

            product_name = sample[0].upper()
            short_name, result = ScrapingHelper.find_unified_attrs(product_name, self.db.unified_variations)
            if result is None or len(result) == 0:
                logging.error('Не найдено свойство в строке: {}'.format(sample[0]))
                return False

            if len(result) != 1:
                logging.error('Не найден верный набор свойства в строке: {}'.format(sample[0]))
                return False

            if result[0] != sample[1]:
                logging.error('Параметр "{}" извлечён не корректно из строки "{}"'.format(result[0], sample[0]))
                return False

            if short_name != sample[2]:
                logging.error('Название модели извлечено не корректно "{}" (эталон: {})'.format(short_name, sample[2]))
                return False

            print('В тестовой строке "{}" найдено свойство: {}'.format(sample[0], result))

        return True

    # Тест на ОБНОВЛЕНИЕ данных
    def _execute_test_inserts(self, category_id):

        # Добавляем фиктивную запись о товаре, а затем перезаписываем её - если движок
        # работает корректно, то произойдёт замена полей, а не создание новой записи,
        # поскольку ключём в таблице является url
        product_id = self.db.insert_product(category_id, 'Пластиковый стул', 1999, self.DUMMY_PRODUCT_URL,
                                            force_insert=True)

        if product_id is None or product_id == 0:
            logging.error('Не добавлена запись о товаре')
            return False

        dummy_name = 'Стул из смолы'
        dummy_price = 1799
        self.db.insert_product(category_id, dummy_name, dummy_price, self.DUMMY_PRODUCT_URL, force_insert=True)

        # Получаем объект, описывающий товар и проверяем, корректно ли у него
        # были сохранены значения полей, или нет

        product = ProductContainer(self.db, product_id, self.get_shop_name())
        if product.offers_count() != 1:
            logging.error('Не корректное количество ценовых предложений')
            return False

        if product.container[product.OFFERS][0].get('price') is None:
            logging.error('Отсутствует поле с ценой товара')
            return False

        if product.container[product.OFFERS][0]['price'] != 1799:
            logging.error('Не корректное ценовое предложение (цена)')
            return False

        if product.container[product.OFFERS][0].get('url') is None:
            logging.error('Отсутствует поле с url товара')
            return False

        if product.container[product.OFFERS][0]['url'] != self.DUMMY_PRODUCT_URL:
            logging.error('Не корректное ценовое предложение (url)')
            return False

        if product.get_product_name() != 'СТУЛ ИЗ СМОЛЫ':
            logging.error('Не корректное название товара')
            return False

        return True

    # Тест на КОРРЕКТНУЮ ОБРАБОТКУ КАТЕГОРИЕЙ
    def _execute_test_process_categories(self, category_id):

        # Флаг, позволяющий зафиксировать факт передачи ожидаемой категории
        self._category_was_passed = False

        try:
            self.db.process_categories(
                lambda cat_id, cat_url: self._dummy_process_categories(cat_id, cat_url, category_id), 5)
        except:
            # Исключение было сброшено, поскольку во вспомогательный метод
            # поступили не корректные данные
            return False

        if not self._category_was_passed:
            logging.error('Ожидаемые данные не поступили в обработчик категорий')
            return False

        return True

    # Вспомогательная функция, проверяет совпадение идентификатора
    # категории и URL ожидаемым
    def _dummy_process_categories(self, cat_id, cat_url, category_id):

        if cat_id == category_id and cat_url == self.DUMMY_CATEGORY_URL:

            # Тест на получение корректных данных успешно пройден!
            self._category_was_passed = True

            # Эмулируем работу алгоритма извлечения данных, чтобы избежать
            # получения сообщений о "потенциально устаревших" записях о товаре
            self.db.insert_product(category_id, 'Стул из дерева', 3999, self.DUMMY_PRODUCT_URL, force_insert=True)

        else:

            # Если пришли данные отличные от ожидаемых, то сбрасываем исключение
            logging.error('В обработчик категорий поступили не корректные данные')
            raise Exception

    # Тест на ВЫЯВЛЕНИЕ ССЫЛОК НА УСТАРЕВШИЕ ДАННЫЕ
    def _execute_test_outdated_links(self, category_id):

        # Чтобы гарантировать успешность выполнения теста, ещё раз добавляем
        # данные, которые уже должны были бы быть добавлены в базу предыдущими
        # тестами
        plastic_chair_id = self.db.insert_product(category_id, 'Пластиковый стул', 1999, self.DUMMY_PRODUCT_URL,
                                                  force_insert=True)
        brown_chair_id = self.db.insert_product(category_id, 'Коричневый стул', 650, self.DUMMY_OUTDATED_URL,
                                                force_insert=True)

        self._dummy_product_id = 0

        try:
            self.db.process_categories(
                lambda cat_id, cat_url: self._dummy_process_without_outdated(cat_id, cat_url, category_id), 5)
        except:
            # Исключение было сброшено, поскольку во вспомогательный метод
            # поступили не корректные данные
            logging.error('Не корректная работа process_categories()')
            return False

        if self._dummy_product_id == 0:
            logging.error('Тестовая запись в базу данных не добавлена')
            return False

        # Во второй части теста, нам следует выполнить проход по продуктам внутри
        # категории. Ожидаемый результат - DUMMY_OUTDATED_URL не должен быть обработан.
        # При этом, проверять нужно не только вызов функции process_each_product(), но
        # и функции for_each_product()

        # Тест для process_each_product()
        self._products_was_passed = False

        try:
            self.db.process_each_product(self._dummy_check_outdated, 5)
        except:
            logging.error('Не корректная работа process_each_product()')
            return False

        if not self._products_was_passed:
            logging.error('process_each_product() сформировал не корректные данные')
            return False

        # Тест для for_each_product()
        self._products_was_passed = False

        try:
            self.db.for_each_product(None,
                                     lambda cat_id, cat_name, prod_id, prod_name:
                                     self._dummy_check_outdated(prod_id, prod_name))
        except:
            logging.error('Не корректная работа for_each_product()')
            return False

        if not self._products_was_passed:
            logging.error('for_each_product() сформировал не корректные данные')
            return False

        # В данный момент, у нас есть две записи о товарах в базе данных:
        # DUMMY_PRODUCT_URL и DUMMY_OUTDATED_URL.
        #
        # Добавляем у товаров свойства и списки фотографий. Этих данных
        # хватит для выполнения ещё одного теста - каскадного удаления
        # данных об устаревшем товаре
        self.db.begin_props_insertion()
        self.db.insert_property(plastic_chair_id, 'Article', '1908')
        self.db.insert_property(plastic_chair_id, 'Made in', 'China')
        self.db.add_photo_of_product(plastic_chair_id, '/photos/1908/profile.png', successful=True)
        self.db.add_photo_of_product(plastic_chair_id, '/photos/1908/bigpicture.png', successful=True)
        self.db.end_props_insertion()

        self.db.begin_props_insertion()
        self.db.insert_property(brown_chair_id, 'Article', '1770')
        self.db.insert_property(brown_chair_id, 'Made in', 'Spain')
        self.db.add_photo_of_product(brown_chair_id, '/photos/1908/bad_quality.png', successful=True)
        self.db.end_props_insertion()

        self.db.cascade_delete_outdated()

        # Суть теста: перечисляем вообще все товары, включая те, которые
        # оказались outdated
        try:
            self.db.for_each_product(None,
                                     lambda cat_id, cat_name, prod_id, prod_name:
                                     self._dummy_check_except_if_outdated(prod_id, prod_name),
                                     include_outdated=True)
        except:
            logging.error('Не корректная работа for_each_product()')
            return False

        # Следующий тест: снова добавляем запись о "Коричневом стуле" и
        # делаем её 'outdated' вызвав process_categories()
        self.db.insert_product(category_id, 'Коричневый стул', 700, self.DUMMY_OUTDATED_URL, force_insert=True)

        try:
            self.db.process_categories(
                lambda cat_id, cat_url: self._dummy_process_without_outdated(cat_id, cat_url, category_id), 5)
        except:
            logging.error('Не корректная работа process_categories()')
            return False

        # Теперь добавляем запись ещё раз и обрабатываем категорию так, как будто бы
        # все ранее сохранённые в базу данных товары есть на витрине
        self.db.insert_product(category_id, 'Коричневый стул', 680, self.DUMMY_OUTDATED_URL, force_insert=True)

        # После повтороного добавления записи в базу данных, её флаг 'outdated'
        # должен быть сброшен в нулевое значение и в базе не должно остаться
        # никаких outdated-записей
        try:
            self.db.for_each_product(None,
                                     lambda cat_id, cat_name, prod_id, prod_name:
                                     self._dummy_check_except_if_outdated(prod_id, prod_name),
                                     include_outdated=True)
        except:
            logging.error('Не корректная работа for_each_product()')
            return False

        return True

    # Вспомогательная функция, обновляет в базе данных только один товар
    # из двух, которые ранее были добавлены ранее. Цель - убедиться в том,
    # что у устаревшиго товар значение поля "outdated" устанавливается в "1"
    # и это поле перестаёт использоваться в дальнейших операциях
    def _dummy_process_without_outdated(self, cat_id, cat_url, category_id):

        self._dummy_product_id = self.db.insert_product(category_id, 'Пластиковый стул', 1899, self.DUMMY_PRODUCT_URL,
                                                        force_insert=True)

        # Второй товар - 'Коричневый стул' мы в базу данных не добавляем.
        # Функция process_categories() должна распознать, что ссылка
        # DUMMY_OUTDATED_URL является устаревшей

    # Вспомогательная функция, проверять соответствие состояния базы данных
    # ожиданиям
    def _dummy_check_outdated(self, product_id, second_parameter):

        if product_id == self._dummy_product_id:

            # Тест на получение корректных данных успешно пройден!
            self._products_was_passed = True

        else:

            # Если пришли данные отличные от ожидаемых, то сбрасываем исключение
            raise Exception

        pass

    # Тест проверяет, есть ли среди перечисленных данных те, которые были
    # удалены из базы данных
    def _dummy_check_except_if_outdated(self, product_id, second_parameter):

        product = ProductContainer(self.db, product_id, self.get_shop_name())

        if product.offers_count() == 0:
            logging.error('Не корректное количество ценовых предложений')
            raise Exception

        for offer in product.container[product.OFFERS]:
            if offer.get('outdated') is not None:

                # Свойство 'outdated' не должно было остаться после выполнения
                # циклической чистки данных по критерию 'outdated=1'
                raise Exception

        pass

    # Тест на ОСОБЕННОСТИ ВЫПОЛНЕНИЯ LEFT OUTER JOIN
    def _execute_test_outer_join(self, category_id):

        # Как работает функция process_each_product(): для данного магазина
        # осуществляется загрузка детальной информации о товаре для всех
        # накопленных товарах магазина (в случае теста - тип магазина это
        # TesterCatalogScraper). Первыми в списке страниц на обработку должны
        # идти ссылки на страницы, которые ещё на обрабатывались, а затем те,
        # которые уже обрабатывались. При этом дублирования ссылок не должно
        # быть. Тем не менее, дублирование может происходить из-за объединения
        # LEFT OUTER JOIN в том случае, если после SELECT-а не указано ключевое
        # слово DISTINCT - оно значит, "добавлять только уникальные данные"

        self.father_chair_was_happened = False
        self.mother_chair_was_happened = False
        self.baby_bear_chair_was_happened = False

        self.father_chair_id = self.db.insert_product(category_id, 'Стул для папы', 1999, '/catalog/product/1200',
                                                      force_insert=True)
        self.mother_chair_id = self.db.insert_product(category_id, 'Стул для мамы', 1699, '/catalog/product/1201',
                                                      force_insert=True)
        self.baby_bear_chair_id = self.db.insert_product(category_id, 'Стул для для медвежёнка', 1299,
                                                         '/catalog/product/1202', force_insert=True)

        self.db.begin_props_insertion()
        self.db.add_photo_of_product(self.mother_chair_id, '/photos/2002/first_photo.png', successful=True)
        self.db.add_photo_of_product(self.mother_chair_id, '/photos/2002/second_photo.png', successful=True)
        self.db.end_props_insertion()

        try:
            self.db.process_each_product(self._dummy_process_each_chair, 999)
        except:
            logging.error('Не корректное извлечение данных по схеме LEFT JOIN')
            return False

        if not (
                        self.father_chair_was_happened and self.mother_chair_was_happened and self.baby_bear_chair_was_happened):
            logging.error('Не корректное извлечение данных по схеме LEFT JOIN')
            return False

        return True

    def _dummy_process_each_chair(self, product_id, product_url):

        if product_id == self.father_chair_id:

            # Если ссылка на страницу посвящённую стулу мамы-мишки пришла
            # раньше ссылки на страницу стула папы-мишки это ошибка
            if self.mother_chair_was_happened:
                raise Exception

            # Если ссылка на страницу пришла повторно - это является ошибкой
            if self.father_chair_was_happened:
                raise Exception

            self.father_chair_was_happened = True
            return

        if product_id == self.baby_bear_chair_id:

            if self.mother_chair_was_happened:
                raise Exception

            if self.baby_bear_chair_was_happened:
                raise Exception

            self.baby_bear_chair_was_happened = True
            return

        if product_id == self.mother_chair_id:

            # Ссылка на страницу о стуле мамы-мишки должна прийти
            # последней
            if not self.father_chair_was_happened:
                raise Exception

            if not self.baby_bear_chair_was_happened:
                raise Exception

            # Второй раз на страницу о стуле мамы-мишке попасть мы не должны
            if self.mother_chair_was_happened:
                raise Exception

            self.mother_chair_was_happened = True
