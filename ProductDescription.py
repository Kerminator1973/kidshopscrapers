#!/usr/bin/python
# -*- coding: utf-8

import ScrapingHelper   # Набор вспомогательных функций


# Класс является контейнером свойств товара и реализует набор функции
# объединения информации о товаре из разных магазинов
class ProductContainer:

    PRODUCT_NAME = 'ProductName'
    SHORT_NAMES = 'short_names'
    BRAND = 'Brand'
    EXCESSIVE_NAMES = 'excessive_names'
    OFFERS = 'Offers'
    PHOTOS = 'images'
    PROPERTIES = 'Properties'
    EXTRA = 'Extra'
    AINDEX = 'AIndex'       # Альтернативный индекс товара

    # Issue #118. Список системных свойств, которые не могут являться вариациями
    SYSTEM_PROPERTIES = ['short_name', 'category', 'breadcrumb', 'Бренд', 'product_type', 'brand']

    # В конструкторе сохраняем ссылку на экземпляр базы данных.
    # Так же создаём в базе данных контейнер для хранения объединённой информации о товарах
    def __init__(self, db, product_id, shop_name):
        """
        :param db: объект доступа к базе данных
        :type db: object
        :param product_id: идентификатор товара в магазине
        :type product_id: int
        :param shop_name: Название магазина из которого загружаются данные
        :type shop_name: str
        """

        self.container = {}

        db.get_product_description(product_id, shop_name, self)

    # Функция позволяет установить название бренда
    def set_brand(self, brand):
        self.container[self.BRAND] = brand.upper()

    # Функция возвращает название бренда
    def get_brand(self):
        if self.container.get(self.BRAND) is None:
            return None
        return self.container[self.BRAND]

    # Функция позволяет сохранить в описании объекта альтернативные
    # индексы, состояшие из бренда и артикула, назначенного производителем
    # товара.
    # При установке, артикул производителя подвергается дополнительной
    # обработке - из него удаляются пробелы, а "косая черта" используется
    # в качестве символа-разделителя нескольких артикулов. Примеры:
    # 006.2008/006.2050 - http://www.dochkisinochki.ru/icatalog/products/166505/
    # "28 815" - https://www.detmir.ru/product/index/id/1754171/
    def set_alternative_index(self, producer_article):
        """
        :param str producer_article: артикул производителя
        """

        brand = self.get_brand()
        if brand is None or len(brand) == 0 or producer_article is None:
            return

        # Удаляем пробелы и разделяем артикул на отдельные составляющие.
        # Перед альтернативным индексом размещаем бренд, чтобы артикулы
        # товаров разных брендов не были перемешаны друг с другом
        line = producer_article.replace(" ", "")
        self.container[self.AINDEX] = brand + '-' + line

    # Функция возвращает список альтернативных индексов товара
    def get_alternative_index(self):
        """
        :return str: возвращает альтернативный индекс товара
        """

        if self.container.get(self.AINDEX) is None:
            return None

        return self.container[self.AINDEX]

    # Функция позволяет установить название бренда товара
    def set_product_name(self, product_name):
        self.container[self.PRODUCT_NAME] = product_name.upper()

    def get_product_name(self):
        if self.container.get(self.PRODUCT_NAME) is None:
            return None
        return self.container[self.PRODUCT_NAME]

    # Функция позволяет добавить ещё один вариант названия товара в список
    # возможных названий.
    # EXCESSIVE - излишнее, не умеренное... Поскольку содержит много
    # слабо структурированной информации
    def append_excessive_name(self, name):
        if self.container.get(self.EXCESSIVE_NAMES) is None:
            self.container[self.EXCESSIVE_NAMES] = []
        self.container[self.EXCESSIVE_NAMES].append(name.upper())

    # Функция добавляет информацию о ценовом предложении товара, включающим:
    # магазин, цену, url страницы товара, набор фотографий и коллекцию свойств
    # товара из конкретного ценового предложения
    def append_offer(self, shop, price, price_old, url, photos, properties, description, instock, outdated):
        """
        :param str shop: название магазина, в котором продаётся товар
        :param int price: цена товара со скидкой
        :param int price_old: цена товара до применения с скидки
        :param str url: адрес страницы с детальной информацией о товаре
        :param list photos: список фотограций товара
        :param dict properties: словарь свойств товара
        :param str description: подробное описание товара
        :param int instock: признак наличия товара в магазине. 1 - в наличии, 0 - отсутствует
        :param int outdated: признак того, что ссылка на товар является устаревшей
        """

        if price_old is None:
            price_old = price

        if self.container.get(self.OFFERS) is None:
            self.container[self.OFFERS] = []

        offer_dict = {'shop': shop, 'price': price, 'url': url,
                      self.PHOTOS: photos, self.PROPERTIES: properties, 'Description': description,
                      'instock': instock}

        # Поле outdated включает только для тех случаев, когда оно
        # установлено в единицу, чтобы не переполнять JSON-файл избыточной
        # информацией
        if outdated != 0:
            offer_dict['outdated'] = outdated

        # Поле старая цена добавляем только в том случае, если они отличаются
        # друг от друга
        if price_old is not None:
            if price != price_old:
                offer_dict['price_old'] = price_old

                if price < price_old:
                    # Используем целочисленное деление (//), чтобы не получить,
                    # что-нибудь типа 33.3333333333
                    offer_dict['rebate_amount'] = (price_old - price) * 100 // price_old

        self.container[self.OFFERS].append(offer_dict)

    # В случае, если в продукте сохранено описание только одного ценового предложения,
    # добавить дополнительный атрибут, содержащий данные о свойствах товара, например,
    # о цвете
    def append_extras_to_offer(self, extra):

        if self.container.get(self.OFFERS) is None:
            return

        if len(self.container[self.OFFERS]) != 1:
            return

        self.container[self.OFFERS][0][self.EXTRA] = extra

    # Функция возвращает первое значение указанного свойства из блока Offers.
    # Issue #145.
    def get_first_property_from_offer(self, prop_name):
        """
        :param prop_name: Имя свойства, обычно - "Артикул"
        :type prop_name: str 
        :return: str
        """

        if self.container.get(self.OFFERS) is not None:
            if len(self.container[self.OFFERS]) == 1:
                if self.container[self.OFFERS][0].get(self.PROPERTIES) is not None:

                    props = self.container[self.OFFERS][0][self.PROPERTIES]
                    if props.get(prop_name) is not None:
                        return props[prop_name]
        return None

    # Функция возвращает количество ценовых предложений в контейнере
    def offers_count(self):
        if self.container.get(self.OFFERS) is None:
            return 0
        return len(self.container[self.OFFERS])

    # Функция устанавливает свойство товара в указанное значение.
    def append_property(self, name, value):
        if self.container.get(self.PROPERTIES) is None:
            self.container[self.PROPERTIES] = {}

        # Если в имени свойства встречаются фигурные скобки, то не приводим
        # значение этого свойства к верхнему регистру, поскольку этот
        # особый случай значит, что в свойстве перечисляются вариации
        # товара и ссылки на отдельные вариации. См. Issue #137
        if value.find('{') >= 0 and value.find('}') >= 0:
            self.container[self.PROPERTIES][name] = value
        else:
            self.container[self.PROPERTIES][name] = value.upper()

    def delete_property_in_offer(self, name):
        if self.container.get(self.OFFERS) is not None:
            if len(self.container[self.OFFERS]) > 0:
                if self.container[self.OFFERS][0].get(self.PROPERTIES) is not None:
                    if self.container[self.OFFERS][0][self.PROPERTIES].get(name) is not None:
                        del self.container[self.OFFERS][0][self.PROPERTIES][name]

    # Функция устанавливает свойство товара в указанное значение.
    # ВНИМАНИЕ! Метод работает только в том случае, если в Offers
    # уже есть какие-то свойства. Данная операция модифицирует уже
    # сформированные данные, а не формирует новые данные
    def append_property_in_offer(self, name, value):
        if self.container.get(self.OFFERS) is not None:
            if len(self.container[self.OFFERS]) > 0:
                if self.container[self.OFFERS][0].get(self.PROPERTIES) is not None:
                    self.container[self.OFFERS][0][self.PROPERTIES][name] = value.upper()

    # Функция заменяет значения свойств на свойства из переданного словаря
    def replace_properties(self, props_dict):

        if self.container.get(self.OFFERS) is not None:
            if len(self.container[self.OFFERS]) > 0:
                if self.container[self.OFFERS][0].get(self.PROPERTIES) is not None:

                    for key in list(props_dict.keys()):
                        self.container[self.OFFERS][0][self.PROPERTIES][key] = props_dict[key]

    # Функция возвращает значение конкретного свойства товара
    def get_property(self, name):

        if self.container.get(self.PROPERTIES) is None:
            return None

        if self.container[self.PROPERTIES].get(name) is None:
            return None

        return self.container[self.PROPERTIES][name]

    # Извлекает дополнительные данные из описания указанного продукта и
    # добавляет его к своему описанию. Копируются: фотографии, ценовые
    # предложения
    def merge_from(self, source, ignore_duplicate=False):

        # Копируем названия товара в отдельный список - он будет
        # использоваться для отбора наиболее релевантного (чаще всего
        # встречающегося названия товара). Это действие не актуально
        # для товаров у которых название является частью составного ключа,
        # например - детские коляски (PRAM), но крайне важно для таких
        # категорий, как LEGO, в которых названия наборов каждый магазин
        # переводит, как хочет
        if source.container.get(self.SHORT_NAMES) is None:
            self.container[self.SHORT_NAMES] = []
            self.container[self.SHORT_NAMES].append(self.get_product_name())

        self.container[self.SHORT_NAMES].append(source.get_product_name())

        # Копируем ценовые предложения
        if source.container.get(self.OFFERS) is not None:

            if self.container.get(self.OFFERS) is None:
                self.container[self.OFFERS] = []
            else:
                # Issue #137. Проверяем, не происходит ли дублирование
                # товарного предложения. Такое возможно в случае, если
                # в разных товарах используются одни и те же вариации
                if ignore_duplicate:

                    if source.container[self.OFFERS][0].get('url'):

                        for offer in self.container[self.OFFERS]:
                            if offer.get('url') is None:
                                continue

                            # Считаем, что один и тот же url ведёт на
                            # тоже самое товарное предложение
                            if offer['url'] == source.container[self.OFFERS][0]['url']:
                                return

            # Добавляем товарное предложение в общий контейнер
            for offer in source.container[self.OFFERS]:
                self.container[self.OFFERS].append(offer)

        # Копируем строки с описанием товаров, включая бренд, название, и т.д.
        if source.container.get(self.EXCESSIVE_NAMES) is not None:

            if self.container.get(self.EXCESSIVE_NAMES) is None:
                self.container[self.EXCESSIVE_NAMES] = []

            for photo in source.container[self.EXCESSIVE_NAMES]:
                self.container[self.EXCESSIVE_NAMES].append(photo)

    # Функция возвращает логическое значение (true, или false), в
    # зависимости от того, содержит ли описание товара вариации,
    # отличные от указанной. См. Issue #145
    def has_any_variations(self, exclude_variation=None):
        """
        :param str exclude_variation: Имя вариации, которую следует проигнорировать
        :return: bool 
        """

        if self.container.get(self.PROPERTIES) is not None:

            for prop_name in self.container[self.PROPERTIES]:

                if prop_name[-1:] != '_':
                    continue

                if exclude_variation is not None:
                    if exclude_variation == prop_name[:-1]:
                        continue

                return True

        return False

    # Функция корректирует название свойств, обеспечивая разделение
    # свойств и вариаций
    def fix_issue118(self, unified_variations):

        unified_variations_upper = [s.upper() for s in unified_variations]

        # Корректируем названия свойств таким образом, чтобы убрать
        # у вариаций подчёркивание, а к свойствам, которые близки
        # к вариациям, в конце добавить пробел
        if self.container.get(self.OFFERS) is not None:
            if len(self.container[self.OFFERS]) > 0:
                if self.container[self.OFFERS][0].get(self.PROPERTIES) is not None:

                    props = self.container[self.OFFERS][0][self.PROPERTIES]

                    # Свойство товара, извлёчённое из страниц с детальной
                    # информацией может быть продублировано вариацией, извлечённой
                    # из названия товара. Необходимо устранить дубликаты.
                    # В действительности, значения вариации и свойства могут
                    # даже быть разными - приоритет за вариацией.
                    # Здесь мы выделяем вариации установленные для конкретного товара
                    variations = []
                    for prop_name in props:
                        if prop_name[-1:] == '_':
                            variations.append(prop_name[:-1].upper())

                    # Выполняем цикл по всем свойствам товара
                    modified_props = {}
                    for prop_name in props:

                        if prop_name[-1:] == '_':
                            # Если свойство является вариацией, то удаляем завершающий
                            # имя свойства символ 'underscore'
                            modified_props[prop_name[:-1]] = props[prop_name]
                        else:

                            # Если свойство называется так же, как и вариации, но
                            # вариацией не вляется, то:
                            # - если вариация с таким же именем существует, то
                            #   игнорируем свойство
                            # - если вариация с таким же именем НЕ существует, то
                            #   в конце имени свойства добавляем пробел, для
                            #   того, чтобы товар не попал в группировку по данному
                            #   свойству
                            if prop_name.upper() in unified_variations_upper:
                                if prop_name.upper() not in variations:
                                    modified_props[prop_name + ' '] = props[prop_name]
                            else:
                                modified_props[prop_name] = props[prop_name]

                    # Заменяем старый список свойств на обновлённый
                    del self.container[self.OFFERS][0][self.PROPERTIES]
                    self.container[self.OFFERS][0][self.PROPERTIES] = modified_props
        pass

    # Функция корректирует оригинальный url-товара
    def fix_url(self, shop_url, url_to_substitute=None):

        if self.container.get(self.OFFERS) is None:
            return

        if len(self.container[self.OFFERS]) != 1:
            return

        if url_to_substitute is not None:

            # Issue #137. В случае, если осуществляется создание
            # копий товара для хранения вариаций, осуществляем
            # помену URL
            self.container[self.OFFERS][0]['url'] = ScrapingHelper.concat_path(shop_url, url_to_substitute)

        else:
            relative_url = self.container[self.OFFERS][0]['url']
            self.container[self.OFFERS][0]['url'] = ScrapingHelper.concat_path(shop_url, relative_url)

    # Функция корректирует url-фотографий товаров
    def fix_photos(self, shop_url):

        if self.container.get(self.PHOTOS) is not None:
            for i, photo in enumerate(self.container[self.PHOTOS]):
                self.container[self.PHOTOS][i] = ScrapingHelper.concat_path(shop_url, photo)

        # Корректируем ссылки на фотографии внутри ценовых предложений
        if self.container.get(self.OFFERS) is None:
            return

        if len(self.container[self.OFFERS]) != 1:
            return

        offer = self.container[self.OFFERS][0]
        if offer.get(self.PHOTOS) is None:
            return

        for i, photo_url in enumerate(offer[self.PHOTOS]):
            offer[self.PHOTOS][i] = ScrapingHelper.concat_path(shop_url, photo_url)

    # Функция осуществляет корректировку названия товара, с учётом доступности
    # типа товара и бренда в описании товара
    @staticmethod
    def correct_model_name(product_type, brand, real_name, model_name):
        """
        :param str product_type: тип товара (например, "Велосипед")
        :param brand: бренд
        :param real_name: фактическое название товара, без бренда и типа, но с дополнительными атрибутами
        :param model_name: обработанное название товара, без бренда и типа
        :return str
        """

        # В случае, если не удалось выделить модель из названия товара,
        # а так же отсутствует бренд, или тип продукта, то используем
        # полное название товара для формирования индекса. Это
        # сделано для того, чтобы избержать группировки товаров, например,
        # только по бренду, или только по типу товара
        if (brand is None or product_type is None) and model_name is None:

            model_name = real_name

            # Удаляем из полного названия товара бренд и тип продукта
            if brand is not None:
                model_name = model_name.replace(brand, '').strip()

            if product_type is not None:
                model_name = model_name.replace(product_type, '').strip()

        return model_name
