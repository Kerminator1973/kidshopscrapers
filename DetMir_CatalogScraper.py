#!/usr/bin/python
# -*- coding: utf-8

import re
import json
import copy
import time
from time import localtime, strftime
import requests
import logging
from bs4 import BeautifulSoup
from ProductDescription import ProductContainer         # Класс, используемый для объединения информации о товаре
import ScrapingHelper                                   # Набор вспомогательных функций
import FilterCollection                                 # Класс-агрегатор атрибутов отбора данных
from multiprocessing.dummy import Pool as ThreadPool    # API поддержки много-поточности в Python-приложении
from Dochkisinochki_CatalogScraper import ProductData


# Функция асинхронной загузки html-страниц (и вспомогательных json-документов).
# Может быть использована для работы в Threading Pool
def async_download_html_page_dm(product):

    product_url = product[1]
    url = DetMirCatalogScraper.get_shop_site() + product_url

    try:

        # Загружаем основную страницу товара
        session = requests.Session()
        response = session.get(url)
        if response.status_code != requests.codes.ok:
            logging.error('Status-code отличается от [200]: {}'.format(response.status_code))
            return ProductData(product[0], product[1], None)

        html_page = response.text

        # Загружаем дополнительную страницу товара (с количеством товара
        # на складе)
        json_code = None

        # Выделяем идентификатор товара в магазине. Это число, с количеством
        # элементов от 4 до 8
        str_id = re.findall(R'index/id/\d{4,8}/', product_url)
        if len(str_id) > 0:

            pos = product_url.find(R'id/')
            if pos >= 0:
                shop_product_id = product_url[pos + 3: -1]

                # Формируем http-запрос
                try:
                    # Формируем запрос страницы с информацией о доступности товара
                    url = DetMirCatalogScraper.get_shop_site() + R'/api/rest/2.0/products/?id=' + \
                          shop_product_id + '&region=RU-MOW'
                    json_page = session.get(url).text
                    json_code = json.loads(json_page)

                except requests.exceptions.RequestException as e:
                    logging.error('Не удалось получить информацию о доступности товара: {}'.format(e))
                except ValueError:
                    logging.error('Ошибка при обработке документа доступности товара')

        return ProductData(product[0], product[1], html_page, json_code)

    except requests.exceptions.RequestException as e:
        logging.error('Сбой в сетевой подсистеме:', e)

    return ProductData(product[0], product[1], None)


class DetMirCatalogScraper:

    BRAND = 'brand'
    SHORT_NAMES = 'short_names'

    def __init__(self, database_helper, options):

        # Сохраняем ссылку на класс работы с базой данных. Запоминаем
        # настройки, полученные от пользователя класса
        self.db = database_helper
        self.so = options

        # Указываем название магазина, с которым должны быть связаны все операции
        # в базе данных, связанные с категориями магазина "Детский Мир"
        database_helper.activate_shop('DetMir')

        # Создаём объёкт-сессию, через который будет осуществляться загрузка
        # страниц магазина "Детский мир". Использование библиотеки
        # Requests позволяет активировать режим "Keep-ALive", который ускоряет
        # получение информации с сайта магазина
        self.session = requests.Session()

        # Создаём ассоциативный контейнер (dictionary), в котором будем хранить связь между
        # идентификатором товарной категории (есть у каждого раздела) и "date-id" - идентификатором
        # родительской категории. Особенность магазина "Детский мир" такова, что по "data-id"
        # категории нижнего уровня (drop_menu_lvl_2) привязываются к родительским категориям
        self.parents_id = {}

    @staticmethod
    def get_shop_name():
        return 'Детский Мир'

    @staticmethod
    def get_shop_site():
        return 'http://www.detmir.ru'

    # Public-функция осуществляет загрузку каталога товарных групп магазина "Детский Мир".
    # Параметр filter_name содержит название блока правил, которыми нужно руководствоваться
    # при извлечении данных. Таким правильном может быть, например, извлекать информацию
    # только о наборах LEGO.
    def scrape_catalog(self, filter_name):

        # При каждом запуске скрипта импортируем категории товаров. Это не занимает
        # много времени, но зато список категорий всегда находится в актуальном
        # состоянии.

        # Главное меню магазина "Детский мир" содержит пять основных разделов
        # и около 70 подразделов. Отдельные подразделы содержат ссылки на
        # страницы с описанием товаров
        self._scrape_categories()

        # Осуществляем попытку получения данных по накопленным в базе
        # данных категориям.
        # Внимание! Вызов _scrape_the_showcase() осуществляется через
        # лямбда-функцию по одной простой причине - чтобы явно обозначить
        # значение параметров callback-функции, которая будет вызываться
        # при обработке. Если бы я передавал в качестве параметра только
        # имя callback-функции, то для того, чтобы понять, что получает
        # на входе callback-функция, пришлось бы вычитывать состав полей
        # SQL-запроса в классе ScrapingDatabase.

        self.db.process_categories(
            lambda rowid, url: self._scrape_the_showcase(rowid, url),
            self.so.scrape_categories_to_process,
            additional_condition=self._get_condition_by_name(filter_name))

        # Если установлен параметр извлечения дополнительной информации по ранее
        # полученным данным, осуществляем обработку страниц отдельных товаров.
        # Мы там ищем: постоянные ссылки на фотографии товаров, расширенный
        # список свойств для последующей классификации
        if self.so.scrape_details_by_count > 0:

            print("Формируются URL-адреса для обработки: {}".format(strftime("%H:%M:%S", localtime())))

            # Используем лямбда-функцию, чтобы собрать список product_id и URL
            # для их последующей обработки
            product_url_list = []
            self.db.process_each_product(
                lambda product_id, product_url: product_url_list.append((product_id, product_url)),
                self.so.scrape_details_by_count,
                additional_condition=self._get_condition_by_name(filter_name), commit_periodically=False)

            part_count = 100
            begin_index = 0
            while True:

                print("Загрузка html-страниц с {} по {}. Время: {}".format(begin_index + 1, begin_index + part_count,
                                                                           strftime("%H:%M:%S", localtime())))

                partial_urls_list = product_url_list[begin_index:begin_index + part_count]

                # Формируем pool рабочий потоков, задачей которых будет являться
                # загрузка html-страниц с данными товара
                pool = ThreadPool(4)

                # Результаты работы будут накапливаться в отдельном списке
                results = pool.map(async_download_html_page_dm, partial_urls_list)

                # Устанавливаем флаг завершения работы и ждём, когда
                # все рабочие потоки завершат свою работу
                pool.close()
                pool.join()

                print("Обработка: {}".format(strftime("%H:%M:%S", localtime())))

                for product_data in results:
                    if product_data.product_page is not None:
                        self._scrape_details(product_data.product_id, product_data.product_url,
                                             product_data.product_page, product_data.json_code)
                self.db.connection.commit()

                # Переходим к обработке следующего блока данных
                begin_index += part_count

                # Если все диапазоны URL-адресов обработаны, то завершаем работу
                if begin_index >= len(product_url_list):
                    break

            print("Извлечение деталей товаров завершено: {}".format(strftime("%H:%M:%S", localtime())))

    # Public-функция осуществляет извлечение дополнительных атрибутов
    # из ранее накопленной информации
    def extract_attributes(self, filter_name):

        if "LEGO" == filter_name:

            # Осуществляем обработку ранее извлечённых данных о товарах магазина. В общем случае,
            # обработка предполагает извлечение дополнительных атрибутов из названия товара
            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id_unused, cat_name, prod_id, prod_name:
                self._extract_lego_attrs(cat_name, prod_id, prod_name))
        else:

            brands = []

            filters = FilterCollection.Filter(self.get_shop_name(), filter_name)
            category_name = filters.get_category_name()
            if category_name is not None:

                # Получаем URL-страницы на которой в блоке атрибутов поиска должны находится
                # все добавленные бренды. Нам необходимо извлечь эти бренды
                cat_id = self.db.get_caterory_id_by_name(category_name)
                if cat_id is not None:

                    category_url = self.db.get_category_url('cat_id = {}'.format(cat_id))
                    brands = self._extract_brands(category_url)
                    if brands is None:
                        logging.error('Ошибка: не удалось выделить названия брендов - производителей товаров')

                        # Не смотря на ошибку, продолжаем работу алгоритма, поскольку
                        # дальше по коду можно загрузить полный список брендов - это
                        # хуже по качеству работы алгоритма, но лучше, чем вообще
                        # отказ от выделения свойств
                        pass

            # Если загрузить бренды связанные с конкретной категорией не удалось, то
            # используем страницу со списком всех доступных брендов (http://www.detmir.ru/brands/)
            if brands is None or len(brands) == 0:
                brands = self._extract_all_brands()

            # Загружаем дополнительный справочник названий моделей
            trademarks = self.db.get_brands_and_trademarks()

            # Issue #119. Определяем список вариаций применяемых к конкретной
            # категории товаров.
            valuable_props = filters.get_variations_priority()
            if valuable_props is None:
                valuable_props = self.db.get_unified_variations()

            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id_unused, cat_name, prod_id, prod_name:
                    self._extract_common_attrs(cat_name.upper(), brands, prod_id, prod_name.upper(),
                                               trademarks, valuable_props))
        pass

    # Private-функция формирует фильтр, использующийся для извлечения данных
    # по названию соответствующего правила. Например, правило может называться
    # "LEGO" и эта функция сформирует SQL-условие, которое позволит отобрать
    # только LEGO-категории
    def _get_condition_by_name(self, filter_name):

        # todo: вариант "ALL" нужно отрабатывать в FilterCollection
        if "ALL" == filter_name:
            return ""   # Отбирать всё!

        else:

            filters = FilterCollection.Filter(self.get_shop_name(), filter_name)
            return filters.get_conditions(self.db)

    # Метод извлекает список всех брендов которые продаёт "Детский мир"
    def _extract_all_brands(self):

        brands = []

        try:

            # Формируем адрес страницы для извлечения брендов
            url = self.get_shop_site() + '/brands/'
            html_document = self.session.get(url).text
            soup = BeautifulSoup(html_document, 'html.parser')

            filter_list = soup.find('div', attrs={'class': 'brand_list_all'})
            if filter_list is None:
                return None

            for filter_element in filter_list.findAll('a'):

                if filter_element.find('li') is not None:
                    brand_string = filter_element.getText().strip()
                    brands.append(brand_string.strip().upper() + ' ')

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме:', e)
            return None

        return brands

    # Метод извлекает список брендов, по которым можно отбирать данные
    def _extract_brands(self, category_url):

        brands = []

        try:

            # Формируем адрес страницы для извлечения брендов
            html_document = self.session.get(category_url).text
            soup = BeautifulSoup(html_document, 'html.parser')

            filter_list = soup.find('div', attrs={'class': 'filter_brand'})
            if filter_list is None:
                return None

            for filter_element in filter_list.findAll('label', attrs={'for': True}):

                brand_string = filter_element.getText().strip()
                left_brace = brand_string.find('(')
                brand = brand_string[:left_brace].strip().upper()
                brands.append(ScrapingHelper.normalize_brand(brand))

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме:', e)
            return None

        return brands

    # Private-функция осуществляет загрузку главного меню и извлечение из него
    # информации о товарных категориях
    def _scrape_categories(self):
        try:

            # Извлечь ссылки на категории можно, практически, с любой страницы
            url_to_start = "http://www.detmir.ru/catalog/index/name/igry_i_igrushki/"
            html_document = self.session.get(url_to_start).text
            soup = BeautifulSoup(html_document, "html.parser")

            start_time = time.time()

            main_menu = soup.find('div', attrs={'class': 'main_menu'})
            if main_menu is None:
                logging.critical('Главное меню магазина "Детский Мир" не найдено')
                exit()

            for top_menu_element in main_menu.findAll('div', attrs={'class': 'main_menu_item_wrap'}):

                # На самом верхнем уровне находятся описания главных родительских категорий. Мы
                # извлекаем их, по большей части, только с целью упрощения дальнейшей классификации -
                # данные по полученным ссылкам не обрабатываются, но идентификатор родительской
                # группы используется для формирования цепочки (списка) родительских элементов,
                # который, опять же, накапливается с целью дальнейшего упрощения классификации

                link_and_text = top_menu_element.find('a', attrs={'class': 'topmenu'})
                if link_and_text is None or not link_and_text.has_attr('href'):
                    continue

                parent_id = self.db.insert_category(
                    link_and_text.getText().strip(), link_and_text['href'], '0')

                # Внутри каждой родительской категории находятся ссылки на элементы меню первого уровня.
                # Эти категории могут ссылаться либо сразу на витрину товаров, либо на отдельный
                # список подкатегорий второго уровня (drop_menu_lvl_2). Связывание элементов
                # категорий первого уровня с категориями второго уровня осуществляется по полю "data-id"
                for sub_menu_element in top_menu_element.findAll('li'):

                    if not sub_menu_element.has_attr('data-id'):
                        continue

                    data_id = sub_menu_element['data-id']

                    link_and_text = sub_menu_element.find('a', attrs={'href': True})
                    if link_and_text is not None:

                        href = link_and_text['href']

                        # Игнорируем ссылки, в которых есть подстрока "/actions/item/", т.к. это,
                        # скорее всего - реклама
                        if re.match('.*/actions/item/.*', href) is not None:

                            message = 'Обнаружена подкатегория похожая на рекламу: {}. Игнорируется!'.format(href)
                            logging.error(message)

                        else:

                            category_id = self.db.insert_category(link_and_text.getText().strip(), href, str(parent_id))

                            # Запоминаем связь между 'data-id' и идентификатором категории в базе данных.
                            # Эта связь может потребоваться при классификации категорий товаров
                            self.parents_id[data_id] = category_id

            print("Выделение главных категорий: {}".format(int(round((time.time() - start_time) * 1000))))

            # Третий этап - извлечение категорий второго уровня (drop_menu_lvl_2)
            start_time = time.time()
            self._scrape_second_level_categories(soup)
            print("Выделение подкатегорий: {}".format(int(round((time.time() - start_time) * 1000))))

            '''# УСТАРЕВШАЯ РЕАЛИЗАЦИЯ - Удалить 18.06.2017 г.
            # Третий этап - извлечение категорий второго уровня (drop_menu_lvl_2), которые хранятся
            # отдельно во внешних JSON-файлах. Посколько описание категорий нижнего уровня являются
            # статическими документами, то их кэширование снижает нагрузку на сетевую инфраструктуру.
            # По странной причине, собственные URL-ссылки на категории второго уровня устанавливаются
            # в заглушки (с названием категории) в соответствующих статических файлах. Например,
            # в js-файле для наборов "barbie", у всех категорий второго уровня, URL-ссылки на наборы
            # "Барби" будут совпадать с текстовым названием категории второго уровня. По этой причине,
            # обрабатывать приходится два различных js-файла, чтобы они друг друга взаимодополняли.

            self._scrape_references_to_categories("lego")
            self._scrape_references_to_categories("barbie")'''

        except requests.exceptions.RequestException as e:
            logging.error('Критичный сбой в сетевой подсистеме:', e)
            exit()

    # Функция извлекает информацию о подкатегориях второго уровня
    def _scrape_second_level_categories(self, soup):

        parent_keys = list(self.parents_id.keys())
        for subcategories in soup.findAll('ul', attrs={'class': 'drop_menu_list_lvl_2'}):
            for class_name in subcategories['class']:

                if 'ic' != class_name[:2]:
                    continue

                key = class_name[2:]
                if key not in parent_keys:
                    continue

                # Получаем список полных родительских категорий
                materialized_path = self.db.get_materialized_path(self.parents_id[key])

                # Сообщаем родительской категории, что у неё есть дочерние категории
                self.db.detmir_establish_category_paternity(self.parents_id[key])

                for element in subcategories.findAll('a', attrs={'href': True}):

                    if not element.has_attr('title'):
                        logging.error('У подкатегории не найдено название')
                        continue

                    title = element['title']
                    href = element['href']

                    # Как было описано выше, в функции _scrape_categories, JSON-данные
                    # могут хранить фиктивные ссылки на витрину товаров в том случае, если
                    # название категории и имени статического файла совпадают. Например,
                    # для "barbie", в поле URL-ссылок на дочерние категории второго уровня будут
                    # содержать название подкатегории, а не ссылку. Подобные случаи следует
                    # отсекать
                    if "http://" in href or "https://" in href:
                        self.db.insert_category(title.strip(), href, materialized_path)
        pass

    '''
    # УСТАРЕВШАЯ РЕАЛИЗАЦИЯ - Удалить 18.06.2017 г.
    # Private-функция осуществляет загрузку дочерних категорий второго уровня
    def _scrape_references_to_categories(self, subgroup):

        try:
            # URL для запроса JSON-файлов с описанием категорий находится во внешнем файле и выглядит
            # следующим образом:
            #   "http://static.detmir.ru/upload/export/menu_" + [суб-домен] + ".js" +
            #   + getFullYear() + getDate() + getHour()
            json_url = "http://static.detmir.ru/upload/export/menu_" + subgroup + ".js"
            json_code = self.session.get(json_url).text
            categories = json.loads(json_code)

            if "cat3" in categories:

                # Словарь (dictionary) в JSON-объекте "cat3" содержит ключ, который
                # соответствует "data-id" элементов в главном меню, а значением
                # является ещё один словарь, который содержит пару "url" +
                # "название категории"
                for data_id, subcategories in categories["cat3"].items():

                    if data_id not in self.parents_id.keys():
                        message = 'Отсутствует связь категорий первого и второго уровня для {}'.format(data_id)
                        logging.error(message)

                        for href, title in subcategories.items():

                            message = 'Внесистемная категория: {} - {}'.format(title, href)
                            logging.warning(message)

                        # Поскольку связь между элементами отсутствует, то игнорируем данную
                        # категорию. Возможно мы имеем дело с какой-то вне системной
                        # подкатегорией, типа "пылесосы, утюги и отпареватели"
                        continue

                    # Получаем список полных родительских категорий
                    materialized_path = self.db.get_materialized_path(self.parents_id[data_id])

                    # Сообщаем родительской категории, что у неё есть дочерние категории
                    self.db.detmir_establish_category_paternity(self.parents_id[data_id])

                    # Добавляем все дочерние категории второго уровня (drop_menu_lvl_2) в базу данных
                    for href, title in subcategories.items():

                        # Как было описано выше, в функции _scrape_categories, JSON-данные
                        # могут хранить фиктивные ссылки на витрину товаров в том случае, если
                        # название категории и имени статического файла совпадают. Например,
                        # для "barbie", в поле URL-ссылок на дочерние категории второго уровня будут
                        # содержать название подкатегории, а не ссылку. Подобные случаи следует
                        # отсекать

                        if "http://" in href:
                            self.db.insert_category(title.strip(), href, materialized_path)
            else:
                logging.error('В категориях отсутствует ключевой элемент "cat3"')

        except requests.exceptions.RequestException as e:
            logging.error('Критичный сбой в сетевой подсистеме:', e)
        '''

    # Private-функция выполняющая задачу получения данных по одной
    # товарной категории. Способ отображения информации о товарах
    # разумно назвать "витриной" (a showcase)
    def _scrape_the_showcase(self, category_id, category_url):

        # Сайт "Детского Мира" содержит несколько витрин, которые
        # отличаются друг от друга по способу получения информации
        # о товарах.
        # 1ый подход: описание товаров описываются прямо в html;
        # 2ой подход: описание товаров нужно извлекать посредством AJAX.

        # Изначально мы не знаем, какой из подходов должен быть использован
        # и пробуем 1ый подход.
        logging.info('url = {}'.format(category_url))

        try:

            # ОПТИМИЗАЦИЯ ПО СКОРОСТИ: указываем максимальное количество
            # товаров на одной странице [/per_page/80/]. Обязательно
            # проверяем наличие завершающего slash-символа при конкатенации
            # строк по правилам RESTful
            url = category_url
            if url[-1:] != '/':
                url += '/'
            url += 'per_page/80/'

            html_document = self.session.get(url).text
            soup = BeautifulSoup(html_document, "html.parser")

            result = soup.find('h1', attrs={'class': 'catalog_caption'})
            if result is not None:

                # Выводим контрольную информацию в консоль (для визуальной проверки)
                logging.debug('check group name = {}'.format(result.getText()))

                # Обрабатываем текущую страницу
                self.db.begin_products_insertion()
                self._collect_products(html_document, category_id)
                self.db.end_products_insertion(ignore_old_price=True)

                # Если на странице есть навигационная панель, то обрабатываем
                # все страницы, на которые есть ссылки в навигационной панели.
                # ВНИМАНИЕ! Этот вариант не очень надёжный, т.к. работает
                # только в том случае, если в paginator(е) расположены все
                # ссылки данной товарной категории
                paginator = soup.find('div', attrs={'class': 'paginator'})
                if paginator is not None:

                    pages = paginator.findAll('a', attrs={'class': 'p-item'})
                    if len(pages) > 0:
                        for page in pages:
                            if page['href'] is not None:

                                # ВАЖНЫЙ МОМЕНТ: склейка url-а главной страницы
                                # товарной категории и локального пути ресурса
                                # может быть не простой - следует сохранять
                                # аккуратность
                                server_url_pos = category_url.find('www.detmir.ru/')
                                if server_url_pos > -1:
                                    link = category_url[:server_url_pos + 13] + page['href']
                                else:
                                    link = category_url + page['href']

                                html_document = self.session.get(link).text

                                self.db.begin_products_insertion()
                                self._collect_products(html_document, category_id)
                                self.db.end_products_insertion(ignore_old_price=True)
            else:
                # ПРЕДПОЛОЖЕНИЕ:
                # В случае, если на странице <h1 class='catalog_caption'>,
                # т.е. признак наличия информации о товарах, пробуем получить эту
                # информацию альтернативным способом - используя JSON.
                # Нюанс работы магазина "Детский Мир" состоит в том, что
                # он аггрегирует множество моно-брендовых магазинов под
                # одной вывеской. Часть магазинов работает по разным технологиям.
                # Если типовой подход - HTML, то альтернативный - HTML с загрузкой
                # витрины посредством JSON.
                # Соответственно, если по URL сайт вернул HTML документ без
                # товаров, то пробуем загрузить ту же информацию, но используя
                # JSON
                self.db.begin_products_insertion()
                self._scrape_the_showcase_by_json(category_id, category_url)
                self.db.end_products_insertion(ignore_old_price=True)

        except requests.exceptions.RequestException as e:
            logging.error('Ошибка получения данных о витрине магазина: ', e)
            return

    # Private-функция осуществляет попытку собрать данные по товарной
    # категории используя AJAX (обработка JSON ответа сервера)
    def _scrape_the_showcase_by_json(self, category_id, category_url):

        # Извлечение данных с использованием AJAX требует формирования
        # определённых адресов извлекаемых данных. В частности,
        # необходимо добавлять к category_url дополнительный путь,
        # который может выглядеть как /[domain]/index/ajax-catalog/

        # Выделяем домен, который используется для формирования AJAX-запроса
        domain_start = category_url.find(':')
        if domain_start > 0:
            domain_start += 3

        domain_end = category_url.find('.')
        domain = category_url[domain_start:domain_end]

        url = category_url + '/' + domain + '/index' + '/ajax-catalog/'
        logging.debug('Выделен домен: {}, запрос: {}'.format(domain, url))

        # Для обеспечения сессионности, начинаем выполнять запросы в рамках
        # отдельной клиентской сессии: получаем cookie при первом обращении
        # к сайту, а потом передаём полученные cookie при каждом последующем
        # обращении к сайту
        page = 1
        while True:

            json_code = self.session.post(url, data={'page': page}, headers={'X-Requested-With': 'XMLHttpRequest'})
            if not self._extract_products_from_json(category_id, json_code.text):
                break

            page += 1

        pass

    def _extract_products_from_json(self, category_id, json_code):

        try:

            results = json.loads(json_code)

            # Online-магазин "Детский Мир" представляет собой достаточно комплексную и
            # не целостную сущность. Витрина может быть представлена полностью законченным
            # набором HTML-документов, а может использовать JSON для ускорения своей работы.
            # При этом формат данных, представленных в JSON может быть либо простым, либо
            # расширенным и включать в себя дополнительную иерархию, включающую поля
            # 'status' и 'body'.

            if 'status' in results:

                # Разбираем комплексную структуру с полями 'status' и 'body'

                if results['status'] != 'OK':
                    logging.debug('Код завершения запроса: {}'.format(results['status']))
                    return False

                if 'body' in results:
                    body = results['body']

                    # В поле body['count'] содержится информация о количестве товарных
                    # позиций в категории

                    if 'catalog' in body:

                        count = 0
                        try:
                            # Разбираем страницу так, как будто мы загрузили обычный html
                            count = self._collect_products(body['catalog'], category_id)

                        except TypeError:
                            # Сохраняем информацию об ошибке разбора json-кода
                            message = 'Ошибка разбора json-кода (Детский мир): {}'.format(json_code)
                            logging.error(message)

                        # Если в выборке были какие-то данные, то продолжаем выполнять
                        # сбор данных в товарной категории
                        return count > 0

            else:

                # Разбираем упрощённую структуру с полями 'catalog' и 'count'

                if 'count' in results:
                    if int(results['count']) > 0:
                        if 'catalog' in results:
                            count = self._collect_products(results['catalog'], category_id)

                            # Если в выборке были какие-то данные, то продолжаем выполнять
                            # сбор данных в товарной категории
                            return count > 0

                    # В отдельных категориях может и не быть товаров
                    return False

                logging.error('Изменилась структура JSON-данных')

        except ValueError:
            message = 'Ошибка при обработке JSON-кода. Получены данные: {}'.format(json_code)
            logging.error(message)

        return False

    # Private-функция осуществляет сборку описания товарных позиций из
    # переданного ей на вход html-блока (это может быть лишь часть документа)
    def _collect_products(self, html, category_id):

        # При сборе данных будет фиксировать, сколько товарных позиций было
        # добавлено в коллекцию
        collection_count = 0

        soup = BeautifulSoup(html, "html.parser")
        goods_block = soup.find('div', attrs={'class': 'goods_prod_list'})
        if goods_block is None:
            return 0

        for good in goods_block.findAll('div', attrs={'itemprop': 'itemListElement'}):

            inner_block = good.find('div', attrs={'class': 'caption'})
            if inner_block is not None:
                tag_name = inner_block.find('a')
                if tag_name is not None:

                    # Выделяем название набора и артикул
                    url_details = ''
                    if tag_name['href'] is not None:
                        url_details = tag_name['href']

                    # Цена описывается в отдельном тэге
                    price_tag = good.find('span', attrs={'itemprop': 'price'})
                    if price_tag is not None:

                        price = price_tag.getText().strip().split(' ')
                        if price is not None:

                            # TODO: необходимо сохранять стоимость товара в копейках во всех
                            # магазинах. Преобразовывать сумму в удобную для просмотра нужно
                            # либо при формировании JSON-экспорта, либо при формировании
                            # пользовательской формы

                            # Причина по которой используется доступ по индексу - price[0]
                            # состоит в вызове split(' '), который позволяет отбросить
                            # лишние символы, такие как "РУБ."
                            price_int = ScrapingHelper.parse_amount(price[0], '.')

                            # Цену без применения скидки не указываем, т.к. в верстке сайта
                            # вместо неё указывается округлённый процент. Реальная цена
                            # без скидки есть на странице с детальной информацией
                            self.db.insert_product(category_id, tag_name.getText(), price_int, url_details,
                                                   price_old=self.db.IGNORE_OLD_PRICE)
                            collection_count += 1

        # Завершая работу функции, возвращаем количество добавленных товарных позиций
        return collection_count

    # Private-функция разбора страницы отдельного товара
    def _scrape_details(self, product_id, product_url, html_document, json_code=None):
        """
        :param int product_id: идентификатор товара в базе данных
        :param str product_url: ссылка на загруженную страницу (нужна для логирования)
        :param str html_document: страница для обработки
        :param object json_code: JSON-документ сконвертированный в объект. Это дополнительная информация
        """

        # Удаляем ранее извлечённые свойства товара
        self.db.delete_properties_and_photos(product_id)

        # Сохранять свойства будем через специальный кэширующий механизм
        self.db.begin_props_insertion()

        success = False
        try:

            # Сначала, по косвенным признакам, проверяем, есть ли в
            # полученном документе ценный контент. Для этого осуществляется
            # поиск специальных маркеров.
            stop_processing = False

            last_meta = html_document.rfind('<link rel="')
            start_body = html_document.find('<div class="body">')

            if last_meta < 0:

                stop_processing = True
                logging.info("Отсутствует meta-блок : {}".format(product_url))

            else:

                if start_body < 0:

                    stop_processing = True
                    logging.info("Структура страницы не корректная : {}".format(product_url))

                else:

                    meta_soup = BeautifulSoup(html_document[:last_meta], 'html.parser')

                    # Issue #78. Проверяем, а не помеченна ли ссылка магазином, как не работающая?
                    title = meta_soup.find('title')
                    if title is not None:
                        if title.getText().find('страница не существует') >= 0:

                            stop_processing = True
                            logging.info("Сайт сообщил, что страница не существует: {}".format(product_url))

                            # Помечаем страницу как outdated и уже обработанную
                            self.db.mark_as_outdated(product_url, execute_now=True)

            # Если структура страница не допускает корректную обработку, то завершаем работу
            if stop_processing:
                self.db.add_photo_of_product(product_id, product_url, successful=False)
                self.db.end_props_insertion()
                return

            # На сайте "Детского мира" много полезной информации хранится непосредственно
            # в META-тегах, в области заголовка документа (до тэга </head>).
            # Можно не бояться дубликатов при выполнении SQL-запроса сохранения
            # списка изображений - функция add_photo_of_product() предотвращает
            # подобные ситуации
            for image_property in meta_soup.findAll('meta', attrs={'name': 'og:image'}):
                if image_property.has_attr('content'):

                    # Сохраняем ссылку на оригинальное изображение товара в базе данных.
                    # Поскольку мы не знаем, какое из изображений главное, сохраняем
                    # их так, как будто все они вспомогательные
                    success = self.db.add_photo_of_product(product_id, image_property['content'], True, 0)

            # Удаляем из обрабатываемого документа область с отзывами пользователей и
            # другой подобной информацией - она критически влияет на время обработки данных
            review_block = html_document.find('<div class="wrapper-sliders-product">')
            if review_block > start_body:

                reduced_html = html_document[start_body:review_block]

                accessibility_form = html_document.find('<form name="accessiblityForm">')
                if accessibility_form > 0 and accessibility_form > review_block:
                    reduced_html += html_document[accessibility_form:]

            else:

                reduced_html = html_document[start_body:]

            # Теперь обрабатываем данные из ограниченного подмножества HTML-документа
            soup = BeautifulSoup(reduced_html, 'html.parser')

            # Альтернативный способ извлечения информации о главной фотографии,
            # а так же ссылок на дополнительные фотографии товара
            all_images_block = soup.find('div', attrs={'class': 'car_big_img'})
            if all_images_block is not None:

                images = all_images_block.findAll('img', attrs={'src': True})
                for image in images:

                    # В случае, если выделить основную фотографию из МЕТА-тэгов
                    # не удалось, то пробуем выделить её из списка масштабируемых
                    # фотографий
                    if image.has_attr('class'):
                        if 'img1' in image['class']:
                            if not success:
                                success = self.db.add_photo_of_product(product_id, image['src'])
                        else:

                            # Добавляем дополнительные фотографии в базу данных с особенным
                            # признаком - 0 (не основное изображение товара)
                            self.db.add_photo_of_product(product_id, image['src'], is_main=0)

            # Пытаемся определить цену товара до применения скидок
            old_price = soup.find('div', attrs={'class': 'old_price'})
            if old_price is not None:

                amount_text = old_price.getText().strip()
                if len(amount_text) > 0:

                    old_price_int = ScrapingHelper.parse_amount(amount_text, ',')
                    if old_price_int > 0:
                        self.db.update_product_old_price(product_id, old_price_int)

            # Далее пытаемся выделить свойства продукта и сохранить их
            # в отдельную таблицу.

            # Отдельное свойство - внутренний номер товаров, он может быть полезен для сбора
            # сведения обновленных данных о ценах товара, когда уже не нужно обрабатывать
            # сайт магазина в полном объёме
            internal_id = soup.find('span', attrs={'id': 'product-internal-id'})
            if internal_id is not None:

                # Предполагаем, что мы находимся в блоке со значением
                # свойств и выделяем текст, связанных с этим свойством
                property_value = internal_id.getText().strip()
                if len(property_value) > 0:

                    self.db.include_prop(product_id, 'internal_id', property_value)

            # Свойства товара находятся в очень странном месте - в ячейке
            # таблицы с классом "ijh"
            properties_block = soup.find('td', attrs={'class': 'ijh'})
            if properties_block is not None:

                # Каждое из свойств является текстом в отдельном параграфе
                properties = properties_block.findAll('p')
                for prop in properties:

                    # Разделяем текст на название свойства и его значение
                    prop_text = prop.getText()
                    separator_pos = prop_text.index(':')
                    if separator_pos < 0:
                        continue

                    prop_name = prop_text[0:separator_pos].strip()
                    prop_value = prop_text[separator_pos + 1:].strip()

                    # Сохраняем значение свойства в базе данных
                    if len(prop_name) > 0 and len(prop_value) > 0:
                        self.db.include_prop(product_id, prop_name, prop_value)

            # Пытаемся извлечь "хлебные крошки" (breadcrumb), которые позволяют
            # улучшить классификацию товара
            breadcrumb = ""

            # В новом варианте дизайна, каждый элемент breadcrumb имеющий
            # дочерние элементы, представляет собой выпадающий список
            for block in soup.findAll('div', attrs={'itemscope': True, 'itemtype': True}):
                if 'HTTP://DATA-VOCABULARY.ORG/BREADCRUMB' == block['itemtype'].upper():
                    breadcrumb_href = block.find('a', attrs={'href': True})
                    if breadcrumb_href is not None:
                        breadcrumb = self._add_breadcrumb(breadcrumb, breadcrumb_href)

            # В старом варианте дизайна, все элементы breadcrumb находятся
            # в одном span с атрибутом itemprop И значением title
            for block in soup.findAll('span', attrs={'itemprop': 'title'}):
                breadcrumb = self._add_breadcrumb(breadcrumb, block)

            if len(breadcrumb) > 0:
                self.db.include_prop(product_id, 'breadcrumb', breadcrumb)

            # Сохраняем "детальную информацию о товаре" в базе данных в HTML-формате,
            # с тем, чтобы можно было использовать существующую HTML-разметку
            description_block = soup.find('div', attrs={'class': 'g_block_body'})
            if description_block is not None:

                wrapper = description_block.find('div', attrs={'itemprop': 'description'})
                if wrapper is not None:

                    # Для того, чтобы убрать тэг 'div', в который обёрнут значимый для
                    # покупателя текст, выполняем операцию the list comprehension
                    [x.extract() for x in wrapper.findAll('div', attrs={'itemprop': 'description'})]
                    partial_html = ''.join(str(c) for c in wrapper.contents)
                    self.db.update_product_description(product_id, partial_html)

            # Issue #143. Выделяем идентификатор товара и получаем отдельным
            # запросом информацию о доступности товара в интернет-магазине
            if json_code is not None:
                instock = self._get_instock_info(json_code)
                self.db.update_instock_status(product_id, instock)

            # Issue #116. Осуществляем поиск специфического класса:
            # "prod_card_feature_column-2" и выделяем из каждого такого
            # блока значимые вариации товара.
            # Для информации: пример названия свойства - 'select_clothes_size'
            variants_lists = soup.findAll('div', attrs={'class': 'prod_card_feature_column-2'})
            for variants in variants_lists:

                if not variants.has_attr('id'):
                    continue

                properties = []
                for property_value in variants.findAll('div', attrs={'class': 'item'}):
                    variation_name = property_value.getText().strip()
                    if len(variation_name) == 0:
                        continue

                    property_href = ''
                    if property_value.has_attr('optionproducturl'):
                        property_href = property_value['optionproducturl']

                    product_variation = variation_name
                    product_variation += '{'
                    product_variation += property_href
                    product_variation += '}'

                    properties.append(product_variation)

                if len(properties) > 0:
                    self.db.include_prop(product_id, variants['id'], ';'.join(properties))

        except requests.exceptions.RequestException as e:
            logging.error('Не удалось загрузить информацию с атрибутами товара: {}'.format(e))
            self.db.connection.commit()

        except ValueError:
            logging.error('Ошибка при обработке страницы свойств товара')
            self.db.connection.commit()

        # Если по каким-то причинам выделить ссылку на изображение не удалось, сохраняем
        # информацию об этом в базе данных для последующего разбора. Так же важно исключить
        # эту запись из повторной обработки, т.е. добавив ссылку на Products.rowid
        # из поля product_id таблицы Photos
        if not success:
            self.db.add_photo_of_product(product_id, product_url, successful=False)

        # Осуществляем пакетную запись в базу данных всех свойств сразу
        self.db.end_props_insertion()

    # Метод выполняет запрос в магазин, с целью получения информации
    # о доступности товара на складе
    @staticmethod
    def _get_instock_info(product):
        """
        :param str json_code: Python-объект, полученный в результате обработки JSON-документа, 
            содержащего информацию о доступности товара  
        :return int: 0 - нет нигде, 1 - есть везде, 2 - есть только offline. 
        """

        # Issue #143. Не анализируем количество товара (HIGH/MEDIUM/LOW),
        # а только факт его наличия, либо отсутствия
        online = False
        offline = False

        # JSON-пакет, на верхнем уровне, содержит массив из одного элемента,
        # в котором должен быть объект "quantity"
        if len(product) > 0 and "quantity" in product[0]:

            quantity = product[0]["quantity"]

            if "online" in quantity:
                if len(quantity['online']) > 0 and "quantity" in quantity['online'][0]:
                    online = True

            if "offline" in quantity:
                if len(quantity['offline']) > 0 and "quantity" in quantity['offline'][0]:
                    offline = True

        if online and offline:
            return 1

        elif (not online) and offline:
            return 2

        # Если товара отсутствует в интернет-магазине, то считаем, что его нет

        # Если в процессе работы алгоритма что-то пошло не так, то считаем,
        # что товар отсутствует в магазине
        return 0

    # Вспомогательная функция, добавляет очередную хлебную крошку в контейнер
    @staticmethod
    def _add_breadcrumb(breadcrumb, soup_element):

        name = soup_element.getText().strip().upper()
        if "ГЛАВНАЯ" != name:
            if name not in breadcrumb:
                if len(breadcrumb) > 0:
                    breadcrumb += "/"
                breadcrumb += name

        return breadcrumb

    # Private-функция извлекает дополнительные атрибуты из названия продукта
    def _extract_lego_attrs(self, category_name, product_id, product_name):

        # Если название товара начинается со слова "Конструктор", то исключаем
        # его из названия
        prefix = "Конструктор"
        if 0 == product_name.find(prefix):
            product_name = product_name[len(prefix):].strip()

        # Если дальше идёт название серии наборов, то исключаем его из названия
        if 0 == product_name.find(category_name):
            product_name = product_name[len(category_name):].strip()

        # Далее разделяем оставшуюся часть на отдельные слова и ищем среди
        # них артикул, который является строкой, состоящий не менее, чем из пяти
        # цифр, среди которых может быть знак "минус"
        short_product_name = ""

        collect_words = False
        product_words = product_name.split(' ')
        for word in product_words:

            # Пустые слова пропускаем. Эти пустые слова возникают из-за
            # ошибок в формировании строки названия товара в магазине
            if 0 == len(word):
                continue

            if collect_words:
                if len(short_product_name) > 0:
                    short_product_name += ' '

                short_product_name += word
                continue

            if ScrapingHelper.could_be_an_article(word):

                # Пытаемся сохранить артикул в базу данных - в дальнейшем,
                # это позволит нам не обрабатывать страницы с детальной
                # информацией по товару
                self.db.insert_property(product_id, 'article', word)
                collect_words = True

        if len(short_product_name) > 0:
            # Добавляем сокращённое название товара в базу данных
            self.db.insert_property(product_id, 'playset_name', short_product_name)

    # Private-функция извлекает дополнительные атрибуты из названия продукта
    def _extract_common_attrs(self, category_name, brands, product_id, product_name, trademarks, valuable_props):

        product_name = product_name.replace('  ', ' ')
        product_name = ScrapingHelper.normalize_brand(product_name)

        # Поскольку у продукта может быть сразу несколько свойств/атрибутов, то
        # имеет смысл пакетировать операции, т.е. объединять несколько INSERT-ов
        # в один. В MySQL подобная операция может быть выполнена посредством
        # конструкции INSERT INTO ... ON DUPLICATE KEY UPDATE
        self.db.begin_props_insertion()

        self.db.include_prop(product_id, 'category', category_name)

        for brand_index, brand in enumerate(brands):

            index = product_name.find(brand)
            if index > -1:

                # Встречаются случаи, в которых одно-символьный, или двух-символьный
                # бренд может полностью развалить разбор строки. В подобных случаях
                # необходимо обеспечивать выполнение условия - бренд это слово, или
                # несколько слов, отделённые от остального текста пробелами
                if index > 0 and product_name[index - 1] != ' ':
                    continue

                # В имени бренда хранится дополнительный пробел с правой стороны с тем,
                # чтобы уменьшить вероятность вместо бренда найти часть какого-нибудь слова
                brand = brand.strip()

                sub_category = product_name[:index].strip()
                if len(sub_category) > 0:
                    self.db.include_prop(product_id, 'product_type', sub_category)

                # Выделяем модель товара из названия, исключив тип товара и бренд
                short_name = product_name[index + len(brand):].strip()
                if len(short_name) > 0:

                    # Issue #71, #92. Осуществляем выделение свойств товаров с помощью
                    # унифицированного процесса
                    _short_name, results = ScrapingHelper.find_unified_attrs(short_name, self.db.unified_variations)
                    if results is not None and len(results) > 0:

                        # Issue #119. Если выделенная вариация не должна применяться
                        # к товару в указанной товарной категории, то сохраняем её
                        # в отдельную строку и включаем её, как часть модели
                        model_extension = self.db.insert_prop_variations(product_id, results, valuable_props)
                        if len(model_extension) > 0:
                            if len(_short_name) > 0:
                                _short_name += ' '
                            _short_name += model_extension

                    # Если название модели после выделения свойств изменилось, то
                    # сохраняем изменённое название в базе данных.
                    #
                    # TODO: Следующие ниже две строки кода не эффективны, но была
                    # какая-то здравая мысль, вероятно, потерянная среди
                    # модификаций кода
                    if _short_name != short_name:
                        short_name = _short_name

                    # Пытаемся выделить модель из строки описания товара, используя
                    # специализированный справочник
                    if trademarks.get(brand) is not None:
                        for model in trademarks[brand]:

                            pos = short_name.find(model)
                            if pos >= 0:
                                if len(short_name) > len(model):
                                    short_name = model
                                break

                    self.db.include_prop(product_id, 'short_name', short_name)

                # Сохраняем имя найденного бренда в базу данных
                self.db.include_prop(product_id, 'Бренд', brand)

                # OK. Мы нашли бренд и выделили атрибуты - всё отлично.
                # А теперь, чтобы ускорить процесс, перемещаем найденный бренд в начало
                # списка - это должно ускорить дальнейшую обработку данных
                if brand_index > 20:
                    brands.insert(0, brands.pop(brand_index))

                self.db.end_props_insertion()
                return

        # Если выделить сокращённое название товара не удалось, то добавляем
        # в базу данных полное название товара
        self.db.include_prop(product_id, 'short_name', product_name)
        self.db.end_props_insertion()

    # Public-функция, помещает в коллекцию append_to информацию о товарах
    # отобранных по фильтру filter_name, в иерархическом, структурированном виде
    def export_products(self, filter_name, append_to):

        # Выгружаем всю информацию по продуктам из базы данных по указанному фильтру
        if "LEGO" == filter_name:
            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id, cat_name, prod_id, prod_name: self._export_lego_playset(prod_id, append_to)
            )

        else:

            # Перед началом экспорта данных получаем полный список фотографий
            # и свойств для каждого товара в обрабатываемом магазине
            self.db.preload_data()

            # Issue #97. Если для указанной категории явным образом указывается
            # необходимость определять при экспорте каждый товар, как уникальный,
            # то изменяем соответствующую глобальную настройку --random_product_type
            old_random_product_type = self.so.random_product_type

            filter_obj = FilterCollection.Filter(self.get_shop_name(), filter_name)
            if filter_obj.is_unique_each_product():
                self.so.random_product_type = True

            # Issue #145. Формируем таблицу перекодировки артикулов
            complex_articles_set = self._prepare_articles_dict(filter_name)

            # Выгружаем данные в общем виде, без какой-либо особенной обработки
            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id, cat_name, prod_id, prod_name:
                    self._export_common_data(prod_id, append_to, complex_articles_set),
                include_outdated=True, cache_product_fields=True
            )

            self.so.random_product_type = old_random_product_type

            # Удаляем из памяти предварительно загруженные данные
            self.db.clear_preloaded_data()

        pass

    # Функция осуществляет подготовку словаря трансляции артикулов
    # для решения проблемы с группировкой товаров в ряде категорий.
    # См. Issue #145
    def _prepare_articles_dict(self, filter_name):
        """
        :param str filter_name: условное имя категории для которой формируются данные
        :return: set
        """

        articles_dict = {}
        self.db.for_each_product(
            self._get_condition_by_name(filter_name),
            lambda cat_id, cat_name, prod_id, prod_name: self._collect_article(prod_id, articles_dict),
            include_outdated=True, cache_product_fields=True
        )

        # См. комментарий к реализации алгоритма в "Кораблике"!
        unique_articles = []
        for key in list(articles_dict.keys()):
            if len(articles_dict[key]) > 1:
                for article in articles_dict[key]:
                    if article not in unique_articles:
                        unique_articles.append(key + '_' + article)

        return set(unique_articles)

    def _collect_article(self, product_id, articles_dict):

        # Извлекаем описание товара из базы данных
        product = ProductContainer(self.db, product_id, self.get_shop_name())

        # Ищем в свойствах товара внутренний артикул
        internal_article = product.get_first_property_from_offer('Артикул')
        if internal_article is None:
            return

        # Исключаем дополнительную обработку свойств товара, т.к.
        # для дальнейшей обработки используются только бренд, тип
        # продукта, название модели и внутренний артикул магазина
        self._update_product(product, modify_everything=False)

        # Подготовливаем параметры, которые будут использованы для формирования артикула
        brand = product.get_brand()

        product_type = product.get_property('product_type')
        real_name = product.get_product_name()
        model_name = product.get_property('short_name')
        model_name = ProductContainer.correct_model_name(product_type, brand, real_name, model_name)

        article = ScrapingHelper.gen_article(product_type, brand, model_name)

        if articles_dict.get(article) is None:
            articles_dict[article] = []

        else:

            # Добавляем в список только уникальные "внутренние артикулы"
            if internal_article in articles_dict[article]:
                return

        articles_dict[article].append(internal_article)

    # Private-функция включает данные об указанном товаре в контейнер append_to
    def _export_lego_playset(self, product_id, append_to):

        product = ProductContainer(self.db, product_id, self.get_shop_name())
        self._update_product(product, product_name_property='playset_name')

        article = product.get_property('article')
        if article is None:
            # Вполне может существовать ситуация, в которой у товара не будет
            # артикула, например: "Пенал LEGO с наполнением  Friends Jungle".
            # В этой ситуации имеет смысл создать случайный, отличный от других
            # идентификатор товара - в дальнейшем, такие товары можно будет
            # пытаться классифицировать посредством дополнительных алгоритмов
            article = "LEGO-" + ScrapingHelper.generator_random_string()

        # Добавляем информацию о товаре в коллекцию
        append_to.append(article, product)

    # Функция корректирует параметры товара "Детского мира". Часть параметров
    # магазина являются загрязнёнными, например название бренда полученной из
    # поля описания бренда содержит также и страну производителя, например:
    # "Inglesina (Испания)".
    #
    # Ещё одна проблема - название товара, которое при извлечении со страницы
    # сайта содержит: тип товара, бренд, название товара и ряд дополнительных
    # атрибутов. Соответственно, имеет смысл скорректировать данные, удалив
    # тип товара и название бренда.
    #
    # Третья проблема - оригинальный URL товара является относительным и не
    # содержит части 'http://www.detmir.ru'
    def _update_product(self, product, product_name_property='short_name', modify_everything=True):
        """
        :param product: объект, хранящий информацию о товаре
        :param str product_name_property: Имя свойств, хранящего сокращённое название продукта
        :param bool modify_everything: если False, то не осуществлять дополнительную обработку данных
        """

        # Корректируем название бренда
        brand_prop = product.get_property('Бренд')
        if brand_prop is not None:

            split_brand = ScrapingHelper.split_embraced_text(brand_prop)
            if len(split_brand) > 0:
                product.set_brand(split_brand[0])

        # Корректируем название продукта
        product_name = product.get_property(product_name_property)
        if product_name is not None and len(product_name) > 0:
            product.set_product_name(product_name)

        if modify_everything:

            # Issue #90. Расширяем "хлебные крошки" названием магазина, как
            # это сделано в "Кораблике". Важно понимать, что к этому моменту,
            # хлебные крошки уже были скопированы в список 'Offers' и нам
            # нужно работать именно с этим контейнером, а не с 'Properties'
            if product.container.get(product.OFFERS) is not None:
                if len(product.container[product.OFFERS]) == 1:
                    if product.container[product.OFFERS][0].get(product.PROPERTIES) is not None:

                        props = product.container[product.OFFERS][0][product.PROPERTIES]
                        if props.get('breadcrumb') is not None:
                            props['breadcrumb'] = self.get_shop_name().upper() + '/' + props['breadcrumb']

            # Issue #118. Корректируем названия свойств, добавляя к обычным
            # свойствам пробел в конце, а у вариаций убираем подчёркивание
            product.fix_issue118(self.db.unified_variations.keys())

            # Корректируем оригинальный URL товара
            product.fix_url(self.get_shop_site())

        pass

    def _export_common_data(self, product_id, append_to, complex_articles_set):

        product = ProductContainer(self.db, product_id, self.get_shop_name())
        self._update_product(product)

        brand = product.get_brand()

        # Issue #93. Если бренд товара начинается с символа не попадающего в указанный,
        # при запуске скрипта, диапазон, то не экспортируем товар
        if self.so.json_limit is not None and len(self.so.json_limit) == 2 and brand is not None and len(brand) > 0:
            if brand[0] < self.so.json_limit[0] or brand[0] > self.so.json_limit[0]:
                return

        self._append_data_to_export(product, product.get_product_name(), brand, append_to, complex_articles_set)

    def _append_data_to_export(self, product, real_name, brand, append_to, complex_articles_set):

        product_type = product.get_property('product_type')
        model_name = product.get_property('short_name')

        # Не обязательное действие - выделяем из названия товара - отдельно его свойства
        additional_attr = ScrapingHelper.get_difference(model_name, real_name)
        if additional_attr is not None and len(additional_attr) > 0:
            product.append_extras_to_offer(additional_attr)

        model_name = ProductContainer.correct_model_name(product_type, brand, real_name, model_name)

        # Формируем уникальный ключ товара, по которому будет осуществляться
        # упрощённая классификация товарных позиций
        article = ScrapingHelper.gen_article(product_type, brand, model_name)

        # Issue #145. Проверяем, можно ли использовать обобщённый артикул,
        # пригодный к группировке с товарами других магазинов, или нужно
        # применять композитный артикул, для того, чтобы предотвратить
        # ошибочную группировку разных товаров
        product_code = product.get_property('Артикул')
        if product_code is not None and len(product_code) > 0:

            complex_article = article + '_' + product_code
            if complex_article not in complex_articles_set:
                article = complex_article

            # Issue #160. Добавляем список альтернативных индексов товара
            product.set_alternative_index(product_code)

        # Issue #73. Если указан ключ экспорта "не группировать данные",
        # то подмешиваем к артикулу случайное значение
        if self.so.random_product_type:
            article += ScrapingHelper.generator_random_string()

        # Issue #116. Дублируем вариации товаров с подменой свойства
        # "select_clothes_size" на "Размер"
        clothes_size = product.get_property('select_clothes_size_id')
        if clothes_size is not None:

            product.delete_property_in_offer('select_clothes_size_id')

            for size_value in clothes_size.split(';'):

                # Issue #149. Описание вариации может содержать не только имя,
                # но и href на персональную страницу этого товара
                value = size_value
                href = None

                left_brace = size_value.find('{')
                right_brace = size_value.find('}')
                if 0 < left_brace < right_brace:

                    value = size_value[:left_brace]
                    href = size_value[left_brace + 1:right_brace]

                # Копируем товар перед включением в список, чтобы
                # избежать множественных ссылок на объект в коллекции
                # и, соответственно, зависания
                copy_of_product = copy.deepcopy(product)
                copy_of_product.append_property_in_offer('Размер', value)

                # Если описание вариации содержит URL, то подменяем URL
                # товара-вариации
                if href is not None:
                    copy_of_product.fix_url(self.get_shop_site(), href)

                # Добавляем информацию о товаре в коллекцию
                append_to.append(article, copy_of_product)
        else:

            # Добавляем информацию о товаре в коллекцию
            append_to.append(article, product)

    # Экспериментальный код, который может быть полезен как
    # инструмент формирования вспомогательных справочников в
    # автоматическом режиме
    def find_any_models(self, filter_name):

        # Перед началом экспорта данных получаем полный список фотографий
        # и свойств для каждого товара в обрабатываемом магазине
        self.db.preload_data(load_photos=False)

        # Ключевой задачей при экспорте данных во внешную систему является
        # создание комбинированного артикула, на основании которого можно
        # выполнить классификацию товаров, полученных из разных магазинов

        # Извлечение данных о колясках в "Детском мире" осуществляется таким образом:
        # 1) Данные классифицируются по свойствам: Бренд + Категория + Цена. Составной ключ
        #    получается, например, таким: "Baciuzzi-Коляска 2 в1-16990"
        # 2) В каждом товаре сравниваются названия наборов, которые могут выглядеть так:
        #    'Alum (coffee)', 'Alum (purple)', 'Alum (green)'
        #    Цель сравнения - отделить название товара, от дополнительных свойств,
        #    в частности - цвета (coffee, purple, green)
        # 3) Данные классифицируются по свойствам: Бренд + Категория + Название

        temporary_dict = {}

        # Выполняем классификацию по: Бренд + Категория + Цена
        self.db.for_each_product(
            self._get_condition_by_name(filter_name),
            lambda cat_id, cat_name, prod_id, prod_name: self._extract_models(prod_id, temporary_dict)
        )

        # Выделяем общую часть названия товара из списка товаров одного бренда,
        # одной категории, идущих по одной цене
        dictionary_of_names = ScrapingHelper.extract_dictionary_of_models(temporary_dict, self.BRAND, self.SHORT_NAMES)

        # Выводим на экран JSON-данные с найденными моделями товаров
        print('ДЕТСКИЙ МИР - mining названий моделей...\n')
        for brand in list(dictionary_of_names.keys()):
            models = ','.join(('"' + model + '"') for model in dictionary_of_names[brand])
            print('"{}": [\n\t{}\n],'.format(brand, models))

    # Private-функция выполняет классификацию товаров по схеме:
    # бренд + категория товара + цена
    def _extract_models(self, product_id, append_to):

        # Извлекаем описание товара из базы данных
        product = ProductContainer(self.db, product_id, self.get_shop_name())
        self._update_product(product)

        ScrapingHelper.group_products_by_price(product, append_to.get_products(), self.BRAND)

    # Исследовательская функция осуществляет импорт конкретного товара
    # из базы данных
    def import_special_details(self):
        pass
