﻿#!/usr/bin/python
# -*- coding: utf-8

import re
from time import localtime, strftime
import copy
import json
import logging
import requests
from bs4 import BeautifulSoup
from ProductDescription import ProductContainer  # Класс, используемый для объединения информации о товаре
import ScrapingHelper  # Набор вспомогательных функций
import FilterCollection  # Класс-агрегатор атрибутов отбора данных
from multiprocessing.dummy import Pool as ThreadPool    # API поддержки много-поточности в Python-приложении
from Dochkisinochki_CatalogScraper import ProductData


# Функция асинхронной загузки html-страниц. Может быть использована для
# работы в Threading Pool
def async_download_html_page_korablik(product):

    url = KorablikCatalogScraper.get_shop_site() + product[1]

    try:

        session = requests.Session()
        return ProductData(product[0], product[1], session.get(url).text)

    except requests.exceptions.RequestException as e:
        logging.error('Сбой в сетевой подсистеме:', e)

    # Возвращаем пустую страницу
    return ProductData(product[0], product[1], None)


class KorablikCatalogScraper:
    # Ссылка на раздел "Товары по коллекциям" - он выглядит как ссылка на описания
    # товаров, но на странице находятся дополнительные категории. По этой причине,
    # данную ссылку приходится обрабатывать специальным образом
    COLLECTIONS_HREF = '/catalog/collections'

    def __init__(self, database_helper, options):

        # Сохраняем ссылку на класс работы с базой данных. Запоминаем
        # настройки, полученные от пользователя класса
        self.db = database_helper
        self.so = options

        # Указываем название магазина, с которым должны быть связаны все операции
        # в базе данных, связанные с категориями магазина "Кораблик"
        database_helper.activate_shop('Korablik')

        # Создаём объёкт-сессию, через который будет осуществляться загрузка
        # страниц с сайта магазина "Кораблик". Использование библиотеки Requests
        # позволяет активировать режим "Keep-ALive", который ускоряет получение
        # информации с сайта магазина
        self.session = requests.Session()

        # Счётчик сброса cookies (см. Issue #102)
        self.clear_cookies_count = 0

    @staticmethod
    def get_shop_name():
        return 'Кораблик'

    @staticmethod
    def get_shop_site():
        return 'http://www.korablik.ru'

    # Public-функция осуществляет загрузку каталога товарных групп магазина "Кораблик"
    def scrape_catalog(self, filter_name):

        # При каждом запуске скрипта импортируем категории товаров. Это не занимает
        # много времени, но зато список категорий всегда находится в актуальном
        # состоянии
        self._scrape_categories()

        # Осуществляем попытку получения данных по всем накопленным в базе
        # данных категориям
        self.db.process_categories(
            lambda rowid, url: self._scrape_the_showcase(rowid, url),
            self.so.scrape_categories_to_process,
            additional_condition=self._get_condition_by_name(filter_name))

        # Если установлен параметр извлечения дополнительной информации по ранее
        # извлечённым данным, осуществляем обработку страниц отдельных товаров.
        # Мы там ищем: постоянные ссылки на фотографии товаров, расширенный
        # список свойств для последующей классификации

        if self.so.scrape_details_by_count > 0:

            print("Формируются URL-адреса для обработки: {}".format(strftime("%H:%M:%S", localtime())))

            # Используем лямбда-функцию, чтобы собрать список product_id и URL
            # для их последующей обработки
            product_url_list = []
            self.db.process_each_product(
                lambda product_id, product_url: product_url_list.append((product_id, product_url)),
                self.so.scrape_details_by_count,
                additional_condition=self._get_condition_by_name(filter_name), commit_periodically=False)

            part_count = 100
            begin_index = 0
            while True:

                print("Загрузка html-страниц с {} по {}. Время: {}".format(begin_index + 1, begin_index + part_count,
                                                                           strftime("%H:%M:%S", localtime())))

                partial_urls_list = product_url_list[begin_index:begin_index + part_count]

                # Формируем pool рабочий потоков, задачей которых будет являться
                # загрузка html-страниц с данными товара
                pool = ThreadPool(6)

                # Результаты работы будут накапливаться в отдельном списке
                results = pool.map(async_download_html_page_korablik, partial_urls_list)

                # Устанавливаем флаг завершения работы и ждём, когда
                # все рабочие потоки завершат свою работу
                pool.close()
                pool.join()

                print("Обработка: {}".format(strftime("%H:%M:%S", localtime())))

                for product_data in results:
                    if product_data.product_page is not None:
                        self._scrape_details(product_data.product_id, product_data.product_url,
                                             product_data.product_page)
                self.db.connection.commit()

                # Переходим к обработке следующего блока данных
                begin_index += part_count

                # Если все диапазоны URL-адресов обработаны, то завершаем работу
                if begin_index >= len(product_url_list):
                    break

            print("Извлечение деталей товаров завершено: {}".format(strftime("%H:%M:%S", localtime())))

    def extract_attributes(self, filter_name):

        if "LEGO" == filter_name:

            # Осуществляем обработку ранее извлечённых данных о товарах магазина. В общем случае,
            # обработка предполагает извлечение дополнительных атрибутов из названия товара
            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id, cat_name, prod_id, prod_name: self._extract_lego_attrs(prod_id, prod_name)
            )

        else:

            # Перед началом поиска вариаций в названии, получаем полный
            # свойств для каждого товара в обрабатываемом магазине
            self.db.preload_data(load_photos=False)

            # Загружаем дополнительный справочник названий моделей
            trademarks = self.db.get_brands_and_trademarks()

            # Issue #119. Определяем список вариаций применяемых к конкретной
            # категории товаров.
            filters = FilterCollection.Filter(self.get_shop_name(), filter_name)
            valuable_props = filters.get_variations_priority()
            if valuable_props is None:
                valuable_props = self.db.get_unified_variations()

            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id, cat_name, prod_id, prod_name: self._extract_common_attrs(prod_id, prod_name,
                                                                                        trademarks, valuable_props)
            )

    # Private-функция формирует фильтр, использующийся для извлечения данных
    # по названию соответствующего правила. Например, правило может называться
    # "LEGO" и эта функция сформирует SQL-условие, которое позволит отобрать
    # только LEGO-категории
    def _get_condition_by_name(self, filter_name):

        if "ALL" == filter_name:
            return ""  # Если фильтр не указан, то отбирать будем всё

        # В магазине "Кораблик" все LEGO-наборы находятся в одной категории
        elif "LEGO" == filter_name:
            return "Categories.group_name = 'Lego'"

        else:

            filters = FilterCollection.Filter(self.get_shop_name(), filter_name)
            return filters.get_conditions(self.db)

    # Private-функция осуществляет загрузку категорий из главного меню
    def _scrape_categories(self):
        try:
            catalog_collections_id = 0

            html_document = self.session.get(self.get_shop_site()).text
            soup = BeautifulSoup(html_document, "html.parser")

            border = soup.find('div', attrs={'class': 'header__menu-bx'})
            if border is None:
                return

            # Сначала обрабатываем меню верхнего уровня
            parent_ids_list = []
            for item in border.findAll('a', attrs={'class': 'opens'}):

                if not item.has_attr('href'):
                    continue

                category_name = item.getText().strip()
                category_href = item['href']

                # Игнорируем ссылки, которые не содержат подстроки /catalog/
                if re.match('^/catalog/.*', category_href) is not None:
                    group_id = self.db.insert_category(category_name, category_href, "0")
                    parent_ids_list.append(group_id)

            # Затем начинаем работу со списком подкатегорий
            index = 0
            for submenu_block in border.findAll('div', attrs={'class': 'header__menu-bx-submenu'}):

                # Выделяем все ссылки на подкатегории
                for item in submenu_block.findAll('a', attrs={'href': True}):

                    # Игнорируем ссылки из категории "Promotion", отличительным
                    # признаком которых является логотип бренда
                    if item.find('img') is not None:
                        continue

                    category_name = item.getText().strip()
                    category_href = item['href']

                    # Игнорируем ссылки, которые не содержат подстроки /catalog/
                    if re.match('^/catalog/.*', category_href) is None:

                        logging.info('Обнаружена аномальная категория: "{}". Игнорируется'.format(category_href))

                    else:

                        category_id = self.db.insert_category(category_name, category_href, parent_ids_list[index])

                        # Сохраняем идентификатор раздела "Товары по коллекциям"
                        if category_href == self.COLLECTIONS_HREF:
                            catalog_collections_id = category_id

                index += 1
                if len(parent_ids_list) <= index:
                    # Достигли списка брендов, которые оформлены как мусор - их нужно
                    # проигнорировать. Отбор брендов будет выполнен в методе scrape_brands()
                    break

            # Дополнительно обрабатываем раздел "Товары по коллекциям"
            if catalog_collections_id == 0:
                return

            html_document = self.session.get(self.get_shop_site() + self.COLLECTIONS_HREF).text
            soup = BeautifulSoup(html_document, "html.parser")

            for item in soup.findAll('a', attrs={'href': True}):

                category_name = item.getText().strip()
                category_href = item['href']

                # Игнорируем ссылки, которые не содержат подстроки /catalog/tag/
                if re.match('^/catalog/tag/.*', category_href) is not None:
                    self.db.insert_category(category_name, category_href, catalog_collections_id)

        except requests.exceptions.Timeout:
            logging.error('Сетевая подсистема. Превышен тайм-аут при ожидании ответа')
            self.db.connection.commit()
        except requests.exceptions.RequestException as e:
            logging.error('Сетевая подсистема. Катастрофический сбой в сетевой подсистеме: {}'.format(e))
            self.db.connection.commit()
            exit()

    # Private-функция выполняющая задачу получения данных по одной
    # товарной категории. Способ отображения информации о товарах
    # разумно назвать "витриной" (a showcase)
    def _scrape_the_showcase(self, category_id, category_url):

        # Игнорируем ссылку на "/catalog/collections", поскольку по этой ссылке находится
        # описание дополнительных категорий, а не товарных позиций. Эти категории извлекаются
        # в функции _scrape_main_menu()
        if self.COLLECTIONS_HREF == category_url:
            return

        # Для того, чтобы избежать рекурсии, заставляем метод разбора
        # отдельной страницы возвращать ссылку на следующую страницу
        current_page = category_url

        while True:

            url = self.get_shop_site() + current_page
            try:
                # Все страницы извлекаются в рамках одной сессии. Это должно
                # привести к увеличению скорости извлечения данных

                html = self.session.get(url)

                self.db.begin_products_insertion()
                current_page = self._scrape_the_page(category_id, html.text, url)
                self.db.end_products_insertion(update_product_name=False)

                if len(current_page) == 0:
                    break

            except requests.exceptions.RequestException as e:
                logging.error('Сбой в сетевой подсистеме: {}'.format(e))
                self.db.connection.commit()

    # Private-функция разбора отдельной страницы с товарами
    def _scrape_the_page(self, category_id, html, url_to_trace):

        soup = BeautifulSoup(html, 'html.parser')
        catalog = soup.find('div', attrs={'class': 'body__catalog'})
        if catalog is None:
            logging.error('На странице не найден каталог')
            return ""

        # Находим на странице javascript-код, который содержит дополнительную
        # информацию о товарах, включая полное название товара. Issue #124
        json_products = self._scrape_json_products(html)
        if json_products is None:
            logging.warning('На странице "{}" не найдено json-описание товаров'.format(url_to_trace))

        # С целью контроля эффективности извлечения данных
        # добавляем специальный счётчик
        count = 0

        all_products = catalog.findAll('div', attrs={'class': 'body__catalog-item'})
        for product in all_products:

            # Выделяем название набора
            good_name = product.find('a', attrs={'class': 'body__catalog-item_name'})
            if good_name is None:
                logging.error('Не найден заголовок body__catalog-item_name')
                continue

            name = good_name.getText()

            # Issue #124. Проверяем, возможно в JSON-коде содержится
            # уточнённое имя товара "Кораблика". Но проверяем его только
            # в том случае, если оно заканчивается на "...". К сожалению,
            # информация находящаяся в 'prid' так же может быть крайне
            # глючной (#127).
            # Для определения необходимости замены текста, в конце строки
            # осуществляется поиск символа "Horizontal Ellipsis" (U + 2026)
            if name[-1:] == '…':

                if product.has_attr('prid') is not None:
                    product_id = product['prid']

                    if json_products is not None:
                        if product_id in json_products:
                            name = json_products[product_id]

            name = name.replace('"', ' ').strip()

            # Выделяем URL
            if not good_name.has_attr("href"):
                logging.error('Не удалось выделить URL товара')
                continue

            url_details = good_name["href"]

            # Довольно часто, в магазине "Кораблик" используется tracking переходов
            # по ссылкам и всё, что идёт за символом "?" можно смело отсекать
            parts_of_url = url_details.split('?')
            if len(parts_of_url) > 1:
                url_details = parts_of_url[0]

            # Выделяем цену товара
            price_text = product.find('div', attrs={'class': 'body__catalog-item_price'})
            if price_text is None:
                logging.error('Не найдена тэг с ценой товара')
                continue

            # Удаляем из найденного блока все теги 'span' вместе с их содержимым
            [s.extract() for s in price_text.findAll('span')]
            price_int = ScrapingHelper.parse_amount(price_text.getText().strip())

            # Выделяем базовую цену товара (т.е. без применения скидки)
            old_price_split_list = []
            price_tag = product.find('span', attrs={'class': 'old-price'})
            if price_tag is not None:
                old_price_split_list = price_tag.getText().strip().split(' ')

            old_price_int = price_int
            if old_price_split_list is not None:
                if len(old_price_split_list) > 0:
                    old_price_int = ScrapingHelper.parse_amount(old_price_split_list[0], '.')

            # Добавляем товар в базу данных.
            # Issue #127. При обработке витрины не обновляем название товара.
            # Корректное имя товара содержится только на странице с детальной
            # информацией о товаре
            self.db.insert_product(category_id, name, price_int, url_details, price_old=old_price_int,
                                   update_product_name=False)
            count += 1

        # Теперь находим paginator и ссылку на следующий элемент
        paginator = soup.find('div', attrs={'class': 'paginator'})
        if paginator is not None:

            next_page = soup.find('a', attrs={'class': 'next_page'})
            if next_page is not None:

                if next_page.has_attr('data-noindex'):
                    logging.info("Извлечено данных: {}".format(count))

                    # Возвращаем ссылку для загрузки следующей таблицы paginator(a)
                    logging.info("Переходим на следующую страницу:" + next_page['data-noindex'])
                    return next_page['data-noindex']

        # Возвращаем пустую строку, т.к. переход на следующую страницу отсутствует
        logging.info("Извлечено данных: {}".format(count))
        return ""

    # Private-функция находит на странице javascript-код, который содержит
    # дополнительную информацию о товарах, включая полное название товара. Issue #124
    @staticmethod
    def _scrape_json_products(html):

        products = None

        last_position = 0

        # В разметке "Кораблика" может суцществовать несколько
        # блоков dataLayer.push({...});
        while True:

            begin = html.find('dataLayer.push({', last_position)
            if begin < 0:
                break

            end = html.find('});', begin)
            if end < 0:
                break

            last_position = end

            json_code = html[begin + len('dataLayer.push('):end + 1]

            try:
                struct = json.loads(json_code.replace("'", '"'))
                if struct.get('ecommerce') is not None:
                    if struct['ecommerce'].get('impressions') is not None:

                        for item in struct['ecommerce']['impressions']:

                            if item.get('id') is None or item.get('name') is None:
                                continue

                            if products is None:
                                products = {}

                            products[item['id']] = item['name']

            except ValueError:
                logging.error('Ошибка при обработке JSON-кода товаров (Кораблик)')

        return products

    # Private-функция разбора страницы отдельного товара
    def _scrape_details(self, product_id, product_url, html_document):
        """
        :param int product_id: идентификатор товара в базе данных
        :param str product_url: ссылка на загруженную страницу (нужна для логирования)
        :param str html_document: страница для обработки
        """

        # Удаляем ранее извлечённые свойства товара
        self.db.delete_properties_and_photos(product_id)

        # Сохранять свойства будем через специальный кэширующий механизм
        self.db.begin_props_insertion()

        success = False
        try:

            soup = BeautifulSoup(html_document, 'html.parser')

            # Issue #127. Выделяем название товара и сохраняем его в базе данных.
            # Применяется алгоритм пакетированного добавления данных (для повышения
            # скорости работы алгоритма)
            product_name = soup.find('h1', attrs={'class': 'page_title'})
            if product_name is not None:
                product_name = product_name.getText().strip()
                self.db.update_product_name_pack(product_id, product_name)

            # На сайте "Кораблика" используется т.н. "хлебные крошки" (breadcrumb)
            # которые можно использовать для выделения/уточнения подкатегории товара
            body_breadcrumb = soup.find('div', attrs={'class': 'body__breadcrumb'})
            if body_breadcrumb is not None:

                breadcrumb = ""
                for name in body_breadcrumb.findAll('a', attrs={'href': True}):

                    if len(breadcrumb) > 0:
                        breadcrumb += "/"
                    breadcrumb += name.getText().strip().upper()

                if len(breadcrumb) > 0:
                    self.db.include_prop(product_id, 'breadcrumb', breadcrumb)

            # Issue #143. Определяем, есть ли товар в наличии, или нет.
            # Ниже по коду применяется следующее правило "неведомой хрени":
            # если происходит что-то плохое (например, изменилась верстка),
            # то считаем, что товара в магазине нет. Это хорошее правило,
            # поскольку если верстка изменится, товар со страниц исчезнет,
            # мы быстро поймём проблему и исправим её в течение 1-2 дней.
            instock = 0
            offer_tags = soup.find('div', attrs={'itemprop': 'offers'})
            if offer_tags is not None:

                # Сначала мы пытаемся использовать чёткий признак наличия товара -
                # meta-тэг 'itemprop' со значением 'availability'.
                availability = offer_tags.find('meta', attrs={'itemprop': 'availability'})
                if availability is None:

                    # К сожалению, на практике этот признак не всегда присутствует.
                    # Дополнительно проверяем наличие кнопки "Добавить в корзину"
                    add_to_basket_tags = soup.find('div', attrs={'id': 'add_p_b'})
                    if add_to_basket_tags is not None:

                        if 'ДОБАВИТЬ В КОРЗИНУ' == add_to_basket_tags.getText().strip().upper():
                            instock = 1

                else:

                    # Если мета-тэг "доступности" есть в верстке, считаем, что товар есть в наличии
                    instock = 1

            # Вместе с тем, практика показывает, что meta-тэг может отсутствовать и тогда
            # необходимо ориентироваться на

            self.db.update_instock_status(product_id, instock)

            # Сохраняем "детальную информацию о товаре" в базе данных в HTML-формате,
            # с тем, чтобы можно было использовать существующую HTML-разметку

            wrapper = soup.find('div', attrs={'class': 'body__content_tabs-block_item'})
            if wrapper is not None:
                # Убираем из описания товара блок div-а с текстом "Описание", а так же IFRAME
                # с видео-материалами. Видео-материалы нужно отрабатывать отдельно
                [x.extract() for x in wrapper.findAll('div', attrs={'class': 'body__content_stat-title'})]
                [x.extract() for x in wrapper(['iframe'])]

                # Для того, чтобы убрать тэг 'div', в который обёрнут значимый для
                # покупателя текст, выполняем операцию the list comprehension
                partial_html = ''.join(str(c) for c in wrapper.contents)
                self.db.update_product_description(product_id, partial_html)

            # В магазине "Кораблик" используется т.н. "карусель", т.е. набор различных изображений
            # элементов. Находим "карусель" и добавляем ссылки на другие изображения товара
            carousel = soup.find('div', attrs={'class': 'c-carousel'})
            if carousel is None:
                self.db.end_props_insertion()
                logging.error('Ошибка при поиске класса c-carousel: {}'.format(product_url))
                return

            success = False

            preview_images = carousel.findAll('div', attrs={'class': 'goods-colors-item'})
            for preview_image in preview_images:

                is_main_image = 0

                # Пропускаем элемент с классом "active", т.к. его изображение мы уже получили
                # когда обрабатывали элемент большого размера
                if type(preview_image['class']) is list:
                    if 'active' in preview_image['class']:
                        is_main_image = 1

                image = preview_image.find('img', attrs={'style': True})
                if image is None:
                    continue

                # Нюанс: в HTML-коде сайта есть ошибки. В частности, в тэге, который описывает
                # изображение в области Preview есть дублирующиеся атрибуты 'src' и 'alt'.
                # По этой причине надёжнее обрабатывать атрибут style
                style_prepared = image['style'].split("'")
                if len(style_prepared) != 3:
                    continue

                # Сохраняем ссылку на вспомогательные изображения товара
                result = self.db.add_photo_of_product(product_id, style_prepared[1], is_main=is_main_image)
                if is_main_image:
                    success = result

            # Выделяем на странице отдельные свойства продукта
            props = soup.findAll('ul', attrs={'class': 'body__content_stat-list'})
            for properties in props:

                lines = properties.findAll('li')
                for one_line in lines:

                    fields = one_line.findAll('span')
                    if len(fields) < 2:
                        # Здесь я обрабатываю ошибку в верстке сайта "Кораблика" - в отдельных
                        # свойствах они забывают закрывать тэги <li> и <span>
                        logging.error('Ошибка: в описании свойства, количество полей меньше от двух')
                        continue

                    prop_name = fields[0].getText().strip()

                    # Сайт магазина "Кораблик" содержит множество ошибок в Html-верстке.
                    # В частности, они не завершают блок <li><span>Вес</span><span>...
                    # завершающими </span></li>.
                    # BeautifulSoup отрабатывает эту ситуации, в целом корректно, но
                    # в случае извлечения поля "Вес", он считает, что внутри второго <span>
                    # вложенного в <li> есть ещё один <span>, который содержит в себе
                    # уже заголовок другого поля ("Габариты"). Код ниже проверяет, есть
                    # ли фантомный вложенный <span> внутри второго поля

                    prop_value = ''
                    if fields[1].find('span') is None:

                        # Ниже идёт корректное поведение - вложенного фантомного <span>
                        # не найдено
                        prop_value = fields[1].getText().strip()

                    else:

                        # Найден фантомный <span> извлекаем нужные нам данные
                        # альтернативным способом - используя коллекцию contents
                        if len(fields[1].contents) > 0:
                            prop_value = str(fields[1].contents[0]).strip()

                            # Здесь можно было бы уведомить оператора об ошибке
                            # в верстке, но сообщений будет слишком много...
                            # print('Ошибка: в верстке свойства товара "{}"'.format(prop_name))

                    prop_value = prop_value.replace('\t', '')

                    # Сохраняем значение свойств в базе данных
                    if len(prop_name) > 0 and len(prop_value) > 0:
                        self.db.include_prop(product_id, prop_name, prop_value)

                    pass
                pass

            # Дополнительно выделяем свойство "Размер". Оно может существовать
            # на странице опционально
            variations_blocks = soup.findAll('div', attrs={'class': 'goods-standart'})
            for variations in variations_blocks:

                variation_name_block = variations.find('span', attrs={'class': 'goods-standart-title'})
                if variation_name_block is None:
                    continue

                # В имени свойства последним символом может быть ':'
                variation_name = variation_name_block.getText().strip()
                if len(variation_name) > 0 and variation_name[-1:] == ':':
                    variation_name = variation_name[:-1]

                # Issue #117. Постараемся собрать информацию о вариациях товара
                variations_list = None

                for variation in variations.findAll('span', attrs={'class': 'goods-standart-item'}):
                    if 'active' in variation['class']:

                        # Найдено свойство 'Размер' конкретного товара - нужно
                        # сохранить его в базу данных
                        self.db.include_prop(product_id, variation_name, variation.getText().strip())

                    href = variation.find('a', attrs={'href': True})
                    if href is not None:

                        if variations_list is None:
                            variations_list = []

                        # Issue #137. Сохраняем в отдельном свойстве доступные вариации
                        # товара с ссылками на каждый из них
                        product_variation = href.getText().strip()
                        product_variation += '{'
                        product_variation += href['href']
                        product_variation += '}'

                        variations_list.append(product_variation)

                # Сохраняем все вариации товара в отдельном свойстве под особенным именем
                if variations_list is not None:
                    self.db.include_prop(product_id, 'variations_list', ';'.join(variations_list))

        except requests.exceptions.RequestException as e:
            logging.error('Сбой в сетевой подсистеме: {}'.format(e))
            self.db.connection.commit()

        # Если по каким-то причинам выделить ссылку на изображение не удалось, сохраняем
        # информацию об этом в базе данных для последующего разбора. Так же важно исключить
        # эту запись из повторной обработки, т.е. добавив ссылку на Products.rowid
        # из поля product_id таблицы Photos
        if not success:
            self.db.add_photo_of_product(product_id, product_url, successful=False)

        # Осуществляем пакетную запись в базу данных всех свойств сразу
        self.db.end_props_insertion()

    # Private-функция извлекает дополнительные атрибуты из названия продукта
    def _extract_lego_attrs(self, product_id, product_name):

        # Если название товара начинается со слова "Конструктор", то исключаем
        # его из названия
        prefix = "Конструктор"
        if 0 == product_name.find(prefix):
            product_name = product_name[len(prefix):].strip()

        short_product_name = ""

        product_words = product_name.split(' ')

        collect = False
        for word in product_words:

            if len(word) == 0:
                continue

            # Артикул может быть указан в скобках - исключаем левую и правую скобки
            # из слова
            if len(word) > 0 and '(' == word[0]:
                word = word[1:]

            if len(word) > 0 and ')' == word[-1]:
                word = word[:-1]

            if ScrapingHelper.could_be_an_article(word):
                # Пытаемся сохранить артикул в базу данных - в дальнейшем,
                # это позволит нам не обрабатывать страницы с детальной
                # информацией по товару
                self.db.insert_property(product_id, "article", word)
                break

            # Если в слове есть хотя бы один кириллический символ, то
            # начинаем накапливать название набора. Предполагаем, что
            # серии наборов LEGO всегда приводятся на английском языке,
            # а название конкретного набора - на русском.
            # Для нахождения всех кириллических символов используем
            # регулярные выражения и Unicode-диапазон с кириллическими
            # символами
            cyrillic = re.findall(u"[\u0400-\u0500]+", word)
            if len(cyrillic) > 0:
                collect = True

            if collect:
                if len(short_product_name) > 0:
                    short_product_name += ' '

                short_product_name += word

            pass

        if len(short_product_name) > 0:
            # Добавляем сокращённое название товара в базу данных
            self.db.insert_property(product_id, "playset_name", short_product_name)

        pass

    # Private-функция извлекает дополнительные атрибуты из названия продукта
    def _extract_common_attrs(self, product_id, product_name, trademarks, valuable_props):

        product_name = product_name.replace('  ', ' ')
        product_name = ScrapingHelper.normalize_brand(product_name)

        # Сохранять свойства будем через специальный кэширующий механизм
        self.db.begin_props_insertion()

        brand = self.db.get_cached_property(product_id, 'Торговая марка')
        if brand is not None:

            # Строка с информацией о товаре идёт в следующей последовательности:
            # "[Тип товара] [Бренд] [Название товара]", например:
            # "Коляска прогулочная HAPPY BABY TWIGGY MARINE"

            brand = ScrapingHelper.normalize_brand(brand.upper())
            product_name = product_name.upper()

            # Исправляем ошибки в названии брендов в магазине "Кораблик". В частности,
            # в магазине могут произвольным образом использоваться зарегистрированный
            # польский бренд "BART-PLAST" и ошибка в виде "BARTPLAST"
            if product_name.find("BARTPLAST") >= 0:
                product_name = product_name.replace("BARTPLAST", "BART-PLAST")

            if "BARTPLAST" == brand:
                brand = "BART-PLAST"

            # Находим бренд в описании товара
            pos = product_name.find(brand)
            if pos >= 0:

                # Issue #98. В "Кораблике", товарная марка и бренд могут быть
                # совершенно разными значениями. Считаем, что бренд и товарная
                # марка товара совпадают, если товарная марка встречается в
                # названии товара
                self.db.include_prop(product_id, 'Бренд', brand)

                product_type = product_name[:pos].strip()
                if len(product_type) > 0:
                    self.db.include_prop(product_id, "product_type", product_type)
                else:

                    # Описание товара вполне может начинаться с бренда, а не с типа товара,
                    # например: "Pampers Premium Care 3 (5-9 кг) 120 шт."
                    # В этом случае, тип товара пытаемся выделить из хлебных
                    # крошек (breadcrumb).
                    # В общем виде, строка может выглядеть таким образом:
                    # "ДЕТСКИЕ ТОВАРЫ/ПОДГУЗНИКИ, ТРУСИКИ/PAMPERS/PAMPERS ACTIVE BABY"
                    # В случае группы товаров DIAPERS правильным было бы выделить
                    # кириллический текст "ПОДГУЗНИКИ, ТРУСИКИ", но это был бы
                    # не универсальный подход, который рекомендует выбрать
                    # самое значение из списка названий товара

                    breadcrumb = self.db.get_cached_property(product_id, 'breadcrumb')
                    if breadcrumb is None:

                        log_msg = "{}: не извлечены хлебные крошки (breadcrumb) - '{}'" \
                            .format(self.get_shop_name(), product_id)
                        logging.error(log_msg)

                    else:

                        words = breadcrumb.split('/')
                        if len(breadcrumb) > 0:

                            # В магазине "Кораблик", хлебные крошки могут содержать
                            # как название модели, так и бренд. Например:
                            #       ТРУСИКИ/PAMPERS/PAMPERS PANTS
                            # Ни первое, ни второе не соответствуют определению "тип изделия"
                            # и по этой причине, двигаясь с конца, мы ищем первое слово
                            # в котором отсутствует название бренда и принимаем его в качестве
                            # типа продукта
                            for word in reversed(words):
                                if brand not in word:
                                    self.db.include_prop(product_id, "product_type", word)
                                    break
                        else:

                            log_msg = "{}: не удалось определить тип товара - '{}'" \
                                .format(self.get_shop_name(), product_name)
                            logging.error(log_msg)

                short_name = product_name[pos + len(brand):].strip()

                # Issue #71, #92. Осуществляем выделение свойств товаров с помощью
                # унифицированного процесса
                short_name, results = ScrapingHelper.find_unified_attrs(short_name, self.db.unified_variations)
                if results is not None and len(results) > 0:

                    # Issue #119. Если выделенная вариация не должна применяться
                    # к товару в указанной товарной категории, то сохраняем её
                    # в отдельную строку и включаем её, как часть модели
                    model_extension = self.db.insert_prop_variations(product_id, results, valuable_props)
                    if len(model_extension) > 0:
                        if len(short_name) > 0:
                            short_name += ' '
                        short_name += model_extension

                # Пытаемся выделить модель из строки описания товара, используя
                # специализированный справочник
                if trademarks.get(brand) is not None:
                    for model in trademarks[brand]:

                        pos = short_name.find(model)
                        if pos >= 0:
                            if len(short_name) > len(model):
                                short_name = model
                            break

                # Сохраняем название модели в базе данных
                self.db.include_prop(product_id, 'short_name', short_name)

                # Осуществляем пакетную запись в базу данных всех свойств сразу
                self.db.end_props_insertion()

                # Все данные выделены, можно переходить к работе с другим товарам
                return

            else:

                # Issue #98. В "Кораблике" часто возникают ситуации, товарная марка
                # и бренд друг другу не соответствуют. Например:
                # 'Яйцо-трансформер EggStars Star Wars в асортименте', у которого
                # брендом указан 'Bandai'. Чтобы избежать ошибок при формировании
                # названия товара при экспорте, явно указывает, что у данного товара
                # нет бренда (хотя есть товарная марка)

                self.db.include_prop(product_id, 'Бренд', '')
                self.db.include_prop(product_id, 'short_name', product_name)
                self.db.end_props_insertion()

                log_msg = "{}: Бренд '{}' не найден в названии - '{}'".format(self.get_shop_name(), brand, product_name)
                logging.info(log_msg)

        else:
            log_msg = "{}: Бренд не известен - '{}'".format(self.get_shop_name(), product_name)
            logging.error(log_msg)

    # Public-функция, помещает в коллекцию append_to информацию о товарах
    # отобранных по фильтру filter_name, в иерархическом, структурированном виде
    def export_products(self, filter_name, append_to):

        # Выгружаем всю информацию по продуктам из базы данных по указанному фильтру
        if "LEGO" == filter_name:
            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id, cat_name, prod_id, prod_name: self._export_lego_playset(prod_id, append_to)
            )

        else:

            # Перед началом экспорта данных получаем полный список фотографий
            # и свойств для каждого товара в обрабатываемом магазине
            self.db.preload_data()

            # Issue #97. См. комментарий в scraper-е "Детского мира"
            old_random_product_type = self.so.random_product_type

            filter_obj = FilterCollection.Filter(self.get_shop_name(), filter_name)
            if filter_obj.is_unique_each_product():
                self.so.random_product_type = True

            # Issue #145. Формируем таблицу перекодировки артикулов
            complex_articles_set = self._prepare_articles_dict(filter_name)

            self.db.for_each_product(
                self._get_condition_by_name(filter_name),
                lambda cat_id, cat_name, prod_id, prod_name:
                    self._export_products(prod_id, append_to, complex_articles_set),
                include_outdated=True, cache_product_fields=True
            )

            self.so.random_product_type = old_random_product_type

            # Удаляем из памяти предварительно загруженные данные
            self.db.clear_preloaded_data()

        pass

    # Функция осуществляет подготовку словаря трансляции артикулов
    # для решения проблемы с группировкой товаров в ряде категорий.
    # См. Issue #145
    def _prepare_articles_dict(self, filter_name):
        """
        :param str filter_name: условное имя категории для которой формируются данные
        :return: set
        """

        articles_dict = {}
        self.db.for_each_product(
            self._get_condition_by_name(filter_name),
            lambda cat_id, cat_name, prod_id, prod_name: self._collect_article(prod_id, articles_dict),
            include_outdated=True, cache_product_fields=True
        )

        # В этом месте в словаре articles_dict есть все артикулы,
        # с разделением по "внутренним артикулам".

        # По сути, ниже мы делаем таблицу исключений для того, чтобы
        # сохранять у товаров, у которых вариации имеют разные артикулы
        # производителя, группировку по "тип товара + бренд + название товара".
        # Если этого не делать, то товарные предложения с вариациями не
        # будут попадать в одну группу, фактически, создавая независимые группы,
        # что является алгоритмической ошибкой
        unique_articles = []
        for key in list(articles_dict.keys()):
            if len(articles_dict[key]) > 1:
                for article in articles_dict[key]:
                    if article not in unique_articles:
                        unique_articles.append(key + '_' + article)

        return set(unique_articles)

    # Функция включает в словарь трансляции артикулов товар в том случае,
    # если у товара есть артикул производителя.
    def _collect_article(self, product_id, articles_dict):
        """
        :param str product_id: символический идентификатор товара
        :param dict articles_dict: словарь трансляции артикулов
        """

        # Извлекаем описание товара из базы данных
        product = ProductContainer(self.db, product_id, self.get_shop_name())

        # Ищем в свойствах товара внутренний артикул
        internal_article = product.get_first_property_from_offer('Артикул производителя')
        if internal_article is None:
            return

        # Исключаем дополнительную обработку свойств товара, т.к.
        # для дальнейшей обработки используются только бренд, тип
        # продукта, название модели и внутренний артикул магазина
        self._update_product(product, modify_everything=False)

        # Подготовливаем параметры, которые будут использованы для формирования артикула
        brand = product.get_brand()

        product_type = product.get_property('product_type')
        real_name = product.get_product_name()
        model_name = product.get_property('short_name')
        model_name = ProductContainer.correct_model_name(product_type, brand, real_name, model_name)

        article = ScrapingHelper.gen_article(product_type, brand, model_name)

        if articles_dict.get(article) is None:
            articles_dict[article] = []

        else:

            # Добавляем в список только уникальные "внутренние артикулы"
            if internal_article in articles_dict[article]:
                return

        articles_dict[article].append(internal_article)

    # Private-функция включает данные об указанном товаре в контейнер append_to
    def _export_lego_playset(self, product_id, append_to):

        product = ProductContainer(self.db, product_id, self.get_shop_name())
        self._update_product(product, product_name_property='playset_name')

        article = product.get_property('article')
        if article is None:
            # Для товаров из каталога "Кораблика" не всегда удаётся найти в названии товара
            # артикул. В подобных случаях, используется артикул, найденный среди детальной
            # информации о товаре (свойство - 'Артикул производителя')
            article = product.get_property('Артикул производителя')

        if article is None:
            article = "LEGO-" + ScrapingHelper.generator_random_string()

        # Добавляем информацию о товаре в коллекцию
        append_to.append(article, product)

    # Private-функция включает данные об указанном товаре в контейнер append_to
    def _export_products(self, product_id, append_to, complex_articles_set):

        product = ProductContainer(self.db, product_id, self.get_shop_name())
        self._update_product(product)

        brand = product.get_brand()

        # Issue #93. Если бренд товара начинается с символа не попадающего в указанный,
        # при запуске скрипта, диапазон, то не экспортируем товар
        if self.so.json_limit is not None and len(self.so.json_limit) == 2 and brand is not None and len(brand) > 0:
            if brand[0] < self.so.json_limit[0] or brand[0] > self.so.json_limit[0]:
                return

        product_type = product.get_property('product_type')
        real_name = product.get_product_name()
        model_name = product.get_property('short_name')
        model_name = ProductContainer.correct_model_name(product_type, brand, real_name, model_name)

        # Формируем уникальный ключ товара, по которому будет осуществляться
        # упрощённая классификация товарных позиций
        article = ScrapingHelper.gen_article(product_type, brand, model_name)

        # Issue #145. Проверяем, можно ли использовать обобщённый артикул,
        # пригодный к группировке с товарами других магазинов, или нужно
        # применять композитный артикул, для того, чтобы предотвратить
        # ошибочную группировку разных товаров
        product_code = product.get_property('Артикул производителя')
        if product_code is not None and len(product_code) > 0:

            complex_article = article + '_' + product_code
            if complex_article not in complex_articles_set:
                article = complex_article

            # Issue #160. Добавляем список альтернативных индексов товара
            product.set_alternative_index(product_code)

        # Issue #73. Если указан ключ экспорта "не группировать данные",
        # то подмешиваем к артикулу случайное значение
        if self.so.random_product_type:
            article += ScrapingHelper.generator_random_string()

        # Issue #117. Дублируем вариации товаров с подменой свойства
        # "variations_list" на "Размер"
        clothes_size = product.get_property('variations_list')
        if clothes_size is not None:

            product.delete_property_in_offer('variations_list')

            for size_value in clothes_size.split(';'):

                # Issue #137. Описание вариации может содержать не только
                # число - размер товара, но и href на персональную страницу
                # этого товара. href нам особенно важен, поскольку необходимо
                # вести пользователя по правильным ссылкам с фронта
                # сайта "ДетскийСписок". Формат представления вариаций:
                # [Вариация]{[URL]}

                value = size_value
                href = None

                left_brace = size_value.find('{')
                right_brace = size_value.find('}')
                if 0 < left_brace < right_brace:

                    value = size_value[:left_brace]
                    href = size_value[left_brace + 1:right_brace]

                # Копируем товар перед включением в список, чтобы
                # избежать множественных ссылок на объект в коллекции
                # и, соответственно, зависания
                copy_of_product = copy.deepcopy(product)
                copy_of_product.append_property_in_offer('Размер', value)

                # Если описание вариации содержит URL, то подменяем URL
                # товара-вариации
                if href is not None:
                    copy_of_product.fix_url(self.get_shop_site(), href)

                # Добавляем информацию о товаре в коллекцию
                append_to.append(article, copy_of_product)
        else:

            # Добавляем информацию о товаре в коллекцию
            append_to.append(article, product)

    # Метод корректирует название товара, а так же ссылки на страницу товара и фотографии
    def _update_product(self, product, product_name_property='short_name', modify_everything=True):

        # Сохраняем название бренда. Issue #98. В "Кораблике" есть два
        # схожих свойства "Торговая марка" (находится на странице деталей
        # товара) и "Бренд" (выделяется по косвенным признакам). Использовать
        # при генерации JSON-необходимо 'Бренд', см. Issue Tracker
        brand_prop = product.get_property('Бренд')
        if brand_prop is not None:
            product.set_brand(brand_prop)

        # Корректируем название продукта
        product_name = product.get_property(product_name_property)
        if product_name is not None and len(product_name) > 0:
            product.set_product_name(product_name)

        if modify_everything:

            # Issue #90. Убираю символ 'ZERO WIDTH NO-BREAK SPACE' из
            # breadcrumb. Этот символ мешает корректно работать с хлебными
            # крошками в скриптах в Node.js
            if product.container.get(product.OFFERS) is not None:
                if len(product.container[product.OFFERS]) == 1:
                    if product.container[product.OFFERS][0].get(product.PROPERTIES) is not None:

                        props = product.container[product.OFFERS][0][product.PROPERTIES]
                        if props.get('breadcrumb') is not None:
                            props['breadcrumb'] = props['breadcrumb'].replace(u'\ufeff', '')

                            # Проверить, что чистка была выполнена успешно можно кодом, который
                            # идёт ниже. Его нужно раскомментировать
                            # print(repr(breadcrumb_prop))

            # Issue #118. Корректируем названия свойств, добавляя к обычным
            # свойствам пробел в конце, а у вариаций убираем подчёркивание
            product.fix_issue118(self.db.unified_variations.keys())

            product.fix_url(self.get_shop_site())
            product.fix_photos(self.get_shop_site())

        pass

    # Исследовательская функция осуществляет импорт конкретного товара
    # из базы данных
    def import_special_details(self):
        self._scrape_details(1, '/product/50328')
